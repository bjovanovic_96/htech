<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => '',
    // preloading 'log' component
    'preload' => array('log'),
    // Language
    'language' => 'en',
    // path aliases
    'aliases' => array(
        'bootstrap' => realpath(__DIR__ . '/../extensions/bootstrap'),
        'yiiwheels' => realpath(__DIR__ . '/../extensions/yiiwheels'),
        'texteditor' => realpath(__DIR__ . '/../extensions/texteditor'),
        'select2' => realpath(__DIR__ . '/../extensions/ESelect2'),
    ),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'bootstrap.behaviors.*',
        'bootstrap.helpers.*',
        'bootstrap.widgets.*',
        'bootstrap.helpers.*',
        'application.extensions.widgets.*',
    ),
    'modules' => array(
        // uncomment the following to enable the Gii tool
        'gii' => array(
            'generatorPaths' => array('bootstrap.gii'),
            'class' => 'system.gii.GiiModule',
            'password' => 'gii',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('24.135.172.36', '::1'),
        ),
    ),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        // yiistrap configuration
        'bootstrap' => array(
            'class' => 'bootstrap.components.Bootstrap',
            'class' => 'bootstrap.components.TbApi'
        ),
        // yiiwheels configuration
        'yiiwheels' => array(
            'class' => 'yiiwheels.YiiWheels',
        ),
        // yiiwheels configuration
        'texteditor' => array(
            'class' => 'texteditor.TextEditorInit',
        ),
        // Cookie secrity
        'request' => array(
            'enableCookieValidation' => true,
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'rules' => array(
                // remove site from url
                '<action:\w+>' => 'site/<action>',
                'login' => 'site/login',
                // Display admin url-s
                '<controller:(admin)>/<action:\w+>' => 'admin/<action>',
                '<controller:(admin)>/<action:\w+>/<id:\w+>' => 'admin/<action>',
                '<controller:(admin)>/<action:\w+>/<id:[^\/]*>' => 'admin/<action>',
            ),
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=htech',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8'
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'admin@admin.com',
        'technicalEmail' => 'admin@admin.com',
    ),
);
