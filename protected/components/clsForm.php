<?php

/**
 * Show field in form
 * Who field type colomn in table(text,datetime)?
 */
class clsForm
{

    /** Set field to be date picker */
    public static function datepicker($that, $opt = array())
    {
        $opt = (object)array(
            'model' => isset($opt['model']) ? $opt['model'] : '',
            'attribute' => isset($opt['attribute']) ? $opt['attribute'] : '',
            'name' => isset($opt['name']) ? $opt['name'] : '',
            'value' => isset($opt['value']) ? $opt['value'] : '',
            'label' => isset($opt['label']) ? $opt['label'] : '',
            'class' => isset($opt['class']) ? $opt['class'] : '',
            'style' => isset($opt['style']) ? $opt['style'] : '',
            'yearRange' => isset($opt['yearRange']) ? $opt['yearRange'] : '-70:+1',
            'minDate' => isset($opt['minDate']) ? $opt['minDate'] : date('d.m.Y'),
            'maxDate' => isset($opt['maxDate']) ? $opt['maxDate'] : '',
        );
        $htmlOptions = array(
            'size' => '10', // textField size
            'maxlength' => '10', // textField maxlength
            'class' => $opt->class,
            //'disabled' => 'disabled',
            'readonly' => 'readonly',
            'style' => $opt->style,
        );

        $options = array(
            'showOn' => 'both',
            'showAnim' => 'slideDown',
            'dateFormat' => 'dd.mm.yy', // format of "2012-12-25"
            'yearRange' => $opt->yearRange,
            'minDate' => $opt->minDate,
            'maxDate' => $opt->maxDate,
            'showOtherMonths' => false, // show dates in other months
            'selectOtherMonths' => false, // can seelect dates in other months
            'changeYear' => true, // can change year
            'changeMonth' => true, // can change month
            'showButtonPanel' => true, // show button panel
        );

        if ($opt->name) {
            if ($opt->label) {
                echo CHtml::label($opt->label, '');
            }
            /** Set date picker by name */
            $that->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => $opt->name,
                'value' => $opt->value ? DataFormat::displayDateDMY($opt->value) : '',
                'language' => 'en',
                'htmlOptions' => $htmlOptions,
                'options' => $options,
            ));
        } else {
            if ($opt->label) {
                echo CHtml::activeLabelEx($opt->model, $opt->attribute);
            }
            /** Set date picker by model attributes */
            $that->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $opt->model,
                'attribute' => $opt->attribute,
                'language' => 'en',
                'htmlOptions' => $htmlOptions,
                'options' => $options,
            ));
        }
        return false;
    }

    /** Display all setted messages */
    public static function displayFlashMsg($flash_messages)
    {
        echo '<div class="alert-fixed">';
        foreach ($flash_messages as $key => $message) {
            echo '<div class = "alert alert-' . $key . ' alert-dismissible fade in" role = "alert">
                    <button type = "button" class = "close" data-dismiss = "alert" aria-label = "Close"><span aria-hidden = "true">×</span></button>
                 ' . $message
                . '</div>';
        }
        echo '</div>';
        return false;
    }

    /** Add text to message */
    public static function setFlashMsg($flash_msg_holder, $flash_msg)
    {
        $msg = Yii::app()->user->hasFlash($flash_msg_holder) ? Yii::app()->user->getFlash($flash_msg_holder) : '';
        $msg .= $flash_msg . '<br/>';
        Yii::app()->user->setFlash($flash_msg_holder, $msg);
        return true;
    }

    public static function getInputName($model, $attr, $opt, $type = null)
    {
        $opt->lang = isset($opt->lang) ? $opt->lang : false;
        $temp_attr = $opt->lang ? '[' . $attr . ']' . '[' . $opt->lang . ']' : '[' . $attr . ']';
        $temp_id = $opt->lang ? $attr . '_' . $opt->lang : $attr;

        $otp_attr_array = $opt->attr_array ? $opt->attr_array : '';
        $field_name = get_class($model) . $temp_attr . $otp_attr_array;

        $otp_attr_name = '';
        if ($otp_attr_array) {
            $otp_attr_array = str_replace('[]', '', $otp_attr_array);
            $otp_attr_array = str_replace('[', '', $otp_attr_array);
            $otp_attr_array = explode(']', $otp_attr_array);
            $otp_attr_array = array_filter($otp_attr_array);
            foreach ($otp_attr_array as $name) {
                $otp_attr_name .= '_' . $name;
            }
        }
        $field_id = get_class($model) . '_' . $temp_id . $otp_attr_name;

        $attr_single_name = $opt->lang ? $attr . '[' . $opt->lang . ']' : $attr;
        $attr_single_name = $attr_single_name . $opt->attr_array;

        $val = '';
        $htmlOptions = $opt->htmlOptions;

        $m_data = $model->$attr;
        $htmlOptions['value'] = isset($htmlOptions['value']) && $htmlOptions['value'] ? $htmlOptions['value'] : $m_data;

        // Allways return array not object array !!!!
        if (is_object($htmlOptions['value'])) {
            $t = array();

            foreach ($htmlOptions['value'] as $key => $val) {
                $t[$key] = $val;
            }

            $htmlOptions['value'] = $t;
        }

        if ($type == 'ddl') {
            $ddl_val = $htmlOptions['value'];
            if (is_array($ddl_val)) {
                $ddl_val = isset($ddl_val[0]) ? $ddl_val[0] : '';
            }
            $htmlOptions['value'] = $ddl_val;
            $htmlOptions['options'] = array($ddl_val => array('selected' => true));
        }


        if ($type == 'datetimepicker') {
            $val = $opt->value ? $opt->value : '';
            $val = !$val && $htmlOptions['value'] ? $htmlOptions['value'] : '';
        }

        if ($type == 'tags') {
            $val = $htmlOptions['value'] ? $htmlOptions['value'] : '';
        }

        return (object)array(
            'input_name' => $field_name,
            'input_id' => $field_id,
            'attr' => $attr_single_name,
            'htmlOptions' => $htmlOptions,
            'value' => $val,
        );
    }

    /** display form attributes */
    public static function FormFields($that, $form, $model, $opt = NULL)
    {
        $form_type = isset($opt['form_type']) && $opt['form_type'] ? $opt['form_type'] : 'single';
        $model_form_fileds_sets = $model->attribute_form($form->id);
        $model_block_fileds_sets = $model->attribute_block();

        $order_arr = array();
        // Order blocks
        foreach ($model_form_fileds_sets as $index => $attr_opt) {
            if ($attr_opt->display->visible == true) {
                $order_arr[$attr_opt->display->block][$attr_opt->display->order] = $index;
            }
            // Set type of form
            $attr_opt->element->opt['form_type'] = $form_type;
        }

        // Order attributes
        ksort($order_arr);
        foreach ($order_arr as $i => $val) {
            ksort($order_arr[$i]);

            $data = array(
                'that' => $that,
                'form' => $form,
                'model' => $model,
                'model_block_fileds_sets' => $model_block_fileds_sets,
                'model_form_fileds_sets' => $model_form_fileds_sets,
                'form_id' => $form->id,
                'i' => $i,
                'order_arr' => $order_arr[$i],
            );
            $that->renderPartial('//template/form/f_block_display_form_fields', $data);
        }
        return false;
    }

    public static function textFieldGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_text_field', $data);
        return false;
    }

    public static function numberFieldGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_number_field', $data);
        return false;
    }

    public static function TextAreaGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_area_field', $data);
        return false;
    }

    public static function DatePickerGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'name' => isset($opt['name']) ? $opt['name'] : '',
            'value' => isset($opt['value']) ? $opt['value'] : '',
            'label' => isset($opt['label']) ? $opt['label'] : '',
            'yearRange' => isset($opt['yearRange']) ? $opt['yearRange'] : '-70:+1',
            'minDate' => isset($opt['minDate']) ? $opt['minDate'] : date('d.m.Y'),
            'maxDate' => isset($opt['maxDate']) ? $opt['maxDate'] : '',
        );

        $opt->attribute = isset($opt->attribute) && $opt->attribute ? $opt->attribute : 'date';

        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);

        $that->renderPartial('//template/form/f_datepicker', $data);
        return false;
    }

    public static function TimePickerGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'name' => isset($opt['name']) ? $opt['name'] : '',
            'value' => isset($opt['value']) ? $opt['value'] : '',
            'label' => isset($opt['label']) ? $opt['label'] : '',
        );

        $opt->attribute = $opt->attribute ? $opt->attribute : 'date';

        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);

        $that->renderPartial('//template/form/f_time_picker', $data);
        return false;
    }

    public static function WorkingTimeGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'days_array' => isset($opt['days_array']) ? $opt['days_array'] : array(),
            'name' => isset($opt['name']) ? $opt['name'] : '',
            'value' => isset($opt['value']) ? $opt['value'] : '',
            'label' => isset($opt['label']) ? $opt['label'] : '',
        );

        $opt->attribute = $opt->attribute ? $opt->attribute : 'date';
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);

        $that->renderPartial('//template/form/f_working_time', $data);
        return false;
    }

    public static function TimeBetweenGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'name' => isset($opt['name']) ? $opt['name'] : '',
            'value' => isset($opt['value']) ? $opt['value'] : '',
            'label' => isset($opt['label']) ? $opt['label'] : '',
        );

        $opt->attribute = $opt->attribute ? $opt->attribute : 'date';

        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);

        $that->renderPartial('//template/form/f_time_between', $data);
        return false;
    }

    public static function DDLGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'value' => isset($opt['value']) ? $opt['value'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_ddl_field', $data);
        return false;
    }

    public static function PriceGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'value' => isset($opt['value']) ? $opt['value'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_price_field', $data);
        return false;
    }

    public static function SwitchCheckBoxGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'value' => isset($opt['value']) ? $opt['value'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_switch_check_box_field', $data);
        return false;
    }

    public static function TagsGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'value' => isset($opt['value']) ? $opt['value'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_tags_field', $data);
        return false;
    }

    public static function MultiChoiceGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsParams::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'value' => isset($opt['value']) ? $opt['value'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_multi_choice', $data);
        return false;
    }

    public static function TextEditorGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_editor_field', $data);
        return false;
    }

    public static function imageCropGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'show_label_text' => isset($opt['show_label_text']) ? $opt['show_label_text'] : true,
            'input_id' => isset($opt['input_id']) ? $opt['input_id'] : '',
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'image_url' => isset($opt['image_url']) ? $opt['image_url'] : '',
            'width' => isset($opt['width']) ? $opt['width'] : '',
            'height' => isset($opt['height']) ? $opt['height'] : '',
            'file_name' => isset($opt['file_name']) ? $opt['file_name'] : '',
            'folder_path_name' => isset($opt['folder_path_name']) ? $opt['folder_path_name'] : '',
            'folder_url_name' => isset($opt['folder_url_name']) ? $opt['folder_url_name'] : '',
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_image_crop_modal', $data);
        return false;
    }

    public static function ProductGalleryFieldGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'max_files' => isset($opt['max_files']) ? $opt['max_files'] : '',
            'file_name' => isset($opt['file_name']) ? $opt['file_name'] : '',
            'folder_path_name' => isset($opt['folder_path_name']) ? $opt['folder_path_name'] : '',
            'folder_url_name' => isset($opt['folder_url_name']) ? $opt['folder_url_name'] : '',
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_product_gallery_field', $data);
        return false;
    }

    public static function ProductPricesFieldGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_product_prices_field', $data);
        return false;
    }

    public static function ProductBarcodesFieldGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_product_barcodes_field', $data);
        return false;
    }

    public static function FileFieldGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'max_files' => isset($opt['max_files']) ? $opt['max_files'] : '',
            'file_name' => isset($opt['file_name']) ? $opt['file_name'] : '',
            'folder_path_name' => isset($opt['folder_path_name']) ? $opt['folder_path_name'] : '',
            'folder_url_name' => isset($opt['folder_url_name']) ? $opt['folder_url_name'] : '',
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_file_field', $data);
        return false;
    }

    public static function FilePDFFieldGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'max_files' => isset($opt['max_files']) ? $opt['max_files'] : '',
            'file_name' => isset($opt['file_name']) ? $opt['file_name'] : '',
            'folder_path_name' => isset($opt['folder_path_name']) ? $opt['folder_path_name'] : '',
            'folder_url_name' => isset($opt['folder_url_name']) ? $opt['folder_url_name'] : '',
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_file_pdf_field', $data);
        return false;
    }

    public static function DDLTagsGroup($that, $form, $model, $attr, $opt = array())
    {
        $return = '';
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            //extra options
            'max_files' => isset($opt['max_files']) ? $opt['max_files'] : '',
            'value' => isset($opt['value']) ? $opt['value'] : array()
        );

        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_ddl_tags_field', $data);
        return false;
    }

    public static function VideoFieldGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'value' => isset($opt['value']) ? $opt['value'] : array(),
            'ddl_label_text' => isset($opt['ddl_label_text']) ? $opt['ddl_label_text'] : '',
            'ddl_display_type' => isset($opt['display_type']) ? $opt['display_type'] : 'inline',
            'link_label_text' => isset($opt['link_label_text']) ? $opt['link_label_text'] : '',
            'link_display_type' => isset($opt['link_display_type']) ? $opt['link_display_type'] : 'inline',
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_video_field', $data);
        return false;
    }

    public static function checkBoxGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_check_box_field', $data);
        return false;
    }

    public static function radioButtonGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'value' => isset($opt['value']) ? $opt['value'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_radio_button_field', $data);
        return false;
    }

    public static function PostalCodeGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_postal_code_ajax_select', $data);
        return false;
    }

    public static function passwordFieldGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_password_field', $data);
        return false;
    }

    public static function DDLWithSearchGroup($that, $form, $model, $attr, $opt = array())
    {
        $return = '';
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            //extra options
            'value' => isset($opt['value']) ? $opt['value'] : array()
        );

        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_dropdown_with_search', $data);
        return false;
    }

    public static function changePasswordGroup($form, $model, $attr_pass, $attr_repeat, $new_password, $scenario, $opt = array())
    {
        $return = '';
        $opt = (object)array(
            'class' => isset($opt['class_alert']) ? $opt['class_alert'] : '',
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
        );

        if ($new_password == 'confirm') {
            $return .= '<div class="form-group">';
            $return .= '<div class="row">';
            $return .= '<div class="col-md-6">';
            $return .= '<label class="col-md-12" for="' . $attr_pass . '">';
            $return .= $form->label($model, $attr_pass) . ':';
            $return .= '<label class="pull-right allert alert-danger">' . $form->error($model, $attr_pass) . ' </label>';
            $return .= '<div class="col-md-12">';
            $return .= $form->textField($model, $attr_pass, $opt->htmlOptions) . ' </div>';
            $return .= '</div>';
            $return .= '</label>';
            $return .= '<div class="col-md-6">';
            $return .= '<label class="col-md-12" for="' . $attr_repeat . '">  ';
            $return .= $form->label($model, 'repeat_password') . ':';
            $return .= '<label class="pull-right allert alert-danger">' . $form->error($model, $attr_repeat) . '</label>';
            $return .= '<div class="col-md-12">';
            $return .= $form->textField($model, 'repeat_password', $opt->htmlOptions) . ' </div>';
            $return .= '</div>';
            $return .= '</label>';
            $return .= '</div>';
            $return .= '</div>';
        } else {
            isset($scenario) && $scenario == "update_password" ? $scenario = 'true' : $scenario = 'false';
            $return .= '<div class="form-group ">';
            $return .= '<br/>';
            $return .= '<div class="row">';
            $return .= '<div class="col-md-3">';
            $return .= ' <span data-toggle="collapse" class="btn btn-primary" data-target="#demo">Promeni sifru</span>';
            $return .= '</div>';
            $return .= '<div id="demo" class="collapse col-md-9">';
            $return .= '<label class="col-md-4" for="' . $attr_pass . '">';
            $return .= '<label>Unesi postojecu sifru: </label>';
            $return .= '</label>';
            $return .= '<div class="col-md-4">';
            $return .= '<input type="text" name="' . get_class($model) . '[confirm_password]">';
            $return .= '</div>';
            $return .= '<div class="col-md-4">';
            $return .= '<button type="submit" class="btn btn-primary" name="scenario" value="' . $scenario . '">Posalji</button>';
            $return .= '</div>';
            $return .= '</div>';
            $return .= '</div>';
            $return .= '</div>';
            $return .= '</div>';
        }
        return $return;
    }

    public static function MultiItemAttributesGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            //extra options
            'value' => isset($opt['value']) ? $opt['value'] : array(),
            'class_item' => isset($opt['class_item']) ? $opt['class_item'] : 'col-md-4',
        );

        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_item_attributes_field', $data);
        return false;
    }

    /** Set postion to make a div on other element */
    public static function MarkTextPositionOnImageGroup($that, $form, $model, $attr, $opt = array())
    {
        $opt = (object)array(
            // Default values
            'display_type' => isset($opt['display_type']) ? $opt['display_type'] : '',
            'form_type' => isset($opt['form_type']) ? $opt['form_type'] : 'single', // Need for multi lang to display value
            'class_error' => isset($opt['class_error']) ? $opt['class_error'] : '',
            'attr_array' => isset($opt['attr_array']) ? $opt['attr_array'] : '',
            'display_label' => isset($opt['display_label']) ? $opt['display_label'] : true,
            'label_text' => isset($opt['label_text']) ? $opt['label_text'] : '',
            'lang' => isset($opt['lang']) ? $opt['lang'] : clsLoad::getSelectedLang(),
            'htmlOptions' => isset($opt['htmlOptions']) ? $opt['htmlOptions'] : array(),
            // Extra values
            'image_url' => isset($opt['image_url']) ? $opt['image_url'] : '',
            'width' => isset($opt['width']) ? $opt['width'] : '',
            'height' => isset($opt['height']) ? $opt['height'] : '',
            'file_name' => isset($opt['file_name']) ? $opt['file_name'] : '',
            'folder_path_name' => isset($opt['folder_path_name']) ? $opt['folder_path_name'] : '',
            'folder_url_name' => isset($opt['folder_url_name']) ? $opt['folder_url_name'] : '',
        );
        $data = array(
            'that' => $that,
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt);
        $that->renderPartial('//template/form/f_mark_text_position_on_image', $data);
        return false;
    }

}
