<?php

class clsLoad
{

    //CITIES
    public static function getDPTblCities($opt = null)
    {
        $tbl_name = 'TblCities';

        /* Get data from DB */
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array(
            'active' => isset($opt->active) ? $opt->active : 1,
            'page_size' => isset($opt->page_size) ? $opt->page_size : 10,
        );

        $opt_db_find = (object)array(
            'condition' => $opt,
            'tbl_name' => $tbl_name,
            'type' => 'tbl_data', // {find | findAll | DP | tbl_data}
            'empty_return_type' => false
        ); // {true return false | false return model}

        $model = clsFunctions::data($opt_db_find);

        /* Return value */
        return $model;
    }

    public static function findAllTblCities($opt = null)
    {
        $tbl_name = 'TblCities';
        $return = array();
        /* Get data from DB */
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array(
            'name' => isset($opt->name) ? $opt->name : "",
            'id_countries' => isset($opt->id_countries) ? $opt->id_countries : "",
            'page_size' => isset($opt->page_size) ? $opt->page_size : "",
            'active' => isset($opt->active) ? $opt->active : 1,
        );
        $opt_db_find = (object)array(
            'condition' => $opt,
            'tbl_name' => $tbl_name,
            'type' => 'findAll', // {find | findAll | DP | tbl_data}
            'empty_return_type' => true
        ); // {true return false | false return model}

        $return = clsFunctions::data($opt_db_find);
        /* Return value */
        return $return;
    }

    public static function findTblCities($opt = null)
    {
        $tbl_name = 'TblCities';
        $return = array();
        /* Get data from DB */
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array(
            'id' => isset($opt->id) ? $opt->id : "",
            'name' => isset($opt->name) ? $opt->name : "",
            'active' => isset($opt->active) ? $opt->active : 1,
        );
        $opt_db_find = (object)array(
            'condition' => $opt,
            'tbl_name' => $tbl_name,
            'type' => 'find', // {find | findAll | DP | tbl_data}
            'empty_return_type' => true
        ); // {true return false | false return model}

        $return = clsFunctions::data($opt_db_find);
        /* Return value */
        return $return;
    }

    public static function getDDLTblCities($opt = null)
    {
        $tbl_name = 'TblCities';

        /* Get data from DB */
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array(
            'active' => isset($opt->active) ? $opt->active : 1,
            'prompt' => isset($opt->prompt) ? $opt->prompt : "",
        );

        $opt_db_find = (object)array(
            'condition' => $opt,
            'tbl_name' => $tbl_name,
            'type' => 'findAll', // {find | findAll | DP}
            'empty_return_type' => false
        ); // {true return false | false return model}

        $opt_db_convert = (object)array(
            'type' => 'ddl', // {count | ddl | index_array | array | value}
            'key' => 'id',
            'value' => 'name',
            'prompt' => $opt->prompt,
        );
        $return = clsFunctions::data($opt_db_find, $opt_db_convert);
        /* Return value */
        return $return;
    }

    //CITY COMMENTS
    public static function findTblCityComments($opt = null)
    {
        $tbl_name = 'TblCityComments';
        $return = array();
        /* Get data from DB */
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array(
            'id' => isset($opt->id) ? $opt->id : "",
            'id_cities' => isset($opt->id_cities) ? $opt->id_cities : "",
            'active' => isset($opt->active) ? $opt->active : 1,
        );
        $opt_db_find = (object)array(
            'condition' => $opt,
            'tbl_name' => $tbl_name,
            'type' => 'find', // {find | findAll | DP | tbl_data}
            'empty_return_type' => true
        ); // {true return false | false return model}

        $return = clsFunctions::data($opt_db_find);
        /* Return value */
        return $return;
    }
    public static function findAllTblCityComments($opt = null)
    {
        $tbl_name = 'TblCityComments';
        $return = array();
        /* Get data from DB */
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array(
            'id_cities' => isset($opt->id_cities) ? $opt->id_cities : "",
            'page_size' => isset($opt->page_size) ? $opt->page_size : "",
            'active' => isset($opt->active) ? $opt->active : 1,
        );
        $opt_db_find = (object)array(
            'condition' => $opt,
            'tbl_name' => $tbl_name,
            'type' => 'findAll', // {find | findAll | DP | tbl_data}
            'empty_return_type' => true
        ); // {true return false | false return model}

        $return = clsFunctions::data($opt_db_find);
        /* Return value */
        return $return;
    }

    //AIRPORTS
    public static function findAllTblAirports($opt = null)
    {
        $tbl_name = 'TblAirports';
        $return = array();
        /* Get data from DB */
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array(
            'id_cities' => isset($opt->id_cities) ? $opt->id_cities : "",
            'active' => isset($opt->active) ? $opt->active : 1,
        );
        $opt_db_find = (object)array(
            'condition' => $opt,
            'tbl_name' => $tbl_name,
            'type' => 'findAll', // {find | findAll | DP | tbl_data}
            'empty_return_type' => false
        ); // {true return false | false return model}

        $return = clsFunctions::data($opt_db_find);
        /* Return value */
        return $return;
    }
    //ROUTES
    public static function findAllTblRoutes($opt = null)
    {
        $tbl_name = 'TblRoutes';
        $return = array();
        /* Get data from DB */
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array(
            'source_airport_id' => isset($opt->source_airport_id) ? $opt->source_airport_id : "",
            'destination_airport_id' => isset($opt->destination_airport_id) ? $opt->destination_airport_id : "",
            'active' => isset($opt->active) ? $opt->active : 1,
        );
        $opt_db_find = (object)array(
            'condition' => $opt,
            'tbl_name' => $tbl_name,
            'type' => 'findAll', // {find | findAll | DP | tbl_data}
            'empty_return_type' => true
        ); // {true return false | false return model}

        $return = clsFunctions::data($opt_db_find);
        /* Return value */
        return $return;
    }

    //USER ACCESS
    public static function findAllTblUserAccess($opt = null)
    {
        $tbl_name = 'TblUserAccess';
        $return = array();
        /* Get data from DB */
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array(
            'username' => isset($opt->username) ? $opt->username : "",
            'password' => isset($opt->password) ? $opt->password : "",
            'active' => isset($opt->active) ? $opt->active : 1,
        );
        $opt_db_find = (object)array(
            'condition' => $opt,
            'tbl_name' => $tbl_name,
            'type' => 'findAll', // {find | findAll | DP | tbl_data}
            'empty_return_type' => true
        ); // {true return false | false return model}

        $return = clsFunctions::data($opt_db_find);
        /* Return value */
        return $return;
    }

    //COUNTRIES
    public static function findAllTblCountries($opt = null)
    {
        $tbl_name = 'TblCountries';
        $return = array();
        /* Get data from DB */
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array(
            'active' => isset($opt->active) ? $opt->active : 1,
        );
        $opt_db_find = (object)array(
            'condition' => $opt,
            'tbl_name' => $tbl_name,
            'type' => 'findAll', // {find | findAll | DP | tbl_data}
            'empty_return_type' => false
        ); // {true return false | false return model}

        $return = clsFunctions::data($opt_db_find);
        /* Return value */
        return $return;
    }

    public static function findTblCountries($opt = null)
    {
        $tbl_name = 'TblCountries';
        $return = array();
        /* Get data from DB */
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array(
            'name' => isset($opt->name) ? $opt->name : "",
            'active' => isset($opt->active) ? $opt->active : 1,
        );
        $opt_db_find = (object)array(
            'condition' => $opt,
            'tbl_name' => $tbl_name,
            'type' => 'find', // {find | findAll | DP | tbl_data}
            'empty_return_type' => true
        ); // {true return false | false return model}

        $return = clsFunctions::data($opt_db_find);
        /* Return value */
        return $return;
    }
    public static function getDDLTblCountries($opt = null)
    {
        $tbl_name = 'TblCountries';

        /* Get data from DB */
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array(
            'active' => isset($opt->active) ? $opt->active : 1,
            'prompt' => isset($opt->prompt) ? $opt->prompt : "",
        );

        $opt_db_find = (object)array(
            'condition' => $opt,
            'tbl_name' => $tbl_name,
            'type' => 'findAll', // {find | findAll | DP}
            'empty_return_type' => false
        ); // {true return false | false return model}

        $opt_db_convert = (object)array(
            'type' => 'ddl', // {count | ddl | index_array | array | value}
            'key' => 'id',
            'value' => 'name',
            'prompt' => $opt->prompt,
        );
        $return = clsFunctions::data($opt_db_find, $opt_db_convert);
        /* Return value */
        return $return;
    }

    //ROLES
    public static function findTblRoles($opt = null)
    {
        $tbl_name = 'TblRoles';
        $return = array();
        /* Get data from DB */
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array(
            'id' => isset($opt->id) ? $opt->id : "",
            'active' => isset($opt->active) ? $opt->active : 1,
        );
        $opt_db_find = (object)array(
            'condition' => $opt,
            'tbl_name' => $tbl_name,
            'type' => 'find', // {find | findAll | DP | tbl_data}
            'empty_return_type' => true
        ); // {true return false | false return model}

        $return = clsFunctions::data($opt_db_find);
        /* Return value */
        return $return;
    }

}
