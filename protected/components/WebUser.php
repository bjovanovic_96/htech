<?php 

class WebUser extends CWebUser {
    
    public function getRole()
    {
        return Yii::app()->user->getState('role');
    }
    public function getPoslovni_partner_id()
    {
        return Yii::app()->user->getState('poslovni_partner_id');
    }
    public function getIme_prezime()
    {
        return Yii::app()->user->getState('ime_prezime');
    }
    public function getRadna_pozicija()
    {
        return Yii::app()->user->getState('radna_pozicija');
    }
    public function getEmail()
    {
        return Yii::app()->user->getState('email');
    }
}