<?php

class clsUrls {

    //SITE
    public static function getHomeUrl() {
        return Yii::app()->getHomeUrl();
    }

    public static function getLoginUrl($slug = null) {
        $slug = $slug ? '/id/' . $slug : '';
        return Yii::app()->createUrl("site/login" . $slug);
    }
    public static function getLogoutUrl($slug = null) {
        $slug = $slug ? '/id/' . $slug : '';
        return Yii::app()->createUrl("site/logout" . $slug);
    }
    public static function getRoutesUrl($slug = null) {
        $slug = $slug ? '/id/' . $slug : '';
        return Yii::app()->createUrl("site/routes" . $slug);
    }
    //ADMIN
    public static function getAdminCitiesEditUrl($opt = null) {
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = array(
            'action' => 'admin/citiesedit',
            'slug' => isset($opt->slug) ? $opt->slug : '',
            'slug_id' => isset($opt->slug_id) ? $opt->slug_id : '',
            'base' => isset($opt->base) ? $opt->base : '',
            'params' => isset($opt->params) ? $opt->params : '',
        );
        return clsFunctions::getPageUrl($opt);
    }

    public static function getAdminCitiesUrl($opt = null) {
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = array(
            'action' => 'admin/cities',
            'slug' => isset($opt->slug) ? $opt->slug : '',
            'slug_id' => isset($opt->slug_id) ? $opt->slug_id : '',
            'base' => isset($opt->base) ? $opt->base : '',
            'params' => isset($opt->params) ? $opt->params : '',
        );
        return clsFunctions::getPageUrl($opt);
    }
    //AJAX
    public static function getAjaxCityCommentsUrl($opt = null) {
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = array(
            'action' => 'ajax/citycomments',
            'slug' => isset($opt->slug) ? $opt->slug : '',
            'slug_id' => isset($opt->slug_id) ? $opt->slug_id : '',
            'base' => isset($opt->base) ? $opt->base : '',
            'params' => isset($opt->params) ? $opt->params : '',
        );
        return clsFunctions::getPageUrl($opt);
    }

}
