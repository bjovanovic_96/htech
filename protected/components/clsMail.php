<?php

class clsMail {

    public static function send($type, $data) {
        $name_from_temp = isset(Yii::app()->user->name) && Yii::app()->user->name != 'Guest' ? Yii::app()->user->name : '';
        $name_temp = isset($data['full_name']) ? $data['full_name'] : $name_from_temp;
        $name_temp = isset($data['admin_name']) ? $data['admin_name'] : $name_temp;
        $name_temp = $name_temp ? $name_temp : clsParams::getEmailSendName();
        $name = '=?UTF-8?B?' . base64_encode($name_temp) . '?=';
        $mail_from_temp = isset(Yii::app()->user->email) ? Yii::app()->user->email : '';
        $mail_from = isset($data['mail_from']) ? $data['mail_from'] : $mail_from_temp;
        $mail_from = $mail_from ? $mail_from : Yii::app()->params['adminEmail'];
        $mail_to = isset($data['mail_to']) && $data['mail_to'] ? $data['mail_to'] : Yii::app()->params['adminEmail'];
        $title = self::getMailTitle($type, $data);
        $body = self::getMailBody($type, $data);
        $subject = '=?UTF-8?B?' . base64_encode($title) . '?=';
        $headers = "From: $name <{$mail_from}>\r\n" .
                "Reply-To: {$mail_from}\r\n" .
                "MIME-Version: 1.0\r\n" .
                "Content-Type: text/html; charset=UTF-8";
        $status = mail($mail_to, $subject, $body, $headers);
        $log = 'Mail => mail_to: ' . $mail_to . ', subject: ' . $subject . ' body: ' . $body;
        dev::saveLog($log, $status ? "OK" : "ERROR");
        return false;
    }

    public static function getMailTitle($type, $data) {
        $return = '';
        switch ($type) {
            case 'contact_admin':
                $return = clsParams::getCompanyName() . ' - Message from ' . $data['data']->first_name . ' ' . $data['data']->last_name;
                break;
        }
        return $return;
    }

    public static function getMailBody($type, $data) {
        $return = '';
        switch ($type) {
            case 'contact_admin':
                $message = 'Message from ' . $data['data']->first_name . ' ' . $data['data']->last_name
                        . '<br/>'
                        . 'Message: ' . $data['data']->body;

                $return = self::connectTemplateData(array('message' => $message));
                break;
        }

        return $return;
    }

    public static function connectTemplateData($opt = null) {
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object) array(
                    'message' => isset($opt->message) ? $opt->message : '',
                    'link_home' => isset($opt->link_home) ? $opt->link_home : Yii::app()->getBaseUrl(true) . clsUrls::getHomeUrl(),
                    'logo' => isset($opt->logo) ? $opt->logo : Yii::app()->getBaseUrl(true) . clsPath::getDefaultImgUrl('logo.png'),
                    'company' => isset($opt->company) ? $opt->company : clsParams::getCompanyName(),
        );
        $body = self::template();
        foreach ($opt as $find => $val) {
            $body = str_replace('{' . $find . '}', $val, $body);
        }
        return $body;
    }

    public static function template() {
        return '
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{company}</title>
    </head>
    <body>
        <div style="max-width: 600px; margin: auto;">
            <div style="background: #f8f8f8; padding: 20px; border: 1px solid #ddd;">
                <a href="{link_home}" target="_blank">
                    <img style="font-size: larger; font-family: arial; color: #1f5d8c;" src="{logo}" alt="{company}" title="{company}">
                </a>
            </div>
            <div style="padding: 20px; border: 1px solid #ddd; border-top: none; font-family: arial; color: #656565; font-size: 14px;">
                <p>Respected,</p>
                <p>&nbsp;</p>
                <p style="line-height: 1.5;" >{message}</p>
                <p style="margin-top: 70px;">With respect</p>
                <p>Your {company}</p>
                <div style=" text-align: center; ">
                    <ul style="list-style-type: none; margin-bottom: -5px; margin-top: 25px; padding: 0;">
                        <li style="display: inline-block;"><a style="color: #656565;" href="{link_home}" target="_blank">visit the site</a> | </li>
                        <li style="display: inline-block;"><a style="color: #656565;" href="{link_ask_us}" target="_blank">send request</a> | </li>
                        <li style="display: inline-block;"><a style="color: #656565;" href="{link_contact}" target="_blank">contact us</a></li>
                    </ul>
                    <p style="color: #b5b4b4; font-size: smaller;margin-top: 25px;">© ' . date('Y') . ' {company} | All rights reserved.</p>
                </div>
            </div>
        </div>
    </body>
</html>';
    }

}
