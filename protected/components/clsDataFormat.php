<?php

/**
 * Prepering data
 * Convert date in sql date
 * Conver model in array
 */
class clsDataFormat
{

    //Return value with no decimal on end
    public static function numberNoDecimal($number)
    {
        return number_format($number, 0, ',', '.');
    }

    //Convert string to phone dispay 064.123.1234
    public static function phoneNumber($data)
    {
        return substr($data, 0, 3) . "." . substr($data, 3, 3) . "." . substr($data, 6);
    }

    //Get real time that is on users side no on server side
    public static function currentRealTime($format = 'H:i d.m.Y')
    {
        return date($format, strtotime('+0 hours'));
    }

    //Get or convert date time for SQL to be valid
    public static function sqlDateTime($date_time = null)
    {
        return $date_time ? date('Y-m-d H:i:s', strtotime($date_time)) : self::currentRealTime('Y-m-d H:i:s');
    }

    //Get or convert date for SQL to be valid
    public static function sqlDate($date_time = null)
    {
        return $date_time ? date('Y-m-d', strtotime($date_time)) : self::currentRealTime('Y-m-d');
    }

    //Defalt format value for Date Time to be in whole project
    public static function displayDateTime($date_time = null)
    {
        return $date_time ? date('H:i d.m.Y', strtotime($date_time)) : self::currentRealTime('H:i d.m.Y');
    }

    //Defalt format value for Date to be in whole project
    public static function displayDate($date_time = null)
    {
        return $date_time ? date('d.m.Y', strtotime($date_time)) : self::currentRealTime('d.m.Y');
    }

    //Defalt format value for Time to be in whole project
    public static function displayTime($date_time = null)
    {
        return $date_time ? date('H:i', strtotime($date_time)) : self::currentRealTime('H:i');
    }

    //Convert Date Time to custom format
    public static function convertDateTime($date_time = null, $format = 'H:i d.m.Y')
    {
        return $date_time ? date($format, strtotime($date_time)) : date($format, strtotime('+2 hours'));
    }

    //Check if Date Time is SQL default value
    public static function isDefaultSQLDateTime($date_time = null)
    {
        $return = true;
        if ($date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00' || $date_time == '1970-01-01' || $date_time == '1970-01-01 00:00:00') {
            $return = false;
        }
        return $return;
    }

    //Get how much time past (ago 15 minutes)
    public static function convertToTimeAgo($that, $date_time = null, $language = 'rs')
    {
        $that->widget('yiiwheels.widgets.timeago.WhTimeAgo', array(
                'date' => date('Y-m-d H:i:s', strtotime($date_time)), // '2008-07-17T09:24:17Z'
                'language' => $language,
            )
        );
        return false;
    }

}
