<?php

class clsSave
{

    public static function modelSave($opt = null)
    {
        $opt = isset($opt) && $opt ? (object)$opt : array();
        $opt = (object)array(
            'post' => isset($opt->post) ? $opt->post : array(),
            'file_path' => isset($opt->file_path) ? $opt->file_path : array(),
            'data' => isset($opt->data) ? $opt->data : array(),
            'default_value' => isset($opt->default_value) ? $opt->default_value : array(),
            'that' => isset($opt->that) ? $opt->that : array()
        );

        $post = $opt->post;
        $data = $opt->data;
        $m = $data['model'];
        // Set data from post to model
        $options = (object)array(
            'model' => $m,
            'data' => $post
        );
        $m = clsFunctions::connectArrayValueWithModelAttributes($options);
        foreach ($m->attributes as $attr => $value) {
            switch ($attr) {
                case 'gallery_images':
                    $image_gallery = "";
                    if ($m->$attr) {
                        $image_gallery = [];
                        for ($i = 0; $i < count($m->$attr['image']); $i++) {
                            if ($m->$attr['image'][$i]) {
                                if (isset($image_gallery[$m->$attr['id_colors'][$i]])) {
                                    $image_gallery[$m->$attr['id_colors'][$i]][] = $m->$attr['image'][$i];
                                } else {
                                    $image_gallery[$m->$attr['id_colors'][$i]] = [$m->$attr['image'][$i]];
                                }
                            }
                        }
                        $m->$attr = $image_gallery ? json_encode($image_gallery) : '';
                    }
                    break;
                case 'gallery_videos':
                    if ($m->$attr)
                        $m->$attr = clsFunctions::convertPostVideoGalleryToJsonArray($m->$attr);
                    else
                        $m->$attr = '';
                    break;
                case 'main_image':
                case 'image':
                    $m->$attr = $m->$attr ? json_encode(array(
                        $m->$attr
                    )) : '';
                    break;
                case 'birth_date':
                case 'publish_date':
                    $m->$attr = $m->$attr ? clsDataFormat::sqlDate($m->$attr) : '';
                    break;
                case 'tags':
                    $m->$attr = $m->$attr ? clsFunctions::convertStringToJSON(array('data' => $m->$attr)) : '';
                    break;
            }
            if ($m->$attr == "") {
                $m->$attr = null;
            }
        }
        $validate = true;
        if (!$m->validate()) {
            $validate = false;
            $temp = '';
            foreach ($m->errors as $key => $e_msg) {
                foreach ($e_msg as $error_msg) {
                    $temp .= $error_msg . '<br/>';
                }
            }
            $temp = substr($temp, 0, -5);
            clsForm::setFlashMsg('danger', '<strong>Validate : </strong><br/>' . $temp);
        }

        // Set data
        $data['model'] = $m;
        // If everything is ok, set code and save
        if ($validate) {
            $slug = '';
            // Get model by lang
            $m = $data['model'];
            if ($m->hasAttribute('slug') && !$m->slug) {
                $slug_title = $m->hasAttribute('title') ? $m->title : $m->username;
                $slug = $m->hasAttribute('slug') ? $m->slug ? $m->slug : clsFunctions::makeSlug($slug_title) : '';
                if ($m->hasAttribute('slug')) {
                    $m->slug = $slug;
                }
            }
            if ($m->hasAttribute('visible')) {
                $m->visible = $m->isNewRecord ? 1 : $m->visible;
            }
            $difault_value = (object)$opt->default_value;

            $opt_model_data = array(
                'active' => isset($difault_value->active) && $difault_value->active ? $difault_value->active : 1,
                'create_id' => isset($difault_value->create_id) && $difault_value->create_id ? $difault_value->create_id : clsRole::get_user_id(),
                'create_date' => isset($difault_value->create_date) && $difault_value->create_date ? $difault_value->create_date : date('Y-m-d H:i:s'),
            );
            $options = (object)array(
                'model' => $m,
                'data' => $opt_model_data
            );

            $m = clsFunctions::connectArrayValueWithModelAttributes($options);
            if ($m->save()) {
                $data['status'] = "OK";
            } else {
                $temp = '';
                foreach ($m->errors as $key => $e_msg) {
                    foreach ($e_msg as $error_msg) {
                        $temp .= $error_msg . '<br/>';
                    }
                }
                $temp = substr($temp, 0, -5);
                clsForm::setFlashMsg('danger', '<strong>Čuvanje:</strong><br/>' . $temp);
            }
        } else {
            $data['status'] = "ERROR";
        }
        return $data;
    }

}
