<?php

class clsRole {

    public static function is_login(){
        $return=false;
        if(Yii::app()->user->id){
            $return=true;
        }
        return $return;
    }

    public static function get_user_id() {
        return isset(Yii::app()->user->id) ? Yii::app()->user->id : null;
    }

    public static function is_admin() {
        $return=false;
        if(self::is_login()){
            $id_roles=Yii::app()->user->id_roles;
            if($id_roles){
                $opt_sql=[
                    'id'=>$id_roles
                ];
                $model_roles=clsLoad::findTblRoles($opt_sql);
                if($model_roles && $model_roles->name=="admin"){
                    $return=true;
                }
            }
        }
        return $return;
    }
    public static function is_user() {
        $return=false;
        if(self::is_login()){
            $id_roles=Yii::app()->user->id_roles;
            if($id_roles){
                $opt_sql=[
                    'id'=>$id_roles
                ];
                $model_roles=clsLoad::findTblRoles($opt_sql);
                if($model_roles && $model_roles->name=="user"){
                    $return=true;
                }
            }
        }
        return $return;
    }

    public static function accessRulesAdminController() {
        $access = array();
        if (self::is_admin()) {
            $access = array(
                'cities',
                'citiesedit',
            );
        }
        return array(
            array(
                'allow', // allow user to perform actions
                'actions' => $access,
                'users' => array(
                    '@'
                )
            ),
            array(
                'deny', // deny all users
                'users' => array(
                    '*'
                )
            )
        );
    }

}
