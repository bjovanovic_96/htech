<?php

class clsView
{

    public static function gridFullTabele($that, $opt)
    {
        $rand_num = rand(100, 999); // Need for more table on same page
        $opt = (object)array(
            'view' => (object)array(
                'title' => isset($opt['view']['title']) ? $opt['view']['title'] : false, // Title of table
                'header_link' => isset($opt['view']['header_link']) ? $opt['view']['header_link'] : false, // Title of table
                'header_link_title' => isset($opt['view']['header_link_title']) ? $opt['view']['header_link_title'] : false, // Title of table
                'title_tag' => isset($opt['view']['title_tag']) ? $opt['view']['title_tag'] : 'H2', // Title tag name
                'ajax_enable' => isset($opt['view']['ajax_enable']) ? $opt['view']['ajax_enable'] : true, // Enable more rows ajax actions
                'search_enable' => isset($opt['view']['search_enable']) ? $opt['view']['search_enable'] : true, // Enable advance search
                'quick_search_enable' => isset($opt['view']['quick_search_enable']) ? $opt['view']['quick_search_enable'] : true, // Enable quick search
                'action_id' => isset($opt['view']['action_id']) ? $opt['view']['action_id'] : '', // What action is active
            ),
            'grid' => (object)array(
                'grid_id' => isset($opt['grid']['grid_id']) ? $opt['grid']['grid_id'] : '', // table ID
                'checkbox_id' => isset($opt['grid']['checkbox_id']) ? $opt['grid']['checkbox_id'] : 'selectedIdsAll_' . $rand_num, //
                'grid_data' => isset($opt['grid']['grid_data']) ? $opt['grid']['grid_data'] : '', // Data provider from model
                'grid_template' => isset($opt['grid']['grid_template']) ? $opt['grid']['grid_template'] : '<div style="display: inline-block; width: 100%;"><span class="pull-left">{pager}</span><span class="pull-right">{summary}</span></div>{items}<div style="display: inline-block; width: 100%;"><span class="pull-left">{pager}</span><span class="pull-right">{summary}</span></div>', // Display grid elements
                'columns' => isset($opt['grid']['columns']) ? $opt['grid']['columns'] : '', // Function that display need columns
                'grid_id_value' => isset($opt['grid']['grid_id_value']) ? $opt['grid']['grid_id_value'] : 'id',
                'grid_btn_template' => isset($opt['grid']['grid_btn_template']) ? $opt['grid']['grid_btn_template'] : '', // Display btn in row
                'emptyText' => isset($opt['emptyText']) ? $opt['emptyText'] : 'No results', // Displaz text if model does't have rows
                'summaryText' => isset($opt['summaryText']) ? $opt['summaryText'] : 'Show {start}-{end} of {count}',
            ),
            'ajax' => (object)array(
                'ajax_link' => isset($opt['ajax']['ajax_link']) ? $opt['ajax']['ajax_link'] : 'ajax/ajaxpromenastatusa', // Ajax link
                'ddl_class_name' => isset($opt['ajax']['ddl_class_name']) ? $opt['ajax']['ddl_class_name'] : 'grid-action-' . $rand_num,
                'btn_class' => isset($opt['ajax']['btn_class']) ? $opt['ajax']['btn_class'] : 'btn btn-info pull-right  admin_loader',
                'ddl_empty' => isset($opt['ajax']['ddl_empty']) ? $opt['ajax']['ddl_empty'] : 'Primeni odabranu akciju',
                'ddl_data' => isset($opt['ajax']['ddl_data']) ? $opt['ajax']['ddl_data'] : array('' => 'Odaberi'), // Data array for select action
            ),
            'search' => (object)array(
                'search_partial_name' => isset($opt['search']['search_partial_name']) ? $opt['search']['search_partial_name'] : '',
                'quick_search_partial' => isset($opt['search']['quick_search_partial']) ? $opt['search']['quick_search_partial'] : '',
            ),
        );

        $title = '';
        $quick_search = '';
        $search = '';
        $ajax_btn = '';
        $model = $opt->grid->grid_data;

        //dev::p($model);

        if ($opt->view->title) {
            $header_link = "";
            if ($opt->view->header_link && $opt->view->header_link_title) {
                $header_link = '<a href="' . $opt->view->header_link . '"><button class="btn btn-success pull-right">' . $opt->view->header_link_title . '</button></a>';
            }
            $title .= '<' . $opt->view->title_tag . ' style="width: 95%;" class="mobile_w_100">' . $opt->view->title . $header_link . '</' . $opt->view->title_tag . '>';
        }

        if ($opt->view->ajax_enable) {
            $ajax_btn .= CHtml::dropDownList($opt->ajax->ddl_class_name, '', $opt->ajax->ddl_data, array('class' => $opt->ajax->ddl_class_name . ' pull-right'));

            $ajax_btn .= CHtml::ajaxLink($opt->ajax->ddl_empty, $that->createUrl($opt->ajax->ajax_link), array(
                "type" => "post",
                'beforeSend' => 'function(){
                        showLoaderCustom();}',
                "data" => 'js:{'
                    . '"temp_status" : $(".' . $opt->ajax->ddl_class_name . ' option:selected").val(), '
                    . '"temp_id" : $.fn.yiiGridView.getChecked("' . $opt->grid->grid_id . '","' . $opt->grid->checkbox_id . '").toString()}', // settings.tableClass = $opt->grid_id; column_id = $opt->checkbox_id
                "success" => 'js:function(data){ '
                    . ' $("#loader-wrapper").remove(); '
                    . ' data = $.parseJSON(data); '
                    . ' if(data.status){  '
                    . ' var $selected_id=$.fn.yiiGridView.getChecked("' . $opt->grid->grid_id . '","' . $opt->grid->checkbox_id . '").toString().split(","); '
                    . ' var $status=$(".' . $opt->ajax->ddl_class_name . ' option:selected").text();'
                    . ' $.each($selected_id, function ($id,$val) { '
                    . ' var $row = $("*[value=\"" + $val + "\"]"); '
                    . ' $row.attr("checked", false).closest(".odd").find(".status_check").text($status);'
                    . ' });'
                    . ' if(data.status=="ERROR"){  '
                    . ' if(data.msg){ errorMsgAlert(data.msg.toString()); } '
                    . '  }else{'
                    . ' if(data.msg){ successMsgAlert(data.msg.toString()); }'
                    . '  }'
                    . '  }'
                    . '  else{'
                    . ' if(data.msg){ alert(data.msg.toString()); } '
                    . ' if(data.url){ window.location.href = data.url; } else { location.reload(); } '
                    . ' }'
                    . '}'), array(
                    'class' => $opt->ajax->btn_class
                )
            );
        }

        if ($opt->view->quick_search_enable) {
            $quick_search .= '<div class="fast_search-form">';
            $quick_search .= $that->renderPartial('//admin/search/' . $opt->search->quick_search_partial, array('model' => $model), true, false);
            $quick_search .= '</div>';
        }

        if ($opt->view->search_enable) {
            Yii::app()->clientScript->registerScript('search', "
                $('.search-button').click(function(){
                        var holder = $(this).closest('.advance-search');
                        holder.find('.search-form').toggle();
                        return false;
                });
                ");
            $search .= '<div class="advance-search">';
            $search .= CHtml::link(Yii::t('Menu', Yii::t('clsView', 'Advanced search')), '#', array('class' => 'search-button btn btn-default'));
            $search .= '<div class="search-form" style="display:none">';
            $search .= $that->renderPartial('//admin/search/' . $opt->search->search_partial_name, array('model' => $model), true, false);
            $search .= '</div>';
            $search .= '</div>';
        }

        if ($opt->grid->grid_data) {
            echo '<div class="x_panel">';
            echo '<div class="x_title">' . $title . '<ul class="nav navbar-right panel_toolbox">
                                            <li class="mobile_hide"><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul><div class="clearfix"></div></div>';
            echo '<div class="x_content">';
            echo $quick_search;
            echo $search;
            echo $ajax_btn;
            echo '<div class="table-responsive">';

            $col = $opt->grid->columns;
            $columns = clsViewData::$col($opt);

            $that->widget('bootstrap.widgets.TbGridView', array(
                'id' => $opt->grid->grid_id,
                'type' => 'hover', // striped bordered condensed
                'dataProvider' => $model->search(),
                'template' => $opt->grid->grid_template,
                'summaryText' => $opt->grid->summaryText,
                'pagerCssClass' => 'pager_css',
                'ajaxUpdate' => false,
                'pager' => array(
                    'nextPageLabel' => '&rarr;',
                    'prevPageLabel' => '&larr;',
                    'firstPageLabel' => '&laquo;',
                    'lastPageLabel' => '&raquo;',
                    'header' => '',
                ),
                'emptyText' => $opt->grid->emptyText,
                'columns' => $columns,
            ));
            echo '</div>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
        } else {
            echo Yii::t('main', 'No result');
        }

        return false;
    }

}
