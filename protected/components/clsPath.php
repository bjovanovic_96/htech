<?php

class clsPath {

    //PATH
    public static function getDefaultImgPath($file = '', $base = false) {
        return './img/default/' . $file;
    }

    public static function getIconsPath($file = '', $base = false) {
        return './img/icons/' . $file;
    }

    //URL

    public static function getDefaultImgUrl($file = '', $base = false) {
        return Yii::app()->request->getBaseUrl($base) . '/img/default/' . $file;
    }

    public static function getIconsUrl($file = '', $base = false) {
        return Yii::app()->request->getBaseUrl($base) . '/img/icons/' . $file;
    }

}
