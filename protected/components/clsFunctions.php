<?php


class clsFunctions
{
    public static function clear_airports_and_routes()
    {
        $opt_sql = [
            'id_cities' => 0
        ];
        $all_airports = clsLoad::findAllTblAirports($opt_sql);
        if ($all_airports) {
            foreach ($all_airports as $model_airports) {
                $opt_sql = [
                    'source_airport_id' => $model_airports->id
                ];
                $all_routes = clsLoad::findAllTblRoutes($opt_sql);
                if ($all_routes) {
                    foreach ($all_routes as $model_routes) {
                        $model_routes->delete();
                    }
                }
                $opt_sql = [
                    'destination_airport_id' => $model_airports->id
                ];
                $all_routes = clsLoad::findAllTblRoutes($opt_sql);
                if ($all_routes) {
                    foreach ($all_routes as $model_routes) {
                        $model_routes->delete();
                    }
                }
                $model_airports->delete();
            }
        }
    }

    public static function db_edit()
    {
        $all_airports = clsLoad::findAllTblAirports();
        if ($all_airports) {
            foreach ($all_airports as $model_airports) {
                if ($model_airports->city && $model_airports->country) {
                    $opt_sql = [
                        'name' => $model_airports->country
                    ];
                    $model_countries = clsLoad::findTblCountries($opt_sql);
                    if (!$model_countries) {
                        $model_countries = new TblCountries();
                        $model_countries->name = $model_airports->country;
                        $model_countries->create_id = 1;
                        $model_countries->create_date = clsDataFormat::sqlDateTime();
                        $model_countries->active = 1;
                        if (!$model_countries->save()) {
                            dev::pd($model_countries->errors);
                        }
                    }
                    if ($model_countries) {
                        $opt_sql = [
                            'name' => $model_airports->city
                        ];
                        $model_cities = clsLoad::findTblCities($opt_sql);
                        if (!$model_cities) {
                            $model_cities = new TblCities();
                            $model_cities->name = $model_airports->city;
                            $model_cities->id_countries = $model_countries->id;
                            $model_cities->create_id = 1;
                            $model_cities->create_date = clsDataFormat::sqlDateTime();
                            $model_cities->active = 1;
                            if (!$model_cities->save()) {
                                dev::pd($model_countries->errors);
                            }
                        }
                        $model_airports->id_countries = $model_countries->id;
                        $model_airports->id_cities = $model_cities->id;
                        if (!$model_airports->save()) {
                            dev::pd($model_airports->errors);
                        }
                    }

                }
            }
        }
    }

    public static function findDBData($opt = null)
    {
        $opt = self::convertToObjectArray($opt);
        $opt = (object)array('condition' => isset($opt->condition) ? $opt->condition : '', 'tbl_name' => isset($opt->tbl_name) ? $opt->tbl_name : '', 'type' => isset($opt->type) ? $opt->type : '', 'empty_return_type' => isset($opt->empty_return_type) ? $opt->empty_return_type : false);
        $tbl_name = $opt->tbl_name;
        $model = new $tbl_name();
        $model->unsetAttributes();
        if ($opt->condition) {
            $opt_model = (object)array('model' => $model, 'data' => $opt->condition);
            $model = clsFunctions::connectArrayValueWithModelAttributes($opt_model);
            $type = strtolower($opt->type);
            switch ($type) {
                case "find":
                    $model = $model->search()->getData();
                    if (isset($model[0])) {
                        $model = $model[0];
                    } else {
                        $model = false;
                    }
                    break;
                case "findall":
                    $model = $model->search()->getData();
                    if (!$model) {
                        $model = false;
                    }
                    break;
                case "dp":
                    $model = $model->search();
                    break;
                case "tbl_data":
                    $model = $model;
                    break;
            }
        }
        if ($model == false) {
            if (!$opt->empty_return_type) {
                $model = new $tbl_name();
                $model->unsetAttributes();
            }
        }
        return $model;
    }

    public static function convertDBData($opt = null)
    {
        // $opt = self::convertToObjectArray($opt);
        $opt = (object)array(
            'data' => isset($opt->data) ? $opt->data : array(),
            'type' => isset($opt->type) ? $opt->type : '',
            'key' => isset($opt->key) ? $opt->key : '',
            'value' => isset($opt->value) ? $opt->value : '',
            'prompt' => isset($opt->prompt) && $opt->prompt ? $opt->prompt : array('' => Yii::t('default', 'Select')),
            'return_type' => isset($opt->return_type) ? $opt->return_type : '', $return = ''
        );
        $type = strtolower($opt->type);
        if ($opt->type) {
            switch ($opt->type) {
                case 'count':
                    $return = $opt->data ? count($opt->data) : 0;
                    break;
                case 'ddl':
                    $return = CHtml::listData($opt->data, $opt->key, $opt->value);
                    $opt->prompt = (array)$opt->prompt;
                    $return = $opt->prompt + $return;
                    break;
                case 'index_array':
                    $return = CHtml::listData($opt->data, $opt->key, $opt->value);
                    break;
                case 'array':
                    $temp = array();
                    if ($opt->data) {
                        foreach ($opt->data as $index => $row) {
                            $key = $opt->key;
                            if (isset($row->$key)) {
                                $temp[] = $row->$key;
                            }
                        }
                    }
                    $return = $temp;
                    break;
                case 'value':
                    $opt->data = is_array($opt->data) ? $opt->data[0] : $opt->data;
                    $return = $opt->data[$opt->key];
                    break;
            }
        }
        return $return;
    }

    public static function data($opt_db = null, $opt_convert = null)
    {
        $opt_db = is_array($opt_db) ? (object)$opt_db : $opt_db;
        if (isset($opt_db->data) && $opt_db->data) {
            $model = $opt_db->data;
        } else {
            $opt_db = (object)array('condition' => isset($opt_db->condition) ? $opt_db->condition : '', 'tbl_name' => isset($opt_db->tbl_name) ? $opt_db->tbl_name : '', 'type' => isset($opt_db->type) ? $opt_db->type : '', 'empty_return_type' => isset($opt_db->empty_return_type) ? $opt_db->empty_return_type : '');
            $model = self::findDBData($opt_db);
        }
        if (isset($opt_db->condition->together['with']) && $opt_db->condition->together['with']) {
            $rel = $opt_db->condition->together['with'];
            $model = $model->$rel;
        }
        $opt_convert = is_array($opt_convert) ? (object)$opt_convert : $opt_convert;
        if (isset($opt_convert->type) && $opt_convert->type) {
            $opt_convert = (object)array(
                'data' => $model ? $model : array(),
                'type' => isset($opt_convert->type) ? $opt_convert->type : '',
                'key' => isset($opt_convert->key) ? $opt_convert->key : '',
                'value' => isset($opt_convert->value) ? $opt_convert->value : '',
                'prompt' => isset($opt_convert->prompt) ? $opt_convert->prompt : ''
            );
            $model = self::convertDBData($opt_convert);
        }
        return $model;
    }

    public static function modelValidate($opt = null)
    {
        $opt = clsFunctions::convertToObjectArray($opt);
        $opt = (object)array('tbl_name' => isset($opt->tbl_name) ? $opt->tbl_name : '', 'scenario' => isset($opt->scenario) ? $opt->scenario : '', 'posted' => isset($opt->posted) ? $opt->posted : '');
        $response = array('status' => 'ERROR', 'msg' => '');
        $tbl_name = $opt->tbl_name;
        $model = new $tbl_name($opt->scenario);
        $options = array('model' => $model, 'data' => $opt->posted,);
        $model = self::connectArrayValueWithModelAttributes($options);
        if (!$model->validate()) {
            $return['status'] = 'ERROR';
            foreach ($model->errors as $attr => $error) {
                $response['msg'][$opt->tbl_name . '[' . $attr . ']'] = $error[0];
            }
        } else {
            $response['status'] = "OK";
        }
        return $response;
    }

    public static function SetCriteriaCompare($that, $criteria, $attr, $opt = array())
    {
        $opt = (object)array('prefix' => isset($opt['prefix']) ? $opt['prefix'] : 't', 'date_attr' => isset($opt['date_attr']) ? $opt['date_attr'] : false, 'like' => isset($opt['like']) ? $opt['like'] : false, 'glue' => isset($opt['glue']) ? $opt['glue'] : 'AND',);
        if (strpos($that->$attr, 'query') !== false) {
            $str = str_replace('query ', '', $that->$attr);
            $criteria->addCondition($opt->prefix . '.' . $attr . ' ' . $str, $opt->glue);
        } else {
            $criteria->compare($opt->prefix . '.' . $attr . '', $that->$attr, $opt->like, $opt->glue);
        }
        if ($opt->date_attr) {
            if ($that->date_from && $that->date_to) {
                $from = date('Y-m-d H:i:s', strtotime($that->date_from));
                $to = date('Y-m-d 23:59:59', strtotime($that->date_to));
                $criteria->addBetweenCondition($opt->prefix . '.' . $attr, $from, $to, $opt->glue);
            }
        }
        return $criteria;
    }

    /**     * Create models for actionSave     */
    public static function createModelSave($opt = null)
    {
        $opt = clsFunctions::convertToObjectArray($opt);
        $options = (object)array(
            'id' => isset($opt->id) && $opt->id ? $opt->id : '',
            'tbl_name' => isset($opt->tbl_name) && $opt->tbl_name ? $opt->tbl_name : '',
            'selected_lang' => isset($opt->selected_lang) && $opt->selected_lang ? $opt->selected_lang : '',
            'action_id' => isset($opt->action_id) && $opt->action_id ? $opt->action_id : '',
            'scenario' => isset($opt->scenario) && $opt->scenario ? $opt->scenario : '',
            'id_form' => isset($opt->id_form) && $opt->id_form ? $opt->id_form : '',
            'search_attr' => isset($opt->search_attr) && $opt->search_attr ? $opt->search_attr : 'id',
        );
        switch ($options->action_id) {
            case '':
                $type = '';
                break;
        }
        $title = clsParams::getArrayTblInfo()[$options->tbl_name];
        $data = array();

        $data['tbl_name'] = $options->tbl_name;
        $data['code'] = $options->id;
        $data['selected_lang'] = $options->selected_lang ? $options->selected_lang : '';

        $data['title'] = $data['code'] == 'new' ? $title->admin_create_title : $title->admin_update_title;
        if (isset($opt->id_form) && $opt->id_form) {
            $data['id_form'] = $opt->id_form;
        } else {
            $data['id_form'] = 'create-' . $type . $title->short . '-form';
        }

        // Find all active lang
        // Set all model by lang
        if ($data['code'] != 'new') {
            $opt = (object)array(
                $options->search_attr => $data['code'],
            );
            $opt_db_find = (object)array(
                'condition' => $opt,
                'tbl_name' => $data['tbl_name'],
                'type' => 'find', // {find | findAll | DP}
                'empty_return_type' => false
            ); // {true | false}
            $data['model'] = clsFunctions::data($opt_db_find);

            if (!$data['model']) {

                $opt_db_find = (object)array(
                    'condition' => array(),
                    'tbl_name' => $data['tbl_name'],
                    'type' => 'find', // {find | findAll | DP}
                    'empty_return_type' => false
                ); // {true | false}

                $data['model'] = clsFunctions::data($opt_db_find);
                $data['model']->slug = $data['code'];
            }
        } else {

            $opt_db_find = (object)array(
                'condition' => array(
                    'id' => $options->id
                ),
                'tbl_name' => $data['tbl_name'],
                'type' => 'find', // {find | findAll | DP}
                'empty_return_type' => false
            ); // {true | false}

            $data['model'] = clsFunctions::data($opt_db_find);
        }
        if (isset($opt->scenario) && $opt->scenario) {
            $data['model']->scenario = $opt->scenario;
        }
        return $data;
    }

    public static function CreateJsonLikeCondition($opt = null)
    {
        $opt = (object)array('prefix' => isset($opt['prefix']) ? $opt['prefix'] : 't', 'filter_type' => isset($opt['filter_type']) ? $opt['filter_type'] : 'AND', 'attr' => isset($opt['attr']) ? $opt['attr'] : '', 'data' => isset($opt['data']) ? $opt['data'] : 'AND');
        $attr = $opt->attr;
        $prefix = $opt->prefix . '.';
        $condition = $prefix . $attr . ' LIKE ';
        if (is_array($opt->data)) {
            $type = $opt->filter_type;
            $i = 1;
            foreach ($opt->data as $key => $value) {
                if ($i == 1) {
                    $condition .= '"%\"' . $value . '\"%" ';
                } else {
                    $condition .= $type . ' ' . $prefix . $attr . ' LIKE "%\"' . $value . '\"%" ';
                }
                $i++;
            }
        } else {
            $condition .= '"%\"' . $opt->data . '\"%"';
        }
        return $condition;
    }

    public static function getMaxCode($tbl_name)
    {
        $model = $tbl_name::model()->find(array('select' => 'code', 'order' => 'code DESC'));
        $return = 0;
        if ($model) {
            $return = $model->code + 1;
        } else {
            $return = 100;
        }
        return $return;
    }

    public static function getPageUrl($opt = null)
    {
        $opt = self::convertToObjectArray($opt);
        $opt = (object)array(
            'action' => isset($opt->action) && $opt->action ? $opt->action : '',
            'slug' => isset($opt->slug) && $opt->slug ? $opt->slug : '',
            'slug_id' => isset($opt->slug_id) ? $opt->slug_id : true,
            'base' => isset($opt->base) ? $opt->base : false,
            'params' => isset($opt->params) && $opt->params ? $opt->params : array(),
        );
        // Clean site action
        $opt->action = str_replace('site', '', $opt->action);
        $slug = $opt->slug && $opt->slug_id ? 'id/' : '';
        $url = $opt->action . '/' . $slug . $opt->slug;

        $params = self::convertObjectToArray($opt->params);

        if ($opt->base) {
            $url = Yii::app()->createAbsoluteUrl($url, $params, 'https');
        } else {
            $url = Yii::app()->createUrl($url, $params);
        }

        return $url;
    }

    public static function convertObjectToArray($data)
    {
        $data = json_encode($data);
        return json_decode($data, true);
    }

    public static function convertPostVideoGalleryToJsonArray($post)
    {
        $return = array();
        $length = count($post['type']);
        for ($i = 0; $i < $length; $i++) {
            if ($post['url'][$i] != '') {
                $return[$i] = array('type' => $post['type'][$i], 'url' => $post['url'][$i]);
            }
        }
        $return = json_encode($return);
        return $return != '[]' ? $return : '';
    }

    public static function arrayPriceSave($price)
    {
        $return = array();
        $length = count($price['country']);
        for ($i = 0; $i < $length; $i++) {
            if ($price['currency'][$i] != '' && $price['cost'][$i] != '') {
                $return[$price['country'][$i]] = array($price['currency'][$i] => $price['cost'][$i]);
            }
        }
        $return = json_encode($return);
        return $return != '[]' ? $return : '';
    }

    public static function ChangeActiveInTable($opt = null)
    {
        $response = array('status' => 'OK', 'msg' => '',);
        $opt = self::convertToObjectArray($opt);
        $opt = (object)array('id' => isset($opt->id) ? $opt->id : '', 'tbl_name' => isset($opt->tbl_name) ? $opt->tbl_name : '', 'active' => isset($opt->active) ? $opt->active : 0,);
        $tbl = $opt->tbl_name;
        if ($opt->id && $opt->tbl_name) {
            $model = $tbl::model()->findByPk($opt->id);
            $model->active = $opt->active;
            if (!($model->save())) {
                $response['status'] = 'ERROR';
                $response['msg'] = 'Model nije sacuvan.';
            }
        } else {
            $response['status'] = 'ERROR';
            $response['msg'] = 'Greska.';
        }
        return $return;
    }

    public static function getArrayTblCategoriesSubCategoriesWithTitleMainCategories($opt = null)
    {
        $opt = self::convertToObjectArray($opt);
        $opt = (object)array('data' => isset($opt->data) ? $opt->data : array());
        $return = array();
        $return[''] = Yii::t('main_page', 'Select');
        if ($opt->data) {
            foreach ($opt->data as $key => $value) {
                $mSubCateogory = clsLoad::findTblCategoriesByCode($key);
                $code_main_categories = json_decode($mSubCateogory->code_main_categories, true);
                foreach ($code_main_categories as $k => $code) {
                    $title_main = clsLoad::findTblCategoriesByCode(array('code' => $code))->title;
                    $return[$key] = $title_main . '/' . $value;
                }
            }
        }
        return $return;
    }

    /**     * Connect post data with model attributes     */
    public static function convertPostValueToLangPostData($post_data)
    {
        $model = array();
        foreach ($post_data as $attr => $val) {
            foreach ($val as $lang => $value) {
                $model[$lang][$attr] = $value;
            }
        }
        return $model;
    }

    /**     * Connect post data with model attributes     */
    public static function connectArrayValueWithModelAttributes($opt = NULL)
    {
        $opt = $opt ? (object)$opt : (object)array();
        $opt = (object)array('model' => isset($opt->model) ? $opt->model : array(), 'data' => isset($opt->data) ? $opt->data : array());
        $data = is_array($opt->data) ? (object)$opt->data : $opt->data;
        $model = $opt->model;
        foreach ($data as $key => $val) {
            if (property_exists($model, $key)) {
                $model->$key = $data->$key;
            } else if ($model->hasAttribute($key)) {
                if (is_string($key) && !(is_array($data->$key))) {
                    if ($data->$key === "_empty_" && $data->$key !== 0) {
                        $model->$key = "";
                    } else {
                        $model->$key = trim($data->$key);
                    }
                } else {
                    $model->$key = $data->$key;
                }
            }
        }
        return $model;
    }

    /**     * Convert recived post model array to array example: from Partner[name] => Zeljko to name => Zeljko     */
    public static function convertPostModelToArray($arr_data)
    {
        $arr = array();
        foreach ($arr_data as $key => $val) {
            $val = (object)$val;
            $matches = explode('[', $val->name);
            $rez = isset($matches[1]) ? $matches[1] : $matches[0];
            if (substr($rez, -1) == "]") {
                $rez = substr($rez, 0, -1);
            }
            if (isset($arr[$rez])) {
                if (is_array($arr[$rez])) {
                    $arr[$rez][count($arr[$rez])] = $val->value;
                } else {
                    $arr[$rez] = array($arr[$rez], $val->value);
                }
            } else
                $arr[$rez] = $val->value;
        }
        return $arr;
    }

    /**     * Check if data is JSON type     */
    public static function isJSON($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }

    /**     * Check if data is serialized     */
    public static function isSerialized($data)
    {
        return (@unserialize($data) !== false);
    }

    /**     * Sorting multidimension array by specifed index array     */
    public static function msort($array, $key, $sort_flags = SORT_REGULAR)
    {
        if (is_array($array) && count($array) > 0) {
            if (!empty($key)) {
                $mapping = array();
                foreach ($array as $k => $v) {
                    $sort_key = '';
                    if (!is_array($key)) {
                        $sort_key = $v[$key];
                    } else {
                        foreach ($key as $key_key) {
                            $sort_key .= $v[$key_key];
                        }
                        $sort_flags = SORT_STRING;
                    }
                    $mapping[$k] = $sort_key;
                }
                asort($mapping, $sort_flags);
                $sorted = array();
                foreach ($mapping as $k => $v) {
                    $sorted[] = $array[$k];
                }
                return $sorted;
            }
        }
        return $array;
    }

    /**     * Convert any data to object array     */
    public static function convertToObjectArray($obj, $det = ',')
    {
        $t_arr = $obj;
        switch ($obj) {
            case self::isJSON($obj):
                $t_arr = json_decode($obj);
                break;
            case self::isSerialized($obj):
                $t_arr = unserialize($obj);
                break;
            case is_string($t_arr):
                $temp_arr = explode($det, $obj);
                $t_arr = array();
                foreach ($temp_arr as $k => $v) {
                    $t_arr[$k] = trim($v);
                }
                break;
            case is_numeric($obj):
                $t_arr = '';
                break;
        }
        if ($t_arr) {
            $t_arr = json_encode($t_arr);
            return json_decode($t_arr);
        } else {
            return false;
        }
    }

    /**     * Convert JSON string to display string with no space in value     */
    public static function convertJSONToStringWithOutSpace($opt = null)
    {
        $opt = self::convertToObjectArray($opt);
        $opt = (object)array('data' => isset($opt->data) ? $opt->data : '', 'separate' => isset($opt->separate) ? $opt->separate : ',');
        $data = self::isJSON($opt->data) ? json_decode($opt->data) : array();
        foreach ($data as $index => $value) {
            $data[$index] = trim($value);
        }
        $data = implode($opt->separate, $data);
        return $data;
    }

    /**     * Convert String to JSON string with no space in value and with no empty value     */
    public static function convertStringToJSON($opt = null)
    {
        $opt = self::convertToObjectArray($opt);
        $opt = (object)array('data' => isset($opt->data) ? $opt->data : '', 'separate' => isset($opt->separate) ? $opt->separate : ',');
        $return = '';
        if ($opt->data && is_string($opt->data)) {
            $return = explode($opt->separate, $opt->data);
            $temp = array();
            foreach ($return as $index => $value) {
                if ($value) {
                    $temp[] = trim($value);
                }
            }
            $return = json_encode($temp);
        }
        return $return;
    }

    /**     * Create array without "_empty_" value     */
    public static function ArrayWithOutEmptyValue($array)
    {
        $return = array();
        if ($array) {
            if (is_array($array)) {
                foreach ($array as $value) {
                    if ($value != "_empty_") {
                        $return[] = $value;
                    }
                }
            } else {
                $return[] = $array;
            }
        }
        return $return;
    }

    public static function DisplayJsonValueCategoriesAdminView($opt = null)
    {
        $return = '';
        $opt = self::convertToObjectArray($opt);
        $opt = (object)array('data' => isset($opt->data) ? json_decode($opt->data, true) : array(), 'lang' => isset($opt->lang) ? $opt->lang : '',);
        if ($opt->data) {
            foreach ($opt->data as $key => $value) {
                if ($value == 0)
                    $return .= '<p>' . Yii::t('default', 'Main category') . '<p><br/>';
                else
                    $return .= '<p>' . clsLoad::getTitleTblCategoriesByCode(array('code' => $value, 'lang' => $opt->lang)) . '<p><br/>';
            }
        }
        return $return;
    }

    public static function ConvertPostProductsAtributesToArrayAttributes($opt = null)
    {
        $return = array();
        $opt = self::convertToObjectArray($opt);
        $opt = (object)array('data' => isset($opt->data) ? $opt->data : array(), 'type_attr' => isset($opt->type_attr) ? $opt->type_attr : array(),);
        $count = 0;
        foreach ($opt->data as $attr => $value) {
            $count = count($value);
        }
        $attributes_array = clsParams::getArrayProductsAttributesByTypeCategories($opt->type_attr);
        if ($count) {
            for ($i = 0; $i < $count; $i++) {
                foreach ($attributes_array as $index => $attr) {
                    $array_attr_value = $opt->data->$attr;
                    if ($array_attr_value[$i])
                        ;
                    $return[$i][$attr] = $array_attr_value[$i];
                }
            }
        }
        $return_final = array();
        foreach ($return as $key => $attributes_value) {
            if ($attributes_value['quantity']) {
                $return_final[] = $return[$key];
            }
        }
        return $return_final;
    }

    public static function getErrorMesageFromModelasString($model, $tbl_name = null)
    {
        $return = array();
        $tbl_name = $tbl_name ? $tbl_name : get_class($model);
        if ($model->errors) {
            foreach ($model->errors as $attr => $error) {
                $return[$tbl_name . '[' . $attr . ']'] = $error[0];
            }
        }
        return $return;
    }

    /**     * <======== /Array ===========>     */
    /**     * <======== String ===========>     */
    /**     * Convert to string to valid programmer name     */
    public static function convertStringToVariable($text)
    {
        $text = strtolower($text);
        $text = trim($text);
        $text = str_replace('   ', ' ', $text);
        $text = str_replace('  ', ' ', $text);
        $text = str_replace(' ', '-', $text);
        $text = str_replace('ć', 'c', $text);
        $text = str_replace('č', 'c', $text);
        $text = str_replace('š', 's', $text);
        $text = str_replace('đ', 'dj', $text);
        $text = str_replace('ž', 'z', $text);
        $text = str_replace('Ž', 'z', $text);
        $text = str_replace('Š', 's', $text);
        $text = str_replace('Č', 'c', $text);
        $text = str_replace('Ć', 'c', $text);
        $text = str_replace('Đ', 'dj', $text);
        $text = str_replace('/', '-', $text);
        $text = str_replace('?', '', $text);
        return $text;
    }

    public static function convertArrayToString($opt)
    {
        $return;
        $opt = (object)array('model' => isset($opt['model']) ? $opt['model'] : '', 'attr' => isset($opt['attr']) ? $opt['attr'] : '');
        $atttr = $opt->attr;
        $model = $opt->model;
        if ($model && $atttr) {
            $return = str_replace('[', "", $model->$atttr);
            $return = str_replace(']', "", $return);
            $return = str_replace('", "', ",", $return);
            $return = str_replace('"', "", $return);
            $return = trim($return);
        }
        return $return ? $return : '';
    }

    /**     * Make slug     */
    public static function makeSlug($text)
    {
        $text = trim($text);
        return self::convertStringToVariable($text) . '-' . time();
    }

    /**     * Parse URL to params     */
    public static function parseStringUrl($url, $cell = null)
    {
        $parts = parse_url($url);
        parse_str($parts['query'], $query);
        $return_cell = $cell && isset($query[$cell]) ? $query[$cell] : false;
        $return = $cell ? $return_cell : $query;
        return $return;
    }

    /**     * Cut string, function don't cut word, cut before word     */
    public static function cutText($description, $max_length = 150)
    {
        if (strlen($description) > $max_length) {
            $description = wordwrap($description, $max_length);
            $description = substr($description, 0, strpos($description, "\n")) . '...';
        }
        return $description;
    }

    /**     * Cut text with out <br/> and "     */
    public static function cutMetaText($description, $max_length = 150)
    {
        $description = strip_tags($description);
        $description = str_replace('"', '', $description);
        $description = self::cutText($description, $max_length);
        return $description;
    }

    /**     * Get random string with letters and numbers     */
    public static function generateRandomString($length = 17)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function displayArrayAsString($array = array(), $key = '', $separator = ', ')
    {
        $return = '';
        if ($array) {
            foreach ($array as $val) {
                if ($key) {
                    $return .= trim($val[$key]) . $separator;
                } else {
                    $return .= trim($val) . $separator;
                }
            }
            $return = substr($return, 0, (strlen($separator) * -1));
        }
        return $return;
    }

    public static function dateIsBetween($from, $to, $date = 'now')
    {
        $date = is_int($date) ? $date : strtotime($date);
        $from = is_int($from) ? $from : strtotime($from);
        $to = is_int($to) ? $to : strtotime($to);
        return ($date > $from) && ($date < $to);
    }

    public static function yearsOld($date)
    {
        $now_year = date('Y');
        $date = date('Y', strtotime($date));
        return $now_year - $date;
    }

    public static function diffDateTimeInMinutes($to_time, $from_time)
    {
        $to_time = strtotime($to_time);
        $from_time = strtotime($from_time);
        return round(abs($to_time - $from_time) / 60, 2);
    }

    /**     * <======== /Date Time ===========>     */
    /**     * <======== File ===========>     */
    /**     * Check if url path to file is valid     */
    public static function isFileUrlExist($url)
    {
        return file_exists($url) ? true : false;
    }

    public static function urlExists($url)
    {
        $url = filter_var($url, FILTER_SANITIZE_URL);
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE || !in_array(strtolower(parse_url($url, PHP_URL_SCHEME)), ['http', 'https'], true)) {
            return false;
        }
        $file_headers = @get_headers($url);
        return !(!$file_headers || strpos($file_headers[0], '404 Not Found') !== false);
    }

    public static function getImages($opt = null)
    {
        $opt = (object)array('data' => isset($opt->data) && $opt->data ? $opt->data : array(), 'setting_category' => isset($opt->setting_category) && $opt->setting_category ? $opt->setting_category : '', 'setting_change_url' => isset($opt->setting_change_url) && $opt->setting_change_url ? $opt->setting_change_url : '', 'url' => isset($opt->url) && $opt->url ? $opt->url : 'getRootUrl',);
        $return = array();
        $change_localhost_to_ip = clsParams::getSettings($opt->setting_change_url);
        $fnc_url = $opt->url;
        $default_image = clsParams::getSettings($opt->setting_category) ? clsParams::getSettings($opt->setting_category) : 'logo.png';
        if ($change_localhost_to_ip) {
            $default_image = clsPath::getDefaultImgUrl($default_image, true);
            $default_image = str_replace('localhost', $change_localhost_to_ip, $default_image);
        } else {
            $default_image = clsPath::getDefaultImgUrl($default_image, true);
        }
        if ($opt->data) {
            switch ($opt->data) {
                case is_array($opt->data):
                case is_object($opt->data):
                    foreach ($opt->data as $img) {
                        $url = '';
                        if ($change_localhost_to_ip) {
                            $url = clsPath::$fnc_url($img, true);
                            $url = str_replace('localhost', $change_localhost_to_ip, $url);
                        } else {
                            $url = clsPath::$fnc_url($img, true);
                        }
                        if (clsFunctions::urlExists($url)) {
                            $return[] = $url;
                        }
                    }
                    break;
                case is_string($opt->data):
                    $url = '';
                    if ($change_localhost_to_ip) {
                        $url = clsPath::$fnc_url($opt->data, true);
                        $url = str_replace('localhost', $change_localhost_to_ip, $url);
                    } else {
                        $url = clsPath::$fnc_url($opt->data, true);
                    }
                    if (clsFunctions::urlExists($url)) {
                        $return[] = $url;
                    }
                    break;
            }
        }
        if (!$return) {
            $return[] = $default_image;
        }
        return $return;
    }

    public static function findImage($model, $img_path, $attr = null)
    {
        $attr = $attr ? $attr : 'images';
        $images = clsFunctions::convertArrayToString(array('model' => $model, 'attr' => $attr));
        $images = (explode(",", $images));
        $images = isset($images[0]) ? $images[0] : clsFunctions::convertArrayToString(array('model' => $model, 'attr' => $attr));
        return CHtml::image($img_path . $images, "", array("style" => "width:100px;height:100px;"));
    }

    /**     * Saving data to Excel file can save to location or download     */
    public static function excelExport($title, $data, $opt)
    {
        $opt = (object)array('creator' => isset($opt['creator']) ? $opt['creator'] : 'PZM', 'title' => isset($opt['title']) ? $opt['title'] : 'PZM - ' . date('d.m.Y'), 'subject' => isset($opt['subject']) ? $opt['subject'] : 'PZM - ' . date('d.m.Y') . '.xls', 'description' => isset($opt['description']) ? $opt['description'] : '', 'keywords' => isset($opt['keywords']) ? $opt['keywords'] : '', 'category' => isset($opt['category']) ? $opt['category'] : '', 'active_sheet_title' => isset($opt['active_sheet_title']) ? $opt['active_sheet_title'] : 'Sheet', 'export_type' => isset($opt['export_type']) ? $opt['export_type'] : 'Excel5', 'save_path_folder' => isset($opt['save_path_folder']) ? $opt['save_path_folder'] : '');
        Yii::import('ext.phpexcel.XPHPExcel');
        $objPHPExcel = XPHPExcel::createPHPExcel();
        $objPHPExcel->getProperties()->setCreator($opt->creator)->setTitle($opt->title)->setSubject($opt->subject)->setDescription($opt->description)->setKeywords($opt->keywords)->setCategory($opt->category);
        $letters = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
        $count_row = 1;
        for ($i = 0; $i < count($title); $i++) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letters[$i] . $count_row, $title[$i]);
        }
        for ($i = 0; $i < count($data); $i++) {
            $count_row++;
            $leter_count = 0;
            foreach ($data[$i] as $val) {
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($letters[$leter_count] . $count_row, $val);
                $leter_count++;
            }
        }
        $objPHPExcel->getActiveSheet()->setTitle($opt->active_sheet_title);
        $objPHPExcel->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="01simple.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $opt->export_type);
        if ($opt->save_path_folder) {
            $objWriter->save($opt->save_path_folder . $opt->subject);
        } else {
            $objWriter->save('php://output');
        }
        return false;
    }

    /**     * Formating folder to zip file     */
    public static function zipFolder($opt = null)
    {
        $opt = (object)array('realpath' => isset($opt['realpath']) ? $opt['realpath'] : '', 'save_path_folder' => isset($opt['save_path_folder']) ? $opt['save_path_folder'] : './', 'save_path_folder_url' => isset($opt['save_path_folder_url']) ? $opt['save_path_folder_url'] : '/', 'file_name' => isset($opt['file_name']) ? $opt['file_name'] : 'zip_' . time() . '.zip');
        $rootPath = realpath($opt->realpath);
        $zip = new ZipArchive();
        $save_path_file = $opt->save_path_folder . $opt->file_name;
        $save_path_file_url = $opt->save_path_folder_url . $opt->file_name;
        $zip->open($save_path_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY);
        foreach ($files as $name => $file) {
            if (!$file->isDir()) {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);
                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
        return $save_path_file_url;
    }

    /**     * Remove all files and folder     */
    public static function removeFolderContent($dir)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? removeFolderContent("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    /**     * Count visitors page in file, no DB need     */
    public static function visitorCount($COUNT_TXT = './visitor_counter.txt')
    {
        if (file_exists($COUNT_TXT)) {
            $fil = fopen($COUNT_TXT, 'r');
            $dat = fread($fil, filesize($COUNT_TXT));
            fclose($fil);
            $homepage = Yii::app()->request->baseUrl . '/';
            $currentpage = $_SERVER['REQUEST_URI'];
            if ($homepage == $currentpage) {
                $fil = fopen($COUNT_TXT, 'w');
                $dat++;
                fwrite($fil, $dat);
                fclose($fil);
            } else {                // dev::p($homepage. ' == '.$currentpage);                // dev::pd('nije');  
            }
        } else {
            $fil = fopen($COUNT_TXT, 'w');
            fwrite($fil, 1);
            fclose($fil);
            $dat = 1;
        }
        return str_pad($dat, 4, "0", STR_PAD_LEFT);
    }

    /**     * <======== /File ===========>     */
    /**     * <======== Cookies ===========>     */
    /** Get IP adress */
    public static function getIP()
    {
        return Yii::app()->request->getUserHostAddress();
    }

    /**     * Get value from cookie     */
    public static function getCookieValue($name)
    {
        $cookie = self::getCookie($name);
        $value = isset($cookie->value) && $cookie->value ? json_decode($cookie->value, true) : '';
        return $value;
    }

    /**     * Get whole data for cookie     */
    public static function getCookie($name)
    {
        if (isset(Yii::app()->request->cookies[$name]) && Yii::app()->request->cookies[$name]) {
            return Yii::app()->request->cookies[$name];
        }
        return false;
    }

    /**     * Set new or update cookie, cookie max load is 5MB     */
    public static function setCookie($opt)
    {
        $opt = self::convertToObjectArray($opt);
        $opt = (object)array('name' => isset($opt->name) && $opt->name ? $opt->name : '', 'value' => isset($opt->value) && $opt->value ? $opt->value : '', 'days' => isset($opt->days) && $opt->days ? $opt->days : '', 'hours' => isset($opt->hours) && $opt->hours ? $opt->hours : 24,
            'disable_client_cookies' => isset($opt->disable_client_cookies) && $opt->disable_client_cookies ? $opt->disable_client_cookies : false);
        $time = time();
        if ($opt->days) {
            $time += 60 * 60 * 24 * $opt->days;
        } else if ($opt->hours) {
            $time += 60 * 60 * $opt->hours;
        }
        $cookie = new CHttpCookie($opt->name, json_encode($opt->value));
        $cookie->expire = $time;
        $cookie->httpOnly = $opt->disable_client_cookies;
        Yii::app()->request->cookies[$opt->name] = $cookie;
        return false;
    }

    /**     * Delete cookie (for using site)     */
    public static function removeCookie($name)
    {
        unset(Yii::app()->request->cookies[$name]);
        return false;
    }

    /**     * Delete all existing cookies (for using site)     */
    public static function clearAllCookie()
    {
        Yii::app()->request->cookies->clear();
        return false;
    }

    /**     * <======== /Cookies ===========>     */
    /**     * <======== Urls ===========>     */
    /**     * Get current whole url     */
    public static function currentPageURL()
    {
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

    public static function getOrderCol($tbl_name, $get)
    {
        $sort = isset($get[$tbl_name . '_sort']) ? $_GET[$tbl_name . '_sort'] : '';
        if ($sort) {
            $sort_arr = explode('.', $sort);
            $sort = 't.' . $sort_arr[0];
            $sort .= isset($sort_arr[1]) ? ' ' . $sort_arr[1] : '';
        }
        return $sort;
    }

    /**     * <======== /Urls ===========>     */
    /**     * <======== Cart ===========>     */
    /**     * Calculate EAN13 barcode, need to send 12 digits and function calculate 13.     * digit     */
    public static function barcodeean13Genarator($num = null)
    {
        if (!$num) {
            $int_ean_1 = rand(1000, 9999);
            $int_ean_2 = rand(1000, 9999);
            $int_ean_3 = rand(1000, 9999);
            $ean_1 = (string)$int_ean_1;
            $ean_2 = (string)$int_ean_2;
            $ean_3 = (string)$int_ean_3;
            $ean = $ean_1 . $ean_2 . $ean_3;
        } else {
            $ean = $num;
        }
        $ean_arr = str_split($ean);
        $sum = 0;
        foreach ($ean_arr as $k => $v) {
            $sum += ($k % 2 > 0) ? $v * 3 : $v;
        }
        $sum_sub = ceil($sum / 10) * 10;
        $ean_suf = $sum_sub - $sum;
        return $ean . $ean_suf;
    }

    /**     * Get item price by product model     */
    public static function getItemPrice($model)
    {
        $return = array('status' => '', 'price' => '', 'total' => '');
        $check_other = true;
        if ($model->sold && $check_other) {
            $return = array('status' => 'sold', 'can_use_item' => 'no', 'display_type' => '', 'use_vat' => false, 'price' => '', 'total' => Yii::t('discounts', 'Sold'));
            $check_other = false;
        }
        if (!$model->stock && $check_other) {
            $return = array('status' => 'out_of_stock', 'can_use_item' => 'no', 'display_type' => '', 'use_vat' => false, 'price' => '', 'total' => Yii::t('discounts', 'Out of stock'));
            $check_other = false;
        }
        if ($model->stock > 0 && $model->stock - clsLoad::getReservedItems(array('item_id' => $model->id)) == 0 && $check_other) {
            $return = array('status' => 'all_reserved', 'can_use_item' => 'no', 'display_type' => '', 'use_vat' => false, 'price' => '', 'total' => Yii::t('discounts', 'Ask for price'));
            $check_other = false;
        }
        if (clsRole::is_vip_member() && $check_other) {
            $return = array('status' => 'vp_price', 'can_use_item' => 'yes', 'display_type' => '', 'use_vat' => true, 'price' => '', 'total' => $model->vip_price);
            $check_other = false;
        }
        if ($model->original_price > 0 && $model->price_with_discount > 0 && $model->original_price != $model->price_with_discount && date('Y-m-d') >= date('Y-m-d', strtotime($model->discount_start)) && date('Y-m-d') <= date('Y-m-d', strtotime($model->discount_end)) && $check_other) {
            $return = array('status' => 'action', 'can_use_item' => 'yes', 'display_type' => 'two_prices', 'use_vat' => true, 'price' => $model->original_price, 'total' => $model->price_with_discount);
            $check_other = false;
        }
        if ($model->original_price > 0 && $model->price_with_discount > 0 && $model->original_price != $model->price_with_discount && date('Y-m-d', strtotime($model->discount_end)) == date('Y-m-d', strtotime($model->discount_start)) && $check_other) {
            $return = array('status' => 'discount', 'can_use_item' => 'yes', 'display_type' => 'two_prices', 'use_vat' => true, 'price' => $model->original_price, 'total' => $model->price_with_discount);
            $check_other = false;
        }
        if ($model->original_price > 0 && $check_other) {
            $return = array('status' => 'original_price', 'can_use_item' => 'yes', 'display_type' => '', 'use_vat' => true, 'price' => $model->original_price, 'total' => $model->original_price);
            $check_other = false;
        }
        if ($model->original_price < 1 && $check_other) {
            $return = array('status' => 'ask_for_price', 'can_use_item' => 'no', 'display_type' => '', 'use_vat' => false, 'price' => '', 'total' => Yii::t('discounts', 'Ask for price'));
            $check_other = false;
        }
        return $return;
    }

    /**     * <======== /Cart ===========>     */
}
