<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;

    public function authenticate() {

        $opt_sql=[
            'username'=>$this->username,
        ];
        $all_user_access=clsLoad::findAllTblUserAccess($opt_sql);
        $model_user_access=null;
        if($all_user_access){
            foreach ($all_user_access as $model){
                $password=$model->salt.$this->password;
                $password=md5($password);
                if($model->password===$password){
                    $model_user_access=$model;
                    break;
                }
            }
        }
        if ($model_user_access === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {
            $model_users=$model_user_access->rel_users;
            $model_roles=$model_user_access->rel_roles;

                $this->_id = $model_users->id;
                Yii::app()->user->setState('first_name', $model_users->first_name);
                Yii::app()->user->setState('last_name', $model_users->last_name);
                Yii::app()->user->setState('full_name', $model_users->get_full_name());
                Yii::app()->user->setState('id_roles', $model_roles->id);

                $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    /**
     * Returns id of current user identity.
     */
    public function getId() {
        return $this->_id;
    }

}
