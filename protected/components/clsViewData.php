<?php

class clsViewData {

    public static function gridCitiesColumns($opt) {
        $return = array();
        $ajax_chk = array();

        if ($opt->view->ajax_enable) {
            $ajax_chk = array(
                'name' => 'check',
                'id' => $opt->grid->checkbox_id,
                'value' => '$data->' . $opt->grid->grid_id_value,
                'class' => 'CCheckBoxColumn',
                'selectableRows' => 2,
            );
        }

        $return_arr = array(
            array(
                'name' => 'id',
            ),
            array(
                'name' => 'name',
            ),
            array(
                'name' => 'id_countries',
                'value' => '$data->print_countries()',
            ),
            array(
                'name' => 'description',
                'type' => 'raw',
            ),
            array(
                'name' => 'create_date',
                'value' => 'clsDataFormat::displayDateTime($data->create_date)',
            ),
            array(
                'class' => 'CButtonColumn',
                'template' => '{edit}',
                'buttons' => array
                (
                    'edit' => array
                    (
                        'options' => array('class' => "col-md-12"),
                        'label' => '<span title="Edit" class="btn btn-warning"><i class="fa fa-pencil-square-o"></i></span>',
                        'url' => 'Yii::app()->createUrl("admin/citiesedit", array("id"=>$data->id))',
                    ),
                ),
            ),
        );
        if ($ajax_chk) {
            $return[] = $ajax_chk;
        }

        foreach ($return_arr as $row) {
            $return[] = $row;
        }

        return $return;
    }

}
