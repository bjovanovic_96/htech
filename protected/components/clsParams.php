<?php

class clsParams
{
    public static function getSystemOptions($key = null)
    {
        $return = (object)array(
            'company' => (object)[
                'logo' => clsPath::getDefaultImgUrl('company_logo.png'),
                'name' => 'Test site'
            ],
            'meta' => (object)[
                'author' => '',
                'site_url' => '',
                'site_name' => '',
                'site_title' => 'Test site',
                'keywords' => '',
                'description' => '',
            ],
            'image' => (object)[
                'alt' => 'Test site'
            ],
            'email' => (object)[
                'admin' => 'admin@admin.com'
            ],
        );
        if ($key) {
            eval('$return=$return->' . $key . ';');
        }
        return $return;
    }

    public static function getFileTime()
    {
        return time();
    }

    public static function getMaxPageSize()
    {
        $return = 99999;
        return $return;
    }

    public static function getArrayTblInfo()
    {
        return array(
            'TblCities' => (object)array(
                'admin_create_title' => 'Add city',
                'admin_update_title' => 'Edit city',
                'short' => 'cities'
            ),
        );
    }

    public static function getSelectedLang()
    {
        return Yii::app()->language;
    }

}

?>