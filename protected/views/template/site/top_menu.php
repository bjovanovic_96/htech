<?php
$data = (object)[
    'action' => Yii::app()->controller->action->id,
    'model_users' => clsRole::get_user_id()
];
?>
<header class="header">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Test site</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="<?= $data->action == "index" ? "active" : ""; ?>"><a href="<?= clsUrls::getHomeUrl(); ?>">Cities</a>
                </li>
                <li class="<?= $data->action == "routes" ? "active" : ""; ?>"><a href="<?= clsUrls::getRoutesUrl(); ?>">Routes</a></li>
            </ul>
            <?php if(clsRole::is_login()){ ?>
            <ul class="nav navbar-nav navbar-right">
                <?php if(clsRole::is_admin()){
                    ?>
                    <li><a href="<?= clsUrls::getAdminCitiesUrl(); ?>"><span class="glyphicon glyphicon-user"></span><?= Yii::app()->user->full_name ?></a>
                    </li>
                    <?php
                }
                if(clsRole::is_user()){
                    ?>
                    <li>
                        <a href="<?= clsUrls::getHomeUrl(); ?>">
                            <span class="glyphicon glyphicon-user"></span>
                        <?= Yii::app()->user->full_name ?>
                        </a>
                    </li>
                    <?php
                }
                ?>

                <li><a href="<?= clsUrls::getLogoutUrl(); ?>"><span class="glyphicon glyphicon-log-in"></span>
                        Logout</a></li>
            </ul>
            <?php }else{
                ?>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?= clsUrls::getLoginUrl(); ?>"><span class="glyphicon glyphicon-log-in"></span>
                            Login</a></li>
                </ul>
                <?php
            } ?>
        </div>
    </nav>
</header>
