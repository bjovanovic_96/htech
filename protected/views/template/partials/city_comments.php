<?php
$data = (object)[
    'all_city_comments' => isset($data['all_city_comments']) && $data['all_city_comments'] ? $data['all_city_comments'] : false,
    'model_city_comments' => isset($data['model_city_comments']) && $data['model_city_comments'] ? $data['model_city_comments'] : false,
    'number_comments' => isset($data['number_comments']) && $data['number_comments'] ? $data['number_comments'] : 0,
];
if(!$data->number_comments && $data->all_city_comments){
    $data->number_comments=count($data->all_city_comments);
}
?>
<div class="comments">
    <div class="row">
        <div class="col-md-8">
            <h5>Number comments:</h5>
        </div>
        <div class="col-md-4">
            <input class="form-control" name="number_comments" value="<?= $data->number_comments; ?>" type="number"/>
        </div>
    </div>
    <?php if ($data->all_city_comments) {
        foreach ($data->all_city_comments as $model_city_comments) {
            $model_users = $model_city_comments->rel_users;
            ?>
            <div class="thumbnail">
                <div class="js-comment_row"
                     data-id_city_comments="<?= $model_city_comments->id; ?>"
                >
                    <div class="row">
                        <div class="col-md-8 text-left">
                            <b><?= $model_users->get_full_name(); ?></b>
                        </div>
                        <div class="col-md-4">
                            <span class="comment_date">created: <?= clsDataFormat::displayDateTime($model_city_comments->create_date); ?></span>
                            <?php if ($model_city_comments->update_date) { ?>
                                <span class="comment_date">updated: <?= clsDataFormat::displayDateTime($model_city_comments->update_date); ?></span>
                            <?php } ?>
                        </div>
                        <div class="col-md-12">
                            <p><?= $model_city_comments->comment; ?></p>
                        </div>
                        <?php if ($model_city_comments->id_users == clsRole::get_user_id()) { ?>
                            <div class="col-md-12 text-right">
                                <button class="btn btn-success fa fa-edit js-edit"></button>
                                <button class="btn btn-danger fa fa-trash-o js-delete"
                                        data-confirm_text="Are you sure you want to delete the comment?"
                                ></button>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>

        <?php
    } else {
        ?>
        <p>No comments</p>
        <?php
    } ?>
    <div class="form js-form"
         data-id_city_comments="<?= $data->model_city_comments ? $data->model_city_comments->id : ''; ?>">
        <div class="form-group">
            <label>Comment</label>
            <textarea class="form-control" name="comment"
                      placeholder="Insert comment"><?= $data->model_city_comments ? $data->model_city_comments->comment : ''; ?></textarea>
            <div class="text-right">
                <button class="fa fa-check btn btn-success js-edit"></button>
            </div>
        </div>
    </div>
</div>
</div>
