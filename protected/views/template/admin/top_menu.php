<div class="nav_menu">
    <nav class="" role="navigation">
        <div class="nav toggle">
            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
        </div>
        <ul class="nav navbar-nav navbar-right">        
            <li class="">
                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?= Yii::app()->user->full_name; ?>
                    <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                    <li>
                        <a href="<?= clsUrls::getLogoutUrl(); ?>" class="js_display_loader"><i class="fa fa-sign-out pull-right"></i>Logout</a>
                    </li>
                </ul>
            </li>
            <li role="presentation" class="dropdown">
                <a href="<?= clsUrls::getHomeUrl(); ?>" target="_blank" class="dropdown-toggle info-number">
                    <i class="fa fa-globe"></i> <?= Yii::t('main_admin', 'site') ?>
                </a>
            </li>
        </ul>
    </nav>
</div>
