<div class="left_col scroll-view">

    <div class="navbar nav_title" style="border: 0;">
        <a href="#" class="site_title">
            <i class="logo-main"></i> <span>Admin Panel</span></a>
    </div>
    <div class="clearfix"></div>

    <div class="profile">
        <div class="profile_info">
            <span>Welcome</span>
            <h2><?= Yii::app()->user->first_name.' '.Yii::app()->user->last_name; ?></h2>
        </div>
    </div>
    <br />
    <div class="clearfix"></div>

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <ul class="nav side-menu">
                <li><a href="<?php echo clsUrls::getAdminCitiesUrl(); ?>"><i class="fa fa-bank"></i>Cities<span class="fa fa-chevron-down"></span></a>
                </li>
            </ul>
        </div>
    </div>
</div>
