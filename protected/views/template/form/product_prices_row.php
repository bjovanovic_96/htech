<?php
$data = (object) [
            'model_product_prices' => isset($data['model_product_prices']) ? $data['model_product_prices'] : false,
            'model_products' => isset($data['model_products']) ? $data['model_products'] : false,
];
$arr_discounts = clsParams::getDiscounts("arr");
$all_stores = clsLoad::findAllTblStores();
$arr_currencies = clsLoad::getArrayTblCurrencies();
if ($data->model_product_prices->isNewRecord) {
    ?>
    <div class="form-group row js-edit-product-prices-row">
        <input type="hidden" name="id_products" value="<?= $data->model_products->id; ?>"/>
        <div class="col-md-2 col-xs-3">
            <select class="form-control" name="id_stores">
                <option value="">Odaberite Radnju</option>
                <?php
                if ($all_stores) {
                    foreach ($all_stores as $model_stores) {
                        ?>
                        <option value="<?= $model_stores->id; ?>"><?= $model_stores->title; ?></option>
                        <?php
                    }
                }
                ?>
                <option value="web">Web shop</option>
            </select>
        </div>
        <div class="col-md-2 col-xs-3">
            <input type="number" name="price" placeholder="Cena" step="any" class="form-control js-product-price"/>
        </div>
        <div class="col-md-2 col-xs-3">
            <select class="form-control js-product-discount" name="discount">
                <option value="">Odaberite popust</option>
                <?php foreach ($arr_discounts as $val => $text) { ?>
                    <option value="<?= $val; ?>"><?= $text; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-md-2 col-xs-3">
            <select class="form-control js-product-addcount" name="addcount">
                <option value="">Odaberite povećanje</option>
                <?php foreach ($arr_discounts as $val => $text) { ?>
                    <option value="<?= $val; ?>"><?= $text; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-md-2 col-xs-4">
            <input type="number" name="total_price" placeholder="Prodajna cena" step="any" class="form-control js-product-total-price" readonly="true"/>
        </div>
        <div class="col-md-1 col-xs-4">
            <select class="form-control" name="id_currencies">
                <?php foreach ($arr_currencies as $val => $text) { ?>
                    <option value="<?= $val; ?>"><?= $text; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-md-1 col-xs-4">
            <button class="btn btn-success js-edit" data-url="<?= clsUrls::getAjaxEditProductPricesUrl(); ?>" data-confirm-text="Da li ste sigurni da želite da izmenite cenu za proizvod <?= $data->model_products->print_title; ?> za {store}?">
                <span class="fa fa-check"></span>
            </button>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="form-group row js-edit-product-prices-row">
        <input type="hidden" name="id_product_prices" value="<?= $data->model_product_prices->id; ?>"/>
        <input type="hidden" name="id_products" value="<?= $data->model_products->id; ?>"/>
        <div class="col-md-2 col-xs-3">
            <select class="form-control" name="id_stores">
                <option value="">Odaberite Radnju</option>
                <?php
                $selected_stores = $data->model_product_prices->id_stores;
                if ($data->model_product_prices->type == "web") {
                    $selected_stores = "web";
                }
                if ($all_stores) {
                    foreach ($all_stores as $model_stores) {
                        ?>
                        <option value="<?= $model_stores->id; ?>" <?= $selected_stores == $model_stores->id ? "selected" : ""; ?>><?= $model_stores->title; ?></option>
                        <?php
                    }
                }
                ?>
                <option value="web" <?= $selected_stores == "web" ? "selected" : ""; ?>>Web shop</option>
            </select>
        </div>
        <div class="col-md-2 col-xs-3">
            <input type="number" name="price" placeholder="Cena" value="<?= $data->model_product_prices->price; ?>" step="any" class="form-control js-product-price"/>
        </div>
        <div class="col-md-2 col-xs-3">
            <select class="form-control js-product-discount" name="discount">
                <option value="">Odaberite popust</option>
                <?php
                $selected_discount = $data->model_product_prices->discount;
                foreach ($arr_discounts as $val => $text) {
                    ?>
                    <option value="<?= $val; ?>" <?= $selected_discount == $val ? "selected" : ""; ?>><?= $text; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-md-2 col-xs-3">
            <select class="form-control js-product-addcount" name="addcount">
                <option value="">Odaberite povećanje</option>
                <?php
                $selected_addcount = $data->model_product_prices->addcount;
                foreach ($arr_discounts as $val => $text) {
                    ?>
                    <option value="<?= $val; ?>" <?= $selected_addcount == $val ? "selected" : ""; ?>><?= $text; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-md-2 col-xs-4">
            <input type="number" name="total_price" placeholder="Prodajna cena" step="any" value="<?= $data->model_product_prices->total_price; ?>" class="form-control js-product-total-price" readonly="true"/>
        </div>
        <div class="col-md-1 col-xs-4">
            <select class="form-control" name="id_currencies">
                <?php
                $selected_currencies = $data->model_product_prices->id_currencies;
                foreach ($arr_currencies as $val => $text) {
                    ?>
                    <option value="<?= $val; ?>" <?= $selected_currencies == $val ? "selected" : ""; ?>><?= $text; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-md-1 col-xs-4">
            <button class="btn btn-danger js-delete" data-url="<?= clsUrls::getAjaxDeleteProductPricesUrl(); ?>" data-id="<?= $data->model_product_prices->id; ?>" data-confirm-text="Da li ste sigurni da želite da obrišete cenu?">
                <span class="fa fa-minus"></span>
            </button>
            <button class="btn btn-success js-edit" data-url="<?= clsUrls::getAjaxEditProductPricesUrl(); ?>" data-confirm-text="Da li ste sigurni da želite da izmenite cenu za proizvod <?= $data->model_products->print_title; ?> za {store}?">
                <span class="fa fa-check"></span>
            </button>
        </div>
    </div>
<?php }
?>