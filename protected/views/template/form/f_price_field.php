<?php
$options= array(
    // 'class_error' => 'pull-right allert alert-danger',
    'htmlOptions' => array(
        'class' => 'form-control'
    ),
    'lang'=>$opt->lang,
    'label_text' => Yii::t('TblProducts', 'country'),
    'htmlOptions'  => array('class' => 'form-control country-select-control'),
    'attr_array' => '[country][]',
    'value' =>$opt->value['country'],
);

//$return = clsForm::getInputName($model, $attr, $opt, 'ddl');

switch ($otp->display_type) {
    case 'inline':
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    <?php
                    $data = array(
                        'form' => $form,
                        'model' => $model,
                        'attr' => $attr,
                        'opt' => $opt);
                    $that->renderPartial('//template/form/f_lable_field', $data);
                    ?>
                </div>
                <div class="col-md-8">
                    <?php
                    if ($opt->form_type == 'multi') {
                        echo $form->dropDownList($model, $return->attr, $opt->value, $return->htmlOptions);
                    }
                    if ($opt->form_type == 'single') {
                        echo $form->dropDownList($model, $return->attr, $opt->value, $opt->htmlOptions);
                    }
                    ?> 
                </div>
            </div>
        </div>
        <?php break; ?>
    <?php
    default :
        ?>

        <div class="form-group" >
        <div class="form-group price-multiple-form-holder" data-db-data='<?= $model->$attr; ?>'>
        <div class="form-group multiple-form-group price-multiple-form">
        <span class="input-group-btn">
                        <button type="button" class="btn btn-success btn-add" style="float: right;">+</button>
                    </span>
           <?php clsForm::DDLGroup($that, $form, $model, $attr, $options);
           
           $options['attr_array']='[currency][]';
           $options['value']=$opt->value['currency'];
           $options['label_text'] = Yii::t('TblProducts', 'currency');
           $options['htmlOptions']  = array('class' => 'form-control currency-select-control');
           clsForm::DDLGroup($that, $form, $model, $attr, $options);
           
           $options['attr_array']='[cost][]';
           $options['label_text'] = Yii::t('TblProducts', 'cost');
           $options['htmlOptions']  = array('class' => 'form-control cost-select-control');
           clsForm::TextFieldGroup($that, $form, $model, $attr, $options);
           ?>

        </div>
        </div>
        </div>

        <?php
        break;
}
?>