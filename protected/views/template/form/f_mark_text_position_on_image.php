


<script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/admin/croppic_text_position_admin.js"></script>

<?php
$return = clsForm::getInputName($model, $attr, $opt);

$return->htmlOptions['value'] = $return->htmlOptions['value']?json_decode($return->htmlOptions['value']):array();

$opt->width = $opt->width ? $opt->width : 500;
$opt->height = $opt->height ? $opt->height : 500;

$opt->image_url = $opt->image_url ? $opt->image_url : clsPath::getThumbsUrl();
$file_name = isset($return->htmlOptions['value']->file) && $return->htmlOptions['value']->file ? $return->htmlOptions['value']->file : '';
$opt->file_name = $opt->file_name ? $opt->file_name : $file_name;

$opt->lable_text = Yii::t(get_class($model), $attr);


$top = isset($return->htmlOptions['value']->top) && $return->htmlOptions['value']->top ? $return->htmlOptions['value']->top : '0px';
$left = isset($return->htmlOptions['value']->left) && $return->htmlOptions['value']->left ? $return->htmlOptions['value']->left : '0px';
$width = isset($return->htmlOptions['value']->width) && $return->htmlOptions['value']->width ? $return->htmlOptions['value']->width : '0px';
$height = isset($return->htmlOptions['value']->height) && $return->htmlOptions['value']->height ? $return->htmlOptions['value']->height : '0px';

//dev::p($return);
?>


<style>
    div.resizable {
        width: 300px;
        height: 200px;
        padding: 0px;
        z-index: 9999;
        border: 1px solid black;
        position: absolute;
    }

    #holder_<?= $return->input_id; ?> .cropControls {
        z-index: 17;
        /** margin: -50px auto 0px auto;*/
        position: relative;
        /* height: 297px; */
        /* width: 100%; */
        width: <?= $opt->width; ?>px;
        display: block;
        top: 0px;
        left: 0px;
        font-family: sans-serif;
        background-color: rgba(0,0,0,0.35);
    }

    #holder_<?= $return->input_id; ?> .crop-btn-controls {
        display: inline-block;
        margin-left: <?= ($opt->width - 266) / 2; ?>px;
    }

    .cropHeaderWrapper .cropImgWrapper {
        overflow:hidden; 
        z-index:1; 
        position:absolute; 
        margin-top: 50px; 
        border: 1px solid #5cb85c; 
        box-shadow: 0px 0px 10px 1px #4CAF50; 
    }

</style>


<?php
switch ($otp->display_type) {
    case 'inline':

        break;

    default :
        ?>
        <div id="holder_<?= $return->input_id; ?>">
            <div>
                <?php
                if ($opt->form_type == 'multi') {
                    $same_attr_name = $attr . '_same';
                    $same_attr = $opt->lang ? '[' . $same_attr_name . ']' . '[' . $opt->lang . ']' : '[' . $same_attr_name . ']';
                    $same_field_name = get_class($model) . $same_attr . $opt->attr_array;
                    ?>
                    <label class="pull-right same-form-data-btn">
                        <input type="checkbox" title="Podaci kao sa prve forme" name="<?= $same_field_name; ?>" <?= $model->$same_attr_name ? 'checked="checked"' : ""; ?>/>
                    </label>
                <?php } ?>

                <label class="col-md-12">
                    <!-- Image crop - Start -->
                    <div class="error_holder hide" tabindex='15'></div> 

                    <label for="cropContainerHeaderButton_<?= $return->input_id; ?>"><?= $opt->lable_text; ?> <span class="required">*</span></label>

                    <?php if ($form->error($model, $attr)) { ?>
                        <label class="pull-right label-error-holder <?= $opt->class_error; ?>">
                            <? pph //echo $form->error($model, $attr); ?>
                            <?= Yii::t('main', 'required_field'); ?>
                        </label>
                    <?php } ?>

                </label>

                <div class="col-md-12">            
                    <div class="input-group image-preview image-form" 
                         id="image-preview-crop-<?= $return->input_id; ?>" 
                         data-url="<?= Yii::app()->createUrl("ajax/ajaxcropfile"); ?>"
                         data-file-name="<?= $opt->file_name; ?>"
                         data-folder-path-name="<?= $opt->folder_path_name; ?>"
                         data-folder-url-name="<?= $opt->folder_url_name; ?>"
                         >

                        <div class="input-group-btn input-group-select">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="multiple-concept-img"><img src="<?= $opt->file_name ? $opt->image_url . $opt->file_name : '/img/icons/photo.jpg'; ?>" /></span> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <img class="multiple-concept-img-holder" src="<?= $opt->file_name ? $opt->image_url . $opt->file_name : '/img/icons/photo.jpg'; ?>" />
                                </li>
                            </ul>
                        </div>

                        <input type="text" class="form-control image-preview-filename" value="<?= $opt->file_name; ?>" disabled="disabled" /> <!-- don't give a name === doesn't send on POST/GET -->
                        <span class="input-group-btn">
                            <!-- image-preview-clear button -->
                            <button type="button" class="btn btn-default image-preview-clear blok" style="display:none;">
                                <span class="glyphicon glyphicon-remove"></span> Obriši
                            </button>
                            <!-- image-preview-input -->
                            <div class="btn btn-default image-preview-input-crop" id="cropContainerHeaderButton_<?= $return->input_id; ?>">
                                <span class="glyphicon glyphicon-folder-open"></span>
                                <span class="image-preview-input-crop-title">Izaberi</span>
                                <input type="hidden" name="<?= $return->input_name; ?>[file]" value="<?= $opt->file_name; ?>" id="<?= $return->input_id; ?>-member-image" /> 

                                <input type="hidden" name="<?= $return->input_name; ?>[top]" value="<?= $top; ?>" class="tpoi_top" />
                                <input type="hidden" name="<?= $return->input_name; ?>[left]" value="<?= $left; ?>" class="tpoi_left" />
                                <input type="hidden" name="<?= $return->input_name; ?>[width]" value="<?= $width; ?>" class="tpoi_width" />
                                <input type="hidden" name="<?= $return->input_name; ?>[height]" value="<?= $height; ?>" class="tpoi_height" />
                                <input type="hidden" name="<?= $return->input_name; ?>[img_width]" value="<?= $opt->width; ?>px" class="tpoi_img_width" />
                                <input type="hidden" name="<?= $return->input_name; ?>[img_height]" value="<?= $opt->height; ?>px" class="tpoi_img_height" />
                                <!-- input type="hidden" name="<?= $return->input_name; ?>[font-size]" value="<?= $opt->file_name; ?>" class="tpoi_font-size"/>
                                <input type="hidden" name="<?= $return->input_name; ?>[text-align]" value="<?= $opt->file_name; ?>" class="tpoi_text-align"/ -->
                            </div>
                        </span>
                    </div>
                </div>
                <!-- Image crop - End -->
            </div>
            <!-- Modal - Start -->
            <div id="modal-<?= $return->input_id; ?>" class="modal fade in">
                <div class="modal-dialog" style="width: 80%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Slika <a data-dismiss="modal" class="fa fa-times pull-right x-xross"></a></h4>
                        </div>
                        <div class="modal-body" style="height: <?= $opt->height + 120; ?>px;">
                            <div class="row">
                                <!-- Tooltip - Start -->
                                <div class="po-markup text-center">
                                    <div class="po-link" data-toggle="popover" data-original-title="" title="">
                                        <i class="fa fa-info-circle"></i>  Kako da pripremim fotografiju?</div>
                                    <div class="po-content hidden">
                                        <div class="po-body">
                                            <p><strong>1.</strong> Pozicionirajte fotografiju na deo koji želite da se vidi</p>
                                            <p><strong>2.</strong> Sliku možete pomeriti držanjem levog klika miša ili prevlačenjem prsta preko slike ukoliko koristite mobilni telefon ili tablet</p>
                                            <p><strong>3.</strong> Paleta sa alatkama za rad sa slikom se nalazi iznad fotografije</p>
                                            <p><strong>4.</strong> Ukoliko je potrebno da izvršite neke izmene to možete uraditi na sledeći način:<br />
                                                Za uvećavanje i smanjenje fotografije koristite 
                                                <i class='fa fa-plus-square color-blue-pzm'></i>, 
                                                <i class='fa fa-plus color-blue-pzm'></i>, 
                                                <i class='fa fa-minus color-blue-pzm'></i>, 
                                                ili 
                                                <i class='fa fa-minus-square color-blue-pzm'></i>.
                                                <br />
                                                Za rotiranje fotografije koristite <i class='fa fa-repeat color-blue-pzm'></i> ili 
                                                <i class='fa fa-undo color-blue-pzm'></i></p>
                                            <p><strong>5.</strong> Kada završite sa svim izmenama pritisnite dugme "Sačuvaj"</p>
                                            <p><strong>6.</strong> Uklanjanje fotografije se vrši klikom na dugme "Odustani"</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Tooltip - End -->

                                <div class="col-lg-12 cropHeaderWrapper">
                                    <?php //echo (!empty($model->$attr)) ? CHtml::image($opt->image_url . $model->$attr, "", array("style" => "width:100px")) : "";  ?>
                                    <div id="croppic_<?= $return->input_id; ?>" style="width: <?= $opt->width; ?>px; height: <?= $opt->height; ?>px; margin: auto;"></div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success pull-right btn-modal-crop-image">Sačuvaj sliku</button>
                            <button type="button" class="btn btn-default pull-right btn-modal-reset-image">Odustani</button>
                            
                            <button type="button" class="btn btn-default pull-right btn-modal-crop-text-position display-none">Sačuvaj poziciju teksta</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal - End -->

            <script>
                /** Call crop image js plugin - Start */
                var croppicHeaderOptions = {
                    cropUrl: $('#image-preview-crop-<?= $return->input_id; ?>').attr('data-url'),
                    file_name: $('#image-preview-crop-<?= $return->input_id; ?>').attr('data-file-name'),
                    folder_path_name: $('#image-preview-crop-<?= $return->input_id; ?>').attr('data-folder-path-name'),
                    folder_url_name: $('#image-preview-crop-<?= $return->input_id; ?>').attr('data-folder-url-name'),
                    customUploadButtonId: 'cropContainerHeaderButton_<?= $return->input_id; ?>',
                    modal: false,
                    processInline: true,
                    imgEyecandy: false,
                    modal_id: '#modal-<?= $return->input_id; ?>',
                    holder_id: '#holder_<?= $return->input_id; ?>',
                    member_image_id: '#<?= $return->input_id; ?>-member-image',
                    //width: $(holder_<?= $return->input_id; ?>).attr('data-width'),
                    //height: $(holder_<?= $return->input_id; ?>).attr('data-height'),

                    //loaderHtml: '<div class="loader bubble-loader-holder"><div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div></div>'
                    loaderHtml: '<div id="loader-wrapper" class="loader"><div id="loader"></div></div>'
                };
                var croppic_<?= $return->input_id; ?> = new CroppicTextPosition('croppic_<?= $return->input_id; ?>', croppicHeaderOptions);

                /* Connect modal buttons with croppic plugin */
                /* Call action crop image */
                $(document).on('click', '#modal-<?= $return->input_id; ?> .btn-modal-crop-image', function () {
                    $('#modal-<?= $return->input_id; ?> .cropControls .cropControlCrop').trigger('click');
                });
                /* Call action reset and hide modal */
                $(document).on('click', '#modal-<?= $return->input_id; ?> .btn-modal-reset-image', function () {
                    $('#modal-<?= $return->input_id; ?>').modal('hide');
                });
                /** Call crop image js plugin - End */

                /* Call action reset and hide modal */
                $(document).on('click', '#modal-<?= $return->input_id; ?> .btn-modal-crop-text-position', function () {
                    
                    var $holder = $('#holder_<?= $return->input_id; ?>');
                    
                    var $holder_src = $holder.find('.modal .cropImgWrapper .draggable');
                    var $holder_target = $holder.find('.image-preview-input-crop');
                    
                    $holder_target.find('.tpoi_top').val( $holder_src.css('top') );
                    $holder_target.find('.tpoi_left').val( $holder_src.css('left') );
                    $holder_target.find('.tpoi_width').val( $holder_src.css('width') );
                    $holder_target.find('.tpoi_height').val( $holder_src.css('height') );
                    //$holder_target.find('.tpoi_font-size').val( '' );
                    //$holder_target.find('.tpoi_text-align').val( 'center' );
        
                    $('#modal-<?= $return->input_id; ?>').modal('hide');
                });
                /** Call crop image js plugin - End */
                
            </script>
        </div>
        <?php
        break;
}
?>