

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    //'id' => 'create-news-form-'.$lang,
    'id' => $id_form,
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
?>

<div class="form-holder">

    <div class="col-md-12">
        <button type="submit" class="btn btn-lg btn-success js_display_loader">Sačuvaj</button>
    </div>
    
    <?php
    clsForm::FormFields($this, $form, $data['model']);
    ?>
    
</div>

<?php $this->endWidget(); ?>

