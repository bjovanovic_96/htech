<?php
$return = clsForm::getInputName($model, $attr, $opt);

// Convert to json
if(is_array($return->htmlOptions['value'])){
    $return->htmlOptions['value'] = json_encode($return->htmlOptions['value']);
}

switch ($otp->display_type) {
    case 'inline':

        break;
    default :
        ?>
        <div class="form-group">
            <div class="contacts pdf-multiple-form-holder multiple-form-group" 
                 data-db-data='<?= $return->htmlOptions['value']; ?>' 
                 data-file-remove-url="<?= Yii::app()->createUrl('ajax/ajaxremovefile'); ?>"
                 data-img-path="<?= $opt->folder_path_name; ?>"
                 data-img-url="<?= $opt->folder_url_name; ?>"
                 data-code="<?= $model->code; ?>"
                 data-tbl-name="<?= get_class($model); ?>"
                 data-col-name="<?= $attr; ?>"
                 data-url="<?= clsUrls::getAdminPDFViewerUrl(); ?>"
                 >
                     <?php
                     if ($opt->form_type == 'multi') {
                         $same_attr_name =  'same_'.$attr;
                         $same_attr = $opt->lang ? '[' . $same_attr_name . ']' . '[' . $opt->lang . ']' : '[' . $same_attr_name . ']';
                         $same_field_name = get_class($model) . $same_attr . $opt->attr_array;
                         ?>
                    <label class="pull-right same-form-data-btn">
                        <input type="checkbox" title="Podaci kao sa prve forme" name="<?= $same_field_name; ?>" <?= $model->$same_attr_name ? 'checked="checked"' : ""; ?>/>
                    </label>
                <?php } ?>

                <?php
                // Label
                $data = array(
                    'form' => $form,
                    'model' => $model,
                    'attr' => $attr,
                    'opt' => $opt);
                $that->renderPartial('//template/form/f_lable_field', $data);
                ?>

                <div class="form-group input-group image-multiple-form image-form pdf-multiple-form">
                 <div class="input-group-btn input-group-select">
                   <a href="<?= clsUrls::getAdminPDFViewerUrl(); ?>" target="_blank" data-url="<?= clsUrls::getAdminPDFViewerUrl(); ?>" class="pdf-url"> <div class="input-group-btn input-group-select">
                        <button type="button" class="btn btn-default">
                            <span class="multiple-concept-img"><i class="fa fa-eye" aria-hidden="true"></i></span>
                        </button>
                        </a>
                    </div>
                     </div>
                    <input type="text" name="<?= $return->input_name . '[]'; ?>" class="form-control input-group-url-val" readonly="readonly"/>

                    <span class="btn btn-default btn-file">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        &nbsp;Izaberi
                        <input type="file" name="<?= $return->input_name . '[file][]'; ?>" class="file-btn-select" accept="application/pdf">
                    </span>

                    <span class="input-group-btn">
                        <?php if($opt->max_files == 1){ ?>
                            <button type="button" class="btn btn-danger btn-remove">–</button>
                        <?php } else {  ?>
                            <button type="button" class="btn btn-success btn-add">+</button>
                        <?php } ?>
                    </span>
                </div>
            </div>
        </div>
        <?php
        break;
}
?>