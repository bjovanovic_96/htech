<?php
$return = clsForm::getInputName($model, $attr, $opt, 'ddl');

switch ($opt->display_type) {
    case 'inline':
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    <?php
                    $data = array(
                        'form' => $form,
                        'model' => $model,
                        'attr' => $attr,
                        'opt' => $opt);
                    $that->renderPartial('//template/form/f_lable_field', $data);
                    ?>
                </div>
                <div class="col-md-8">
                    <?php
                    if ($opt->form_type == 'multi') {
                        echo $form->dropDownList($model, $return->attr, $opt->value, $return->htmlOptions);
                    }
                    if ($opt->form_type == 'single') {
                        echo $form->dropDownList($model, $return->attr, $opt->value, $opt->htmlOptions);
                    }
                    ?> 
                </div>
            </div>
        </div>
        <?php break; ?>
    <?php
    default :
        ?>

        <div class="form-group">
            <?php
            $data = array(
                'form' => $form,
                'model' => $model,
                'attr' => $attr,
                'opt' => $opt);
            $that->renderPartial('//template/form/f_lable_field', $data);
            ?>

            <div class="col-md-12 col-xs-12">
                <?php
                if ($opt->form_type == 'multi') {
                    echo $form->dropDownList($model, $return->attr, $opt->value, $return->htmlOptions);
                }
                if ($opt->form_type == 'single') {
                    echo $form->dropDownList($model, $return->attr, $opt->value, $opt->htmlOptions);
                }
                ?>    
            </div>

        </div>

        <?php
        break;
}
?>