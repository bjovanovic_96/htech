<?php
$return = clsForm::getInputName($model, $attr, $opt);

$edior_id = get_class($model) . '_' . $attr . '_' . $opt->lang;
$edior_id_val = get_class($model) . '_' . $attr . '_' . $opt->lang . '_val';
?>

<div class="form-group">
    <?php
    $data = array(
        'form' => $form,
        'model' => $model,
        'attr' => $attr,
        'opt' => $opt);
    $that->renderPartial('//template/form/f_lable_field', $data);
    ?>
    <div class="col-md-12">

        <?php
        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'id' => $edior_id,
            'name' => $edior_id,
            //'model' => $model,
            //'attribute' => $attr . $opt->attr_array,
            'value' => $model->$attr ? $model->$attr . ' ' . $model->rel_cities->name : '',
            'source' => $this->createUrl('ajax/AjaxGetCitiesAuto'),
            // additional javascript options for the autocomplete plugin
            'options' => array(
                'status' => false,
                'minLength' => '2',
                'showAnim' => 'fold',
                'select' => "js:function(event, ui) {
                                                $('#" . $edior_id . "').val(ui.item.label);
                                                $('#" . $edior_id_val . "').val(ui.item.id);
                                                return false;
                                            }"
            ),
            'htmlOptions' => array(
                'class' => 'form-control',
                'placeholder' => 'Upišite i izaberite mesto',
            ),
                //'cssFile' => false,
        ));
        ?>
        <?php
        echo $form->hiddenField($model, $return->attr, array('value' => $model->$attr, 'id' => $edior_id_val));
        ?>
    </div>
</div>