<?php
$options= array(
    // 'class_error' => 'pull-right allert alert-danger',
    'htmlOptions' => array(
        'class' => 'form-control'
    ),
    'lang'=>$opt->lang,
    'label_text' => Yii::t('TblProducts', 'country'),
    'htmlOptions'  => array('class' => 'form-control country-select-control'),
    'attr_array' => $opt->attr_array,
    //'value' =>$opt->value['country'],
);

//$return = clsForm::getInputName($model, $attr, $opt, 'ddl');

switch ($otp->display_type) {
    case 'inline':
        ?>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    <?php
                    $data = array(
                        'form' => $form,
                        'model' => $model,
                        'attr' => $attr,
                        'opt' => $opt);
                    $that->renderPartial('//template/form/f_lable_field', $data);
                    ?>
                </div>
                <div class="col-md-8">
                    <?php
                    if ($opt->form_type == 'multi') {
                        echo $form->dropDownList($model, $return->attr, $opt->value, $return->htmlOptions);
                    }
                    if ($opt->form_type == 'single') {
                        echo $form->dropDownList($model, $return->attr, $opt->value, $opt->htmlOptions);
                    }
                    ?> 
                </div>
            </div>
        </div>
        <?php break; ?>
    <?php
    default :
        ?>

        <div class="form-group" >
        <div class="form-group price-multiple-form-holder" data-db-data='<?= $model->$attr; ?>'>
        <div class="form-group multiple-form-group price-multiple-form">
        <div class="row">
    
           <?php
           $attr_array=$options['attr_array'];
           $options['attr_array']=$attr_array.'[from][]';
           //$options['value']=$opt->value['currency'];
           $options['display_label'] = false;
           $options['htmlOptions']  = array('class' => 'form-control currency-select-control');
           echo '<div class="col-md-6">';
           clsForm::TimePickerGroup($that, $form, $model, $attr, $options);
           echo '</div>';
           
           $options['attr_array']=$attr_array.'[to][]';
           $options['display_label'] = false;
           $options['htmlOptions']  = array('class' => 'form-control cost-select-control');
           echo '<div class="col-md-6">';
           clsForm::TimePickerGroup($that, $form, $model, $attr, $options);
           echo '</div>';
           ?>
           <!--div class="col-md-2">
    <span class="input-group-btn">
                        <button type="button" class="btn btn-success btn-add" style="float: right;">+</button>
                    </span>
        </div-->
        </div>
        </div>
        </div>
        </div>
        <?php
        break;
}
?>