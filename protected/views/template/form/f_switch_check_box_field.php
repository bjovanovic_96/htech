<?php
$return = clsForm::getInputName($model, $attr, $opt);

switch ($opt->display_type) {
    case 'inline':

        break;

    default :
        ?>
        <div class="form-group">
            <?php
            if($opt->display_label){
                $data = array(
                    'form' => $form,
                    'model' => $model,
                    'attr' => $attr,
                    'opt' => $opt);
                $that->renderPartial('//template/form/f_lable_field', $data);
            }            
            ?>
            <div class="col-md-12">
                <?php
                if($opt->form_type == 'multi'){
                    $that->widget('yiiwheels.widgets.switch.WhSwitch', array(
                        'name' => $return->input_name,
                        'value' => $model->$attr,
                        'onColor' => 'success',
                        'offColor' => 'danger',
                        'htmlOptions' => $opt->htmlOptions,
                    ));
                }
                if($opt->form_type == 'single'){
                    $that->widget('yiiwheels.widgets.switch.WhSwitch', array(
                        'model' => $model,
                        'attribute' => $return->attr,
                        'onColor' => 'success',
                        'offColor' => 'danger',
                        'htmlOptions' => $opt->htmlOptions,
                    ));
                }
                ?>
            </div>
        </div>
        <?php
        break;
}
?>

