<?php
$return = clsForm::getInputName($model, $attr, $opt);

$opt->width = $opt->width ? $opt->width : 500;
$opt->height = $opt->height ? $opt->height : 500;
$opt->input_id = $opt->input_id ? $opt->input_id : $return->input_id;
if ($attr != "gallery_images") {
    $opt->image_url = $opt->image_url ? $opt->image_url : clsPath::getThumbsUrl();
    $opt->file_name = $opt->file_name ? $opt->file_name : $return->htmlOptions['value'];
    is_array(json_decode($opt->file_name, true)) ? $opt->file_name = json_decode($opt->file_name, true)[0] : '';
} else {
    $opt->file_name = $opt->file_name ? $opt->file_name : "";
}

$opt->lable_text = Yii::t(get_class($model), $attr);
$opt->label_text = $opt->label_text ? $opt->label_text : Yii::t(get_class($model), $attr);
//CORE IMAGE NAME
$core_image_name = "";
switch (get_class($model)) {
    case "TblProducts":
        $brand = $model->id_brands ? clsLoad::findTblBrands(['id' => $model->id_brands])->title : "";
        switch ($attr) {
            case "main_image":
                $core_image_name = $brand . ' ' . $model->title . ' ' . $model->barcode . ' main';
                $core_image_name = clsFunctions::makeImageName($core_image_name);
                break;
        }
        break;
    case "TblStores":
        switch ($attr) {
            case "main_image":
                $core_image_name = $model->title ? $model->title : clsFunctions::getAdminSelectedCompany("model")->title;
                $core_image_name = $core_image_name;
                $core_image_name = clsFunctions::makeImageName($core_image_name);
                break;
        }
        break;
}
?>

<?php
Yii::app()->clientScript->registerCss('croppic_css', " #holder_ " . $opt->input_id
        . " .cropControls {
	z-index: 17;
	/** margin: -50px auto 0px auto;*/
	position: relative;
	/* height: 297px; */
	/* width: 100%; */
	width: " . $opt->width . " px;
	display: block;
	top: 0px;
	left: 0px;
	font-family: sans-serif;
	background-color: rgba(0, 0, 0, 0.35);
}

#holder_ " . $opt->input_id . " .crop-btn-controls {
	display: inline-block;
	margin-left:" . ($opt->width - 266) / 2 . "px;
}
.cropHeaderWrapper > div {
    position: relative;
}
.cropHeaderWrapper .cropImgWrapper {
	overflow: hidden;
	z-index: 1;
	position: absolute;
	margin-top: 50px;
        border: 1px solid #a6a6a6;
}");
?>

<?php
switch ($opt->display_type) {
    case 'inline':

        break;

    default:
        ?>
        <div class="js-crop-holder" id="holder_<?= $opt->input_id; ?>" data-core_image_name="<?= $core_image_name; ?>">
            <div>
                <?php if ($opt->show_label_text) { ?>
                    <label class="col-md-12"> <!-- Image crop - Start -->
                        <div class="error_holder hide" tabindex='15'></div> <label
                            for="cropContainerHeaderButton_<?= $opt->input_id; ?>"><?= $opt->label_text; ?> <span
                                class="required">*</span></label>

                        <?php if ($form->error($model, $attr)) { ?>
                            <label
                                class="pull-right label-error-holder <?= $opt->class_error; ?>">
                                    <?php //echo $form->error($model, $attr); ?>
                                    <?= Yii::t('main_admin', 'required_field'); ?>
                            </label>
                        <?php } ?>

                    </label>
                <?php } ?>

                <div class="col-md-12">
                    <div class="input-group image-preview image-form"
                         id="image-preview-crop-<?= $opt->input_id; ?>"
                         data-url="<?= Yii::app()->createUrl("ajax/ajaxcropfile"); ?>"
                         data-file-name="<?= $opt->file_name; ?>"
                         data-folder-path-name="<?= $opt->folder_path_name; ?>"
                         data-folder-url-name="<?= $opt->folder_url_name; ?>">

                        <div class="input-group-btn input-group-select">
                            <button type="button" class="btn btn-default dropdown-toggle"
                                    data-toggle="dropdown">
                                <span class="multiple-concept-img"><img
                                        src="<?= $opt->file_name ? $opt->image_url . $opt->file_name : clsPath::getIconsUrl("photo.jpg"); ?>" /></span>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><img class="multiple-concept-img-holder"
                                         src="<?= $opt->file_name ? $opt->image_url . $opt->file_name : clsPath::getIconsUrl("photo.jpg"); ?>" />
                                </li>
                            </ul>
                        </div>

                        <input type="text" class="form-control image-preview-filename"
                               value="<?= $opt->file_name; ?>" disabled="disabled" />
                        <!-- don't give a name === doesn't send on POST/GET -->
                        <span class="input-group-btn"> <!-- image-preview-clear button -->
                            <button type="button"
                                    class="btn btn-default image-preview-clear blok"
                                    style="display: none;">
                                <span class="glyphicon glyphicon-remove"></span> Obriši
                            </button> <!-- image-preview-input -->
                            <div class="btn btn-default image-preview-input-crop"
                                 id="cropContainerHeaderButton_<?= $opt->input_id; ?>">
                                <span class="glyphicon glyphicon-folder-open"></span> <span
                                    class="image-preview-input-crop-title">Izaberi</span> <input
                                    type="hidden" name="<?= $return->input_name; ?>"
                                    value="<?= $opt->file_name; ?>"
                                    id="<?= $opt->input_id; ?>-member-image" />
                            </div>
                        </span>
                    </div>
                </div>
                <!-- Image crop - End -->
            </div>
            <!-- Modal - Start -->
            <div id="modal-<?= $opt->input_id; ?>" class="modal fade in">
                <div class="modal-dialog" style="width: 80%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                Slika <a data-dismiss="modal"
                                         class="fa fa-times pull-right x-xross"></a>
                            </h4>
                        </div>
                        <div class="modal-body" style="height: <?= $opt->height + 120; ?>px;">
                            <div class="row">

                                <div class="col-lg-12 cropHeaderWrapper">
                                    <?php //echo (!empty($model->$attr)) ? CHtml::image($opt->image_url . $model->$attr, "", array("style" => "width:100px")) : "";   ?>
                                    <div id="croppic_<?= $opt->input_id; ?>" style="width: <?= $opt->width; ?>px; height: <?= $opt->height; ?>px; margin: auto;"></div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-success pull-right btn-modal-crop-image">Sačuvaj</button>
                            <button type="button"
                                    class="btn btn-default pull-right btn-modal-reset-image">Odustani</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal - End -->

            <?php
            Yii::app()->clientScript->registerScript($opt->input_id, "
                /** Call crop image js plugin - Start */
                var croppicHeaderOptions = {
                    cropUrl: $('#image-preview-crop-" . $opt->input_id . "').attr('data-url'),
                    file_name: $('#image-preview-crop-" . $opt->input_id . "').attr('data-file-name'),
                    folder_path_name: $('#image-preview-crop-" . $opt->input_id . "').attr('data-folder-path-name'),
                    folder_url_name: $('#image-preview-crop-" . $opt->input_id . "').attr('data-folder-url-name'),
                    customUploadButtonId: 'cropContainerHeaderButton_" . $opt->input_id . "',
                    modal: false,
                    processInline: true,
                    imgEyecandy: false,
                    modal_id: '#modal-" . $opt->input_id . "',
                    holder_id: '#holder_" . $opt->input_id . "',
                    member_image_id: '#" . $opt->input_id . "-member-image',
                    //width: $(holder_" . $opt->input_id . ").attr('data-width'),
                    //height: $(holder_" . $opt->input_id . ").attr('data-height'),

                    //loaderHtml: '<div class=\"loader bubble-loader-holder\"><div class=\"loader bubblingG\"><span id=\"bubblingG_1\"></span><span id=\"bubblingG_2\"></span><span id=\"bubblingG_3\"></span></div></div>'
                    loaderHtml: '<div id=\"loader-wrapper\" class=\"loader\"><div id=\"loader\"></div></div>'
                };
                var croppic_" . $opt->input_id . " = new Croppic('croppic_" . $opt->input_id . "', croppicHeaderOptions);

                /* Connect modal buttons with croppic plugin */
                /* Call action crop image */
                $(document).on('click', '#modal-" . $opt->input_id . " .btn-modal-crop-image', function () {
                    $('#modal-" . $opt->input_id . " .cropControls .cropControlCrop').trigger('click');
                });
                /* Call action reset and hide modal */
                $(document).on('click', '#modal-" . $opt->input_id . " .btn-modal-reset-image', function () {
                    $('#modal-" . $opt->input_id . "').modal('hide');
                });
                /** Call crop image js plugin - End */
                ", CClientScript::POS_END);
            ?>
            <script>

            </script>
        </div>
        <?php
        break;
}
?>

