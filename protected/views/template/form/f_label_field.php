<?php
if ($opt->display_label) {
    if ($opt->label_text) {
        ?>
        <label class="col-md-12" for="<?= $attr; ?>">
            <label><?= $opt->label_text; ?></label>
            <?php if ($form->error($model, $attr)) { ?>
                <label class="pull-right label-error-holder <?= $opt->class_error; ?>">
                    <?= $form->error($model, $attr); ?>
                </label>
            <?php } ?>
        </label>
    <?php } else {
        ?>
        <label class="col-md-12" for="<?= $attr; ?>">
            <?= $form->labelEx($model, $attr); ?>
            <?php if ($form->error($model, $attr)) { ?>
                <label class="pull-right label-error-holder <?= $opt->class_error; ?>">
                    <?= $form->error($model, $attr); ?>
                </label>
            <?php } ?>
        </label>
    <?php } ?>
<?php } ?>



