<div class="col-md-12 col-xs-12" role="tabpanel" data-example-id="togglable-tabs">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => $id_form,
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>
    <div class="col-md-12 col-xs-12">
        <button type="submit" class="btn btn-lg btn-success js_display_loader" style="margin-top: 9px;">Sačuvaj</button>
        <input type="hidden" name="selected_lang" class="selected_lang" />
    </div>
    <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" aria-labelledby="grid-2-tab">
            <div class="form-holder">                 
                <?php clsForm::FormFields($this, $form, $data['model'], []); ?>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div>


