<div class="js-product-barcodes-holder">
    <label>Barkodovi</label>
    <?php
    $opt_sql = [
        'id_companies' => clsFunctions::getAdminSelectedCompany(),
        'id_products' => $model->id,
        'order' => 't.create_date ASC',
    ];
    if ($model->isNewRecord) {
        $exist_barcodes = false;
    } else {
        $exist_barcodes = clsLoad::findAllTblBarcodes($opt_sql);
    }
// Label
    $count = 1;
    if ($exist_barcodes) {
        foreach ($exist_barcodes as $model_barcodes) {
            $opt_partials = [
                'model_barcodes' => $model_barcodes,
                'model_products' => $model,
            ];
            echo $this->renderPartial('//template/form/product_barcodes_row', ['data' => $opt_partials]);
            $count++;
        }
    }
    $opt_partials = [
        'model_barcodes' => new TblBarcodes(),
        'model_products' => $model,
    ];
    ?>
    <?= $this->renderPartial('//template/form/product_barcodes_row', ['data' => $opt_partials]); ?>
</div>