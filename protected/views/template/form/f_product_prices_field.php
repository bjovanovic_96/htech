<div class="js-product-prices-holder">
    <label>Prodajne cene</label>
    <?php
    $opt_sql = [
        'id_companies' => clsFunctions::getAdminSelectedCompany(),
        'id_products' => $model->id,
        'order' => 't.create_date ASC',
    ];
    if ($model->isNewRecord) {
        $exist_product_prices = false;
    } else {
        $exist_product_prices = clsLoad::findAllTblProductPrices($opt_sql);
    }
// Label
    $count = 1;
    if ($exist_product_prices) {
        foreach ($exist_product_prices as $model_product_prices) {
            $opt_partials = [
                'model_product_prices' => $model_product_prices,
                'model_products' => $model,
            ];
            echo $this->renderPartial('//template/form/product_prices_row', ['data' => $opt_partials]);
            $count++;
        }
    }
    $opt_partials = [
        'model_product_prices' => new TblProductPrices(),
        'model_products' => $model,
    ];
    ?>
    <?= $this->renderPartial('//template/form/product_prices_row', ['data' => $opt_partials]); ?>
</div>