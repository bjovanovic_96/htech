<?php
$return = clsForm::getInputName($model, $attr, $opt);

// Convert to json
if (is_array($return->htmlOptions['value'])) {
    $return->htmlOptions['value'] = json_encode($return->htmlOptions['value']);
}

switch ($opt->display_type) {
    case 'inline':

        break;
    default :
        ?>
        <div class="form-group">
            <div class="contacts image-multiple-form-holder multiple-form-group" 
                 data-db-data='<?= $return->htmlOptions['value']; ?>' 
                 data-file-remove-url="<?= Yii::app()->createUrl('ajax/ajaxremovefile'); ?>"
                 data-img-path="<?= $opt->folder_path_name; ?>"
                 data-img-url="<?= $opt->folder_url_name; ?>"
                 data-tbl-name="<?= get_class($model); ?>"
                 data-col-name="<?= $attr; ?>"
                 data-max="<?= $opt->max_files; ?>"
                 >
                     <?php
                     if ($opt->form_type == 'multi') {
                         $same_attr_name = 'same_' . $attr;
                         $same_attr = $opt->lang ? '[' . $same_attr_name . ']' . '[' . $opt->lang . ']' : '[' . $same_attr_name . ']';
                         $same_field_name = get_class($model) . $same_attr . $opt->attr_array;
                         ?>
                    <label class="pull-right same-form-data-btn">
                        <input type="checkbox" title="Podaci kao sa prve forme" name="<?= $same_field_name; ?>" <?= $model->$same_attr_name ? 'checked="checked"' : ""; ?>/>
                    </label>
                <?php } ?>

                <?php
                // Label
                $data = array(
                    'form' => $form,
                    'model' => $model,
                    'attr' => $attr,
                    'opt' => $opt);
                $that->renderPartial('//template/form/f_lable_field', $data);
                ?>

                <div class="form-group input-group image-multiple-form image-form" 
                     data-default-ddl-value="<?= clsPath::getIconsUrl("photo.jpg"); ?>">
                    <div class="input-group-btn input-group-select">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <span class="multiple-concept-img"><img src="<?= clsPath::getIconsUrl("photo.jpg"); ?>" /></span> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <img class="multiple-concept-img-holder" src="<?= clsPath::getIconsUrl("photo.jpg"); ?>" />
                            </li>
                        </ul>
                    </div>
                    <input type="text" name="<?= $return->input_name . '[]'; ?>" class="form-control input-group-url-val" readonly="readonly"/>

                    <span class="btn btn-default btn-file">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        &nbsp;Izaberi
                        <input type="file" name="<?= $return->input_name . '[file][]'; ?>" class="file-btn-select" accept="image/gif, image/jpeg, image/png">
                    </span>

                    <span class="input-group-btn">
                        <?php if ($opt->max_files == 1) { ?>
                            <button type="button" class="btn btn-danger btn-remove">–</button>
                        <?php } else { ?>
                            <button type="button" class="btn btn-success btn-add">+</button>
                        <?php } ?>
                    </span>
                </div>
            </div>
        </div>
        <?php
        break;
}
?>