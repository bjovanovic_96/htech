<?php
$return = clsForm::getInputName($model, $attr, $opt);

switch ($opt->display_type) {
    case 'inline':

        break;
    default :
        ?>
        <div class="form-group">
            <div class="contacts video-multiple-form-holder" data-db-data='<?= $model->$attr; ?>'>
                <?php
                if ($opt->form_type == 'multi') {
                    $same_attr_name = 'same_'.$attr;
                    $same_attr = $opt->lang ? '[' . $same_attr_name . ']' . '[' . $opt->lang . ']' : '[' . $same_attr_name . ']';
                    $same_field_name = get_class($model) . $same_attr . $opt->attr_array;
                    ?>
                    <label class="pull-right same-form-data-btn">
                        <input type="checkbox" title="Podaci kao sa prve forme" name="<?= $same_field_name; ?>" <?= $model->$same_attr_name ? 'checked="checked"' : ""; ?>/>
                    </label>
                <?php } ?>

                <?php
                // Label
                $data = array(
                    'form' => $form,
                    'model' => $model,
                    'attr' => $attr,
                    'opt' => $opt);
                $that->renderPartial('//template/form/f_lable_field', $data);

                $first_val = '';
                $first_text = '';
                $opt_value_data = '';
                $num = 1;
                foreach ($opt->value as $k => $v) {
                    if ($num == 1) {
                        $first_val = $k;
                        $first_text = $v;
                    }
                    $num++;
                    
                    $opt_value_data .= '<li><a href="#' . $k . '">' . $v . '</a></li>';
                }
                ?>

                <div class="form-group multiple-form-group input-group video-multiple-form" data-default-ddl-value="<?= $first_val; ?>" data-default-ddl-text="<?= $first_text; ?>">
                    <div class="input-group-btn input-group-select">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <span class="concept"><?= $first_text; ?></span> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <?= $opt_value_data; ?>
                        </ul>
                        <input type="hidden" class="input-group-select-val" name="<?= $return->input_name . '[type][]'; ?>" value="<?= $first_val; ?>" />
                    </div>
                    <input type="text" name="<?= $return->input_name . '[url][]'; ?>" class="form-control input-group-url-val" />
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-success btn-add">+</button>
                    </span>
                </div>
            </div>
        </div>
        <?php
        break;
}
?>