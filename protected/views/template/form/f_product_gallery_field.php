<?php

$return = clsForm::getInputName($model, $attr, $opt);

// Convert to json
if (is_array($return->htmlOptions['value'])) {
    $return->htmlOptions['value'] = json_encode($return->htmlOptions['value']);
}
$exist_gallery = $model->$attr ? json_decode($model->$attr) : false;
// Label
$data = array(
    'form' => $form,
    'model' => $model,
    'attr' => $attr,
    'opt' => $opt);
$that->renderPartial('//template/form/f_lable_field', $data);
$count = 1;
if ($exist_gallery) {
    foreach ($exist_gallery as $id_colors => $images) {
        foreach ($images as $image) {
            $opt_partials = [
                'that' => $that,
                'form' => $form,
                'model' => $model,
                'attr' => $attr,
                'id_colors' => $id_colors,
                'image' => $image,
                'input_id' => 'product_gallery_images_' . $count,
            ];
            echo $this->renderPartial('//template/form/product_gallery_row', ['data' => $opt_partials]);
            $count++;
        }
    }
}
$opt_partials = [
    'that' => $that,
    'form' => $form,
    'model' => $model,
    'attr' => $attr,
    'input_id' => 'product_gallery_images_' . $count,
];
?>
<?= $this->renderPartial('//template/form/product_gallery_row', ['data' => $opt_partials]); ?>