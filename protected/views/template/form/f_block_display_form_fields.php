<div class="<?= $model_block_fileds_sets[$i]->$form_id->class ?>" id="<?= isset($model_block_fileds_sets[$i]->$form_id->id) && $model_block_fileds_sets[$i]->$form_id->id ? $model_block_fileds_sets[$i]->$form_id->id : "" ?>">
    <div class="x_panel">
        <div class="x_title"><h2><?= $model_block_fileds_sets[$i]->$form_id->title; ?></h2>
            <ul class="nav navbar-right panel_toolbox mobile_hide">
                <li>
                    <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li>
                    <a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?php
            foreach ($order_arr as $attr_index) {
                $attr = $model_form_fileds_sets[$attr_index]->element->attr;
                $type = $model_form_fileds_sets[$attr_index]->element->type;
                $opt = $model_form_fileds_sets[$attr_index]->element->opt;
                $class_div = $model_form_fileds_sets[$attr_index]->element->class;
                ?>
                <div class="<?= $class_div; ?>">
                    <?php clsForm::$type($that, $form, $model, $attr, $opt); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>



