<?php
$return = clsForm::getInputName($model, $attr, $opt, 'tags');

switch ($opt->display_type) {
    case 'inline':

        break;

    default:
        ?>
        <div class="form-group">
            <?php
            $data = array(
                'form' => $form,
                'model' => $model,
                'attr' => $attr,
                'opt' => $opt
            );
            $that->renderPartial('//template/form/f_lable_field', $data);
            ?>
            <div class="col-md-12 col-xs-12">
                <?php
                if ($model->$attr) {
                    if (is_string($model->$attr) && !(is_array($model->$attr))) {
                        $model->$attr = clsFunctions::convertJSONToStringWithOutSpace(array('data' => $model->$attr));
                    } else
                        $model->$attr = array($model->$attr);
                }
                $that->widget('yiiwheels.widgets.select2.WhSelect2', array(
                    'asDropDownList' => false,
                    // 'name' => $attr . $opt->attr_array,
                    //'model' => $model,
                    'name' => $return->input_name,
                    'value' => $model->$attr,
                    'pluginOptions' => array(
                        'tags' => $opt->value,
                        'placeholder' => 'tagovi',
                        'width' => '100%',
                        'tokenSeparators' => array(
                            ',',
                            ' '
                        )
                    ),
                    'htmlOptions' => $opt->htmlOptions
                ));
                ?>


            </div>
        </div>
        <?php
        break;
}
?>
