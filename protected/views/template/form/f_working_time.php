<?php
$days = $opt->days_array ? $opt->days_array : array();
$options = array(
	// 'class_error' => 'pull-right allert alert-danger',
	'htmlOptions' => array(
		'class' => 'form-control'
	),
	'lang' => $opt->lang,
	'label_text' => Yii::t('TblProducts', 'country'),
	'htmlOptions' => array(
		'class' => 'form-control country-select-control'
	),
	'attr_array' => $opt->attr_array
);
// 'value' =>$opt->value['country'],


// $return = clsForm::getInputName($model, $attr, $opt, 'ddl');

switch ($otp->display_type) {
    case 'inline':
        ?>
<div class="form-group">
	<div class="row">
		<div class="col-md-4">
                    <?php
                    $data = array(
                        'form' => $form,
                        'model' => $model,
                        'attr' => $attr,
                        'opt' => $opt);
                    $that->renderPartial('//template/form/f_lable_field', $data);
                    ?>
                </div>
		<div class="col-md-8">
                    <?php
                    if ($opt->form_type == 'multi') {
                        echo $form->dropDownList($model, $return->attr, $opt->value, $return->htmlOptions);
                    }
                    if ($opt->form_type == 'single') {
                        echo $form->dropDownList($model, $return->attr, $opt->value, $opt->htmlOptions);
                    }
                    ?> 
                </div>
	</div>
</div>
<?php break; ?>
    <?php
    default :
        ?>
        
                     <?php
                     if ($opt->form_type == 'multi') {
                         $same_attr_name =  'same_'.$attr;
                         $same_attr = $opt->lang ? '[' . $same_attr_name . ']' . '[' . $opt->lang . ']' : '[' . $same_attr_name . ']';
                         $same_field_name = get_class($model) . $same_attr . $opt->attr_array;
                     }
                         ?>
                         <label class="pull-right same-form-data-btn">
                        <input type="checkbox" title="Podaci kao sa prve forme" name="<?= $same_field_name; ?>" <?= $model->$same_attr_name ? 'checked="checked"' : ""; ?>/>
                    </label>
          <?php
          foreach ($days as $key_day=>$day){ 
          	$options['attr_array']="[$key_day]";
          	$opt->label_text=$day;
          	$data = array(
          		'form' => $form,
          		'model' => $model,
          		'attr' => $attr,
          		'opt' => $opt);
          	echo '<div class="row">';
          	echo '<div class="col-md-3">';
          	$that->renderPartial('//template/form/f_lable_field', $data);
          	echo '</div>'
          	?>
           <?php
           echo '<div class="col-md-8">';
           clsForm::TimeBetweenGroup($that, $form, $model, $attr, $options);
           echo '</div>';
           echo '</div>';
           ?>

        <?php
          }
        break;

}
?>