<?php
$return = clsForm::getInputName($model, $attr, $opt, 'tags');

switch ($opt->display_type) {
    case 'inline':
        
        break;
    
    default:
        ?>
<div class="form-group">
            <?php
        $data = array(
            'form' => $form,
            'model' => $model,
            'attr' => $attr,
            'opt' => $opt
        );
        $that->renderPartial('//template/form/f_lable_field', $data);
        ?>
            <div class="col-md-12">
                <?php
        if ($opt->form_type == 'multi') {
            /*
             * $that->widget('yiiwheels.widgets.select2.WhSelect2', array(
             * 'asDropDownList' => false,
             * 'name' => $return->input_name,
             * 'value' => $return->value,
             * 'pluginOptions' => array(
             * 'tags' => $opt->value,
             * 'placeholder' => 'tagovi',
             * 'width' => '100%',
             * 'tokenSeparators' => array(',', ' ')
             * ),
             * 'htmlOptions' => $opt->htmlOptions,
             * ));
             */
            // dev::pd($opt->value);
            
            $return->value = json_decode($return->value,true);
            $this->widget('select2.ESelect2', array(
                /*'model' => $model,
                'attribute' => $return->attr,*/
                
                'name' => $return->input_name,
                'value' => $return->value,
                
                'data' => $opt->value,
                'options'=>array(
                    'placeholder'=>Yii::t('default', 'Select'),
                    'allowClear'=>true,
                ),
                'htmlOptions' => array(
                    'multiple' => 'multiple',
                    'style' => 'width:100%'
                )
            ));
        }
        if ($opt->form_type == 'single') {
            $that->widget('yiiwheels.widgets.select2.WhSelect2', array(
                'asDropDownList' => false,
                // 'name' => $attr . $opt->attr_array,
                'model' => $model,
                'attribute' => $return->attr,
                'value' => $model->$attr,
                'pluginOptions' => array(
                    'tags' => $opt->value,
                    'placeholder' => 'tagovi',
                    'width' => '100%',
                    'tokenSeparators' => array(
                        ',',
                        ' '
                    )
                ),
                'htmlOptions' => $opt->htmlOptions
            ));
        }
        ?>


            </div>
</div>
<?php
        break;
}
?>
