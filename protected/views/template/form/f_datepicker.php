<?php
$htmlOptions = array(
    'size' => '25', // textField size
    'maxlength' => '10', // textField maxlength
    //'class' => $opt->class,
    //'disabled' => 'disabled',
    'readonly' => 'readonly',
);

$opt->htmlOptions = array_merge($htmlOptions, $opt->htmlOptions);

$return = clsForm::getInputName($model, $attr, $opt, 'datetimepicker');

$options = array(
    'showOn' => 'both',
    'showAnim' => 'slideDown',
    'dateFormat' => 'dd.mm.yy', // format of "2012-12-25"
    'yearRange' => $opt->yearRange,
    'minDate' => $opt->minDate,
    'maxDate' => $opt->maxDate,
    'showOtherMonths' => false, // show dates in other months
    'selectOtherMonths' => false, // can seelect dates in other months
    'changeYear' => true, // can change year
    'changeMonth' => true, // can change month
    'showButtonPanel' => true, // show button panel
);

if ($opt->name) {
    switch ($opt->display_type) {
        case 'inline':

            break;
        default :
            ?>
            <div class="form-group">
                <?php if ($opt->display_label && $opt->label_text) { ?>
                    <label class="col-md-12" for="<?= $attr; ?>">
                        <?= CHtml::label($opt->label_text, ''); ?>:
                        <?php if ($form->error($model, $attr)) { ?>
                            <label class="pull-right alert alert-danger <?= $opt->class_error; ?>">
                                <?= $form->error($model, $attr); ?>
                            </label>
                        <?php } ?>
                    </label>
                <?php } ?>
                <div class="col-md-12">
                    <?php
                    $val = $opt->value ? $opt->value : '';
                    $val = !$val && $return->htmlOptions['value'] ? $return->htmlOptions['value'] : '';

                    $that->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'name' => $return->input_name,
                        'value' => $return->value,
                        'language' => 'bs',
                        'htmlOptions' => $opt->htmlOptions,
                        'options' => $options,
                    ));
                    ?>
                </div>
            </div>

            <?php
            break;
    }
} else {
    switch ($opt->display_type) {
        case 'inline':

            break;
        default :
            ?>
            <div class="form-group">
                <?php
                $data = array(
                    'form' => $form,
                    'model' => $model,
                    'attr' => $attr,
                    'opt' => $opt);
                $that->renderPartial('//template/form/f_lable_field', $data);
                ?>
                <div class="col-md-12">
                    <?php
                    if ($opt->form_type == 'multi') {
                        $return->value=clsDataFormat::displayDate($return->value);
                        $that->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'name' => $return->input_name,
                            'value' => $return->value,
                            'language' => 'bs',
                            'htmlOptions' => $opt->htmlOptions,
                            'options' => $options,
                        ));
                    }
                    if ($opt->form_type == 'single') {
                        $return->value=clsDataFormat::displayDate($return->value);
                        $that->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model' => $model,
                            'attribute' => $return->attr,
                            'value' => $model->$attr,
                            'language' => 'bs',
                            'htmlOptions' => $opt->htmlOptions,
                            'options' => $options,
                        ));
                    }
                    ?>
                </div>
            </div>

            <?php
            break;
    }
}
?>