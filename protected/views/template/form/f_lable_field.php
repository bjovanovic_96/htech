<?php if ($opt->display_label) { 
    if ($opt->label_text) {
        
       echo CHtml::label($opt->label_text, ''); 
   } else {
        ?>
        <label class="col-md-12 col-xs-12" for="<?= $attr; ?>">
            <?= $form->labelEx($model, $attr); ?>
            <?php if ($form->error($model, $attr)) { ?>
                <label class="pull-right label-error-holder <?= $opt->class_error; ?>">
                    required field
                </label>
            <?php } ?>
        </label>
    <?php } ?>
    <?php } ?>


