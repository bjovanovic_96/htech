<?php
$data = (object) [
            'model_barcodes' => isset($data['model_barcodes']) ? $data['model_barcodes'] : false,
            'model_products' => isset($data['model_products']) ? $data['model_products'] : false,
];
$all_product_colors = $data->model_products->all_available_colors(['type' => 'all']);
$all_product_sizes = $data->model_products->all_available_sizes(['type' => 'all']);
if ($data->model_barcodes->isNewRecord) {
    ?>
    <div class="form-group row js-add-barcode-row"  data-slug-products="<?= $data->model_products->slug; ?>">
        <div class="col-md-5 col-xs-5">
            <div class="input-group">
                <input type="text" name="barcode" class="form-control" placeholder="Barkod"/>
                <span class="input-group-btn">
                    <button class="btn btn-default js-auto-generate-barcode"
                            data-url="<?= clsUrls::getAjaxAutoGenerateBarcodeUrl(); ?>"
                            type="button">
                        Auto
                    </button>
                </span>
            </div>

        </div>
        <div class="col-md-3 col-xs-3">
            <select name="id_colors" class="form-control"> 
                <option value="">Odaberite boju</option> 
                <?php
                if ($all_product_colors) {
                    foreach ($all_product_colors as $model_colors) {
                        ?>
                        <option value="<?= $model_colors->id; ?>"><?= $model_colors->print_title; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>
        <div class="col-md-3 col-xs-3">
            <select name="id_sizes" class="form-control"> 
                <option value="">Odaberite veličinu</option> 
                <?php
                if ($all_product_sizes) {
                    foreach ($all_product_sizes as $model_sizes) {
                        ?>
                        <option value="<?= $model_sizes->id; ?>"><?= $model_sizes->title; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>
        <div class="col-md-1 col-xs-1">
            <button class="btn btn-success js-edit" data-url="<?= clsUrls::getAjaxaEditProductBarcodeUrl(); ?>" data-confirm-text="Da li ste sigurni da zelite da dodelite bar kod za proizvod <?= $data->model_products->print_title; ?> boja {product_color} velicinu {product_size}?">
                <span class="fa fa-plus"></span>
            </button>
        </div>
    </div>
<?php } else { ?>
    <div class="form-group row js-remove-barcode-row"  data-slug-products="<?= $data->model_products->slug; ?>">
        <div class="col-md-5 col-xs-5">
            <input type="text" class="form-control js-barcode-value" value="<?= $data->model_barcodes->barcode; ?>" readonly="true"/> 
        </div>
        <div class="col-md-3 col-xs-3">
            <select class="form-control" disabled="true"> 
                <?php
                if ($all_product_colors) {
                    foreach ($all_product_colors as $model_colors) {
                        ?>
                        <option value="<?= $model_colors->id; ?>" <?= $data->model_barcodes->id_colors == $model_colors->id ? "selected" : ""; ?>><?= $model_colors->print_title; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>
        <div class="col-md-3 col-xs-3">
            <select class="form-control" disabled="true"> 
                <?php
                if ($all_product_sizes) {
                    foreach ($all_product_sizes as $model_sizes) {
                        ?>
                        <option value="<?= $model_sizes->id; ?>" <?= $data->model_barcodes->id_sizes == $model_sizes->id ? "selected" : ""; ?>><?= $model_sizes->title; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>
        <div class="col-md-1 col-xs-1">
            <button class="btn btn-success js-remove" data-url="<?= clsUrls::getAjaxRemoveProductBarcodeUrl(); ?>" data-id="<?= $data->model_barcodes->id; ?>" data-confirm-text="Da li ste sigurni da želite da obrišete barkod {barcode}?">
                <span class="fa fa-minus"></span>
            </button>
        </div>
    </div>
<?php }
?>