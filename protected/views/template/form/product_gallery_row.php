<?php
$data = (object) [
            'that' => isset($data['that']) ? $data['that'] : false,
            'form' => isset($data['form']) ? $data['form'] : false,
            'model' => isset($data['model']) ? $data['model'] : false,
            'attr' => isset($data['attr']) ? $data['attr'] : false,
            'id_colors' => isset($data['id_colors']) ? $data['id_colors'] : false,
            'image' => isset($data['image']) ? $data['image'] : false,
            'input_id' => isset($data['input_id']) ? $data['input_id'] : false,
];
$all_product_colors = $data->model->all_available_colors(['type' => 'all']);
$opt_crop = [
    'image_url' => clsPath::getProductsUrl(),
    'width' => 500,
    'height' => 750,
    'file_name' => '',
    'folder_path_name' => clsPath::getProductsPath(),
    'folder_url_name' => clsPath::getProductsUrl(),
    'attr_array' => '[image][]',
    'show_label_text' => false,
    'file_name' => $data->image,
    'input_id' => $data->input_id,
];
?>
<div class="form-group js-product-gallery-row row">
    <div class="col-md-6 col-xs-6">

        <?php if ($data->image) { ?>
            <div class="col-md-12 col-xs-12">
                <div class="input-group image-preview image-form">
                    <div class="input-group-btn input-group-select">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown">
                            <span class="multiple-concept-img"><img
                                    src="<?= $opt_crop['file_name'] ? $opt_crop['image_url'] . $opt_crop['file_name'] : '/img/icons/photo.jpg'; ?>" /></span>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><img class="multiple-concept-img-holder"
                                     src="<?= $opt_crop['file_name'] ? $opt_crop['image_url'] . $opt_crop['file_name'] : '/img/icons/photo.jpg'; ?>" />
                            </li>
                        </ul>
                    </div>
                    <input type="text" name="TblProducts[gallery_images][image][]" class="form-control image-preview-filename"
                           value="<?= $opt_crop['file_name']; ?>" readonly="true" />
                </div>
            </div>
            <?php
        } else {
            echo clsForm::imageCropGroup($data->that, $data->form, $data->model, $data->attr, $opt_crop);
        }
        ?>
    </div>
    <div class="col-md-4 col-xs-4">
        <select name="TblProducts[gallery_images][id_colors][]" class="form-control js-select2-ddl js-product-gallery-image" 
                data-url="<?= clsUrls::getAjaxGetProductGalleryImageNameUrl(); ?>"
                data-product-slug="<?= $data->model->slug ?>"
                >
            <?php
            if ($all_product_colors) {
                foreach ($all_product_colors as $model_colors) {
                    ?>
                    <option value="<?= $model_colors->id; ?>" <?= $data->id_colors == $model_colors->id ? "selected" : ""; ?>><?= $model_colors->print_title; ?></option>
                    <?php
                }
            }
            ?>
        </select>
    </div>
    <div class="col-md-2 col-xs-2">
        <?php if ($data->image || $data->id_colors) { ?>
            <span class="input-group-btn">
                <button type="button" class="btn btn-danger btn-add js-remove-product-gallery-row" data-url="<?= clsUrls::getAjaxRemoveProductGalleryImageUrl(); ?>" data-confirm_text="Da li ste sigurni da želite da obrišete sliku?">-</button>
            </span>
        <?php } else { ?>
            <span class="input-group-btn">
                <button type="button" class="btn btn-success btn-add js-add-product-gallery-row" data-url="<?= clsUrls::getAjaxAddProductGalleryRowUrl(); ?>" data-id-products="<?= $data->model->id; ?>">+</button>
            </span>
        <?php } ?>
    </div>
</div>