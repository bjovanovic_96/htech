<?php
$data = (object) [
    'file_time' => clsParams::getFileTime()
];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="language" content="en" />
        <meta name="viewport" name="viewport" content="width=device-width, height=device-height,  initial-scale=1.0, user-scalable=no;user-scalable=0;"/>
        <meta name="author" content="<?= clsParams::getSystemOptions('company->name'); ?>" />
        <?php
        Yii::app()->clientScript->registerMetaTag($this->getMetaDescription(), 'Description');
        Yii::app()->clientScript->registerMetaTag($this->getMetaKeywords(), 'Keywords');
        ?>        
        <meta property="og:type"          content="website" />
        <meta property="og:url"           content="<?= $this->getMetaUrl() ?>" />
        <meta property="og:title"         content="<?= $this->getMetaTitle() ?>" />
        <meta property="og:description"   content="<?= $this->getMetaDescription() ?>" />
        <meta property="og:image"         content="<?= $this->getMetaImage() ?>" />
        <?php Yii::app()->bootstrap->register(); ?>
        <title><?= $this->page_title; ?></title>
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/site/custom.css?<?= $data->file_time; ?>" />
    </head>
    <body data-action-id="<?= $this->action->id; ?>">
        <div class="page text-center">
            <?php
            $this->renderPartial('//template/site/top_menu');
            $this->renderPartial('//template/partials/message');
            ?>
            <div class="container">
                <div class="content">
                    <?php echo $content; ?>
                </div>
            </div>
            <?php $this->renderPartial('//template/site/footer'); ?>
        </div>
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/default/default.js?<?= $data->file_time; ?>"></script>
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/default/jquery.min.js"></script>
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/default/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/site/custom.js?<?= $data->file_time; ?>"></script>
    </body>
</html>
