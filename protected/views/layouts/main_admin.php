<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <!-- Meta -->
        <meta charset="utf-8" />
        <meta name="language" content="sr-rs" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="author" content="<?= clsParams::getSystemOptions('company->name'); ?>" />
        <!-- jQuery -->
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/default/jquery.min.js"></script>
        <!-- CSS Bootstrap Core CSS and JS -->
        <?php Yii::app()->bootstrap->register(); ?>
        <!-- Text editor -->
        <?php Yii::app()->texteditor->register(); ?>
        <!-- Title -->
        <title><?php //= $this->page_title;      ?></title>
        <title>Admistracija</title>
        <!-- CSS Loader -->
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/site/loader.css" />
        <!-- CSS Custom fonts -->
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/admin/fonts.css" />
        <!-- CSS Custom css -->
        <link rel="stylesheet" href="<?= Yii::app()->request->baseUrl; ?>/css/admin/custom.css" />
    </head>

    <body class="nav-sm" data-action="<?= $this->action->id; ?>">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <?php $this->renderPartial('//template/admin/left_menu'); ?>
                </div>

                <div class="top_nav">
                    <?php $this->renderPartial('//template/admin/top_menu'); ?>
                </div>

                <div class="right_col" role="main">
                    <?php echo $content; ?>
                </div>
            </div>
            <div class="admin-powered-by">
                <p>Powered by:
                    <a href="<?= clsParams::getSystemOptions("meta->site_url"); ?>" target="_blank">
                        <?= clsParams::getSystemOptions("company->name"); ?>
                    </a>
                </p>
            </div>
        </div>
        <!-- Bootstrap JS -->
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/default/bootstrap.min.js"></script>
        <!-- sRay JS -->
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/default/default.js"></script>
        <!-- Custom JS -->
        <script type="text/javascript" src="<?= Yii::app()->request->baseUrl; ?>/js/admin/custom.js"></script>
    </body>
</html>
