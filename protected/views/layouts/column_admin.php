<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main_admin'); ?>
<!-- Content - Start -->
<?php
$flash_messages = Yii::app()->user->getFlashes();
if($flash_messages){
    clsForm::displayFlashMsg($flash_messages);    
    $has_flash = true; // Variable that can used for dislay ore hide some part of page
}
?>
<?php echo $content; ?>
<!-- Content - End -->
<?php $this->endContent(); ?>