<div class="form-group">
    <?php echo $form->error($model, 'username'); ?>
    <?php echo $form->label($model, 'username'); ?>:
    <?php echo $form->textField($model, 'username', array('class' => 'form-control')); ?>
</div>

<div class="form-group">
    <?php echo $form->error($model, 'password'); ?>
    <?php echo $form->label($model, 'password'); ?>:
    <?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
</div>
<div class="form-group">
    <?php echo $form->error($model, 'rememberMe'); ?>
    <?php echo $form->label($model, 'rememberMe'); ?>:
    <?php echo $form->checkBox($model, 'rememberMe', array('class' => '')); ?>
</div>