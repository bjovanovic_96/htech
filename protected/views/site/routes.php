<?php
$data = (object)[
    'ddl_cities' => isset($data['ddl_cities']) && $data['ddl_cities'] ? $data['ddl_cities'] : false,
    'all_routes' => isset($data['all_routes']) && $data['all_routes'] ? $data['all_routes'] : false,
    'search' => isset($data['search']) && $data['search'] ? $data['search'] : false,
];
?>
<section>
    <h3>Routes</h3>
    <div class="search-form margin_form">
        <form>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>From city</label>
                        <select class="form-control" name="search[from_city]">
                            <?php foreach ($data->ddl_cities as $value => $title) {
                                ?>
                                <option value="<?= $value; ?>" <?= $data->search && isset($data->search['from_city']) && $data->search['from_city'] == $value ? "selected" : ""; ?>><?= $title; ?></option>
                                <?php
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>To city</label>
                        <select class="form-control" name="search[to_city]">
                            <?php foreach ($data->ddl_cities as $value => $title) {
                                ?>
                                <option value="<?= $value; ?>" <?= $data->search && isset($data->search['to_city']) && $data->search['to_city'] == $value ? "selected" : ""; ?>><?= $title; ?></option>
                                <?php
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <button class="btn btn-success form-control">Search</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="routes">
        <?php if ($data->search) {
            if ($data->all_routes) {
                ?>
                <table class="table table-bordered">
                    <thead>
                    <th>From city</th>
                    <th>To city</th>
                    <th>Price</th>
                    <th class="text-center">Cheapest</th>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($data->all_routes as $index => $model_routes) {
                        $model_source_airports = $model_routes->rel_source_airports;
                        $model_destination_airports = $model_routes->rel_destination_airports;
                        ?>
                        <tr>
                            <td class="text-left"><?= $model_source_airports->rel_cities->name; ?></td>
                            <td class="text-left"><?= $model_destination_airports->rel_cities->name; ?></td>
                            <td class="text-left"><?= $model_routes->price; ?></td>
                            <td class="text-center">
                                <?php if($index==0){ ?>
                                <span class="fa fa-check"></span>
                            <?php } ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
                <?php
            } else {
                ?>
                <div class="alert alert-info">
                    Currently have no data to display
                </div>
                <?php
            }
        } ?>
    </div>
</section>