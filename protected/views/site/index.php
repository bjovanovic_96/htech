<?php
$data = (object)[
    'all_cities' => isset($data['all_cities']) && $data['all_cities'] ? $data['all_cities'] : false,
    'ddl_countries' => isset($data['ddl_countries']) && $data['ddl_countries'] ? $data['ddl_countries'] : false,
    'search' => isset($data['search']) && $data['search'] ? $data['search'] : false,
];
?>
<section>
    <h3>Cities</h3>
    <div class="search-form margin_form">
        <form>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="search[name]"
                               value="<?= $data->search && isset($data->search['name']) ? $data->search['name'] : ""; ?>"
                               class="form-control"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Country</label>
                        <select class="form-control" name="search[id_countries]">
                            <?php foreach ($data->ddl_countries as $value => $title) {
                                ?>
                                <option value="<?= $value; ?>" <?= $data->search && isset($data->search['id_countries']) && $data->search['id_countries'] == $value ? "selected" : ""; ?>><?= $title; ?></option>
                                <?php
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <button class="btn btn-success form-control">Search</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="js-cities" data-url="<?= clsUrls::getAjaxCityCommentsUrl(); ?>">
            <?php if ($data->all_cities) {
                foreach ($data->all_cities as $model_cities) {
                    ?>
                    <div class="col-md-6">
                        <div class="thumbnail">
                            <div class="city_thumb js-thumb"
                                 data-id_cities="<?= $model_cities->id; ?>">
                                <div class="row">
                                    <div class="col-md-4">
                                        City: <?= $model_cities->name; ?>
                                    </div>
                                    <div class="col-md-6">
                                        Country: <?= $model_cities->rel_countries->name; ?>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <button class="btn btn-success fa fa-comment js-show_comments">
                                        </button>
                                    </div>
                                    <div class="col-md-12">
                                        <p>Description:</p>
                                        <div><?= $model_cities->description?$model_cities->description:"<p>No description</p>"; ?></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="js-comment_holder">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div class="alert alert-info">
                    No cities to show
                </div>
                <?php
            } ?>
        </div>
    </div>
</section>