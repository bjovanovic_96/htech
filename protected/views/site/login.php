<div class="margin-top-heder">&nbsp;</div>

<div class="container-fluid login_container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="headline-center">
                <h3 class="title-login text-center"><?= Yii::t('main', 'Login'); ?></h3>
            </div>

            <!-- Login form - Start -->
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                //'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>

            <?php $this->renderPartial('_login', array('form' => $form, 'model' => $model)); ?>


            <div class="form-group text-left">
                <button type="submit" id="submit" class="button btn-continue btn-block btn-submit-validation"><?= Yii::t('main', 'Prijavi se'); ?></button>
            </div>

            <?php $this->endWidget(); ?>
            <!-- Login form - End -->
        </div>
    </div>
</div>


