<?php
$data=(object)[
        'ddl_countries'=>clsLoad::getDDLTblCountries()
];
?>
<div class="wide form">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'GET',
    ));
    ?>

    <div class="col-md-2 col-sm-12 col-xs-4 form-group">
        <?php echo $form->error($model, 'name'); ?>
        <?php echo $form->label($model, 'name'); ?>:
        <?php echo $form->textField($model, 'name', array('class' => 'form-control')); ?>
    </div>
    <div class="col-md-2 col-sm-12 col-xs-4 form-group">
        <?php echo $form->error($model, 'id_countries'); ?>
        <?php echo $form->label($model, 'id_countries'); ?>:
        <?php echo $form->dropDownList($model, 'id_countries', clsLoad::getDDLTblCountries(), array('class' => 'form-control'));
        ?>
    </div>
    <div class="col-md-2 col-sm-12 col-xs-4 form-group">
        <?php echo $form->error($model, 'page_size'); ?>
        <?php echo $form->label($model, 'page_size'); ?>:
        <?php echo $form->textField($model, 'page_size', array('class' => 'form-control')); ?>
    </div>

    <div class="col-md-2 col-sm-12 col-xs-12 mobile_right form-group">
        <label class="mobile_hide"> &nbsp;</label>
        <button type="submit" id="submit" class="btn btn-primary btn-block btn-submit-validation js_display_loader">
            Search
        </button>
    </div>


    <?php $this->endWidget(); ?>
</div><!-- search-form -->

