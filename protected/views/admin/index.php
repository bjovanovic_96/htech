
<div class="col-md-6 table-responsive-holder order-left-side js-order-table non-print js-audio-url" data-audio-url="<?= clsPath::getAudioUrl();?>" data-new-orders="<?= $data['new_order'] ? 'yes' : 'no'; ?>" data-selected-row="<?= $data['selected_id']; ?>" data-url="<?= clsUrls::getAjaxCheckNewOrderUrl(); ?>">

    <div class="hidden-sm hidden-xs">
        <div class="col-md-2 date_admin_style">
            Datum
        </div>
        <div class="col-md-4 date_admin_style">
            Korisnik
        </div>
        <div class="col-md-2 date_admin_style">
            Ukupno
        </div>
        <div class="col-md-4 date_admin_style">
            Stanje
        </div>
        <hr class="col-md-12"/> 
    </div>

    <div class="clearfix"></div>

    <?php
        $list_data = $data['list_view'];
        $fnc = $list_data['options']['fnc'];

        clsView::$fnc($list_data['this'], $list_data['data'], $list_data['options']['fnc_option']);
    ?>

</div>

<div class="col-md-6 col-xs-12 order-right-side js-order-details"></div>
