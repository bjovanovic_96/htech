<?php

class SiteController extends Controller
{

    function init()
    {
        parent::init();
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction'
            )
        );
    }

    //META DATA
    public $site_url = '';
    public $site_name = '';
    public $site_title = '';
    public $page_title = '';
    public $company_name = '';
    public $metaKeywords;
    public $metaUrl;
    public $metaTitle;
    public $metaDescription;
    public $metaImage;

    public function getCompanyName()
    {
        if (!$this->company_name) {
            return clsParams::getSystemOptions('company->name');
        }
        return $this->metaKeywords;
    }

    public function getMetaKeywords()
    {
        if (!$this->metaKeywords) {
            return clsParams::getSystemOptions('meta->keywords');
        }
        return $this->metaKeywords;
    }

    public function getMetaUrl()
    {
        if (!$this->metaUrl) {
            return clsParams::getSystemOptions('meta->site_url');
        }
        return $this->metaUrl;
    }

    public function getMetaTitle()
    {
        if (!$this->metaTitle) {
            return clsParams::getSystemOptions('meta->site_title');
        }
        return $this->metaTitle;
    }

    public function getMetaDescription()
    {
        if (!$this->metaDescription) {
            return clsParams::getSystemOptions('meta->description');
        }
        return $this->metaDescription;
    }

    public function getMetaImage()
    {
        if (!$this->metaImage) {
            return Yii::app()->request->getBaseUrl(true) . '/img/default/share_image.png';
        }
        return $this->metaImage;
    }

    public function checkLogin($model, $posted, $page = null)
    {
        $model->attributes = $posted;

        if ($model->validate() && $model->login()) {
            if (clsRole::is_admin()) {
                return $this->redirect(clsUrls::getAdminCitiesUrl());
            }
            if (clsRole::is_user()) {
                return $this->redirect(clsUrls::getHomeUrl());
            }
        }
        return $model;
    }

    //INDEX PAGE
    public function actionIndex()
    {
        //FUNCTION FOR CONNECT AIRPORTS,CITIES AND COUNTRIES
        if (false) {
            clsFunctions::db_edit();
        }
        //DELETE ALL AIRPORT AND ROUTES IF CITY IS EMPTY
        if (false) {
            clsFunctions::clear_airports_and_routes();
        }

        $this->page_title = 'Cities';
        if (clsRole::is_user() || clsRole::is_admin()) {
            $search = false;
            //SET FIRST LOAD
            $opt_sql = [
                'page_size' => 500
            ];
            if (isset($_GET['search']) && $_GET['search']) {
                $search = $_GET['search'];
                if (isset($search['name']) && $search['name']) {
                    $opt_sql['name'] = $search['name'];
                }
                if (isset($search['id_countries']) && $search['id_countries']) {
                    $opt_sql['id_countries'] = $search['id_countries'];
                }
            }
            $all_cities = clsLoad::findAllTblCities($opt_sql);
            $ddl_countries = clsLoad::getDDLTblCountries();
            $data = [
                'all_cities' => $all_cities,
                'ddl_countries' => $ddl_countries,
                'search' => $search,
            ];
            $this->render('index', ['data' => $data]);
        } else {
            return $this->redirect(clsUrls::getLoginUrl());
        }

    }

    public function actionRoutes()
    {
        $this->page_title = 'Routes';
        if (clsRole::is_user() || clsRole::is_admin()) {
            $ddl_cities = clsLoad::getDDLTblCities();
            $all_routes = false;
            $search = false;
            if (isset($_GET['search']) && $_GET['search']) {
                $search = $_GET['search'];
                if (isset($search['from_city']) && $search['from_city'] && isset($search['to_city']) && $search['to_city']) {
                    $opt_sql = [
                        'id_cities' => $search['from_city']
                    ];
                    $all_from_airports = clsLoad::findAllTblAirports($opt_sql);
                    $opt_sql = [
                        'id_cities' => $search['to_city']
                    ];
                    $all_to_airports = clsLoad::findAllTblAirports($opt_sql);
                    if ($all_from_airports && $all_to_airports) {
                        foreach ($all_from_airports as $model_from_airports) {
                            foreach ($all_to_airports as $model_to_airports) {
                                $opt_sql = [
                                    'source_airport_id' => $model_from_airports->id,
                                    'destination_airport_id' => $model_to_airports->id,
                                ];
                                $all_search_routes = clsLoad::findAllTblRoutes($opt_sql);
                                if ($all_search_routes) {
                                    if (!$all_routes) {
                                        $all_routes = [];
                                    }
                                    $all_routes = array_merge($all_routes, $all_search_routes);
                                }
                            }
                        }
                    }
                }
            }
            if ($all_routes) {
                $temp = $all_routes;
                $all_routes = [];
                foreach ($temp as $model_routes) {
                    $key = $model_routes->price + $model_routes->id;
                    $all_routes[$key] = $model_routes;
                }
                ksort($all_routes);
                $all_routes = array_values($all_routes);
            }
            $data = [
                'ddl_cities' => $ddl_cities,
                'all_routes' => $all_routes,
                'search' => $search,
            ];
            $this->render('routes', ['data' => $data]);
        } else {
            return $this->redirect(clsUrls::getLoginUrl());
        }

    }

    //LOGIN PAGE
    public function actionLogin()
    {
        $this->page_title = 'Login';
        $model = new LoginForm();

        if (isset($_POST['LoginForm'])) {
            $model = self::checkLogin($model, $_POST['LoginForm']);
        }

        $this->render('login', array(
            'model' => $model
        ));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}
