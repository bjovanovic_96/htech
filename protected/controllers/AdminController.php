<?php

class AdminController extends ControllerAdmin {

    function init() {
    }

    // Uncomment the following methods and override them if needed

    public function filters() {
        // return the filter configuration for this controller, e.g.:
        return array('accessControl');
    }

    public function actions() {
        // return external action classes, e.g.:
        return array(
            'action1' => 'path.to.ActionClass',
            'action2' => array(
                'class' => 'path.to.AnotherActionClass',
                'propertyName' => 'propertyValue',
            ),
        );
    }

    public $page_title = 'Admin';

    public function accessRules() {
        return clsRole::accessRulesAdminController();
    }

    public function filterAccessControl($filterChain) {
        $rules = $this->accessRules();

        // default deny
        $rules[] = array('deny');

        $filter = new CAccessControlFilter;
        $filter->setRules($rules);
        $filter->filter($filterChain);
    }

    /** Display pages */
    public function actionIndex($id = null) {
        $data = [];
        $this->render('index', array('data' => $data));
    }

    //BANNERS
    public function actionCitiesEdit($id = null) {
        $this->page_title = 'Cities | Admin';
        $data = array();
        $opt = (object) array(
                    'id' => $id,
                    'tbl_name' => 'TblCities',
        );

        $data = clsFunctions::createModelSave($opt);
        if (isset($_POST[$data['tbl_name']])) {
            $opt = (object) array(
                        'post' => $_POST[$data['tbl_name']],
                        'default_value' => array(),
                        'data' => $data,
                        'that' => $this,
            );

            $data = clsSave::modelSave($opt);
            if (isset($data['status']) && $data['status'] == "OK") {
                switch ($id) {
                    case "new":
                        clsForm::setFlashMsg('success', 'City <strong>' . $data['model']->name . '</strong> successfully created');
                        break;
                    default :
                        clsForm::setFlashMsg('success', 'City <strong>' . $data['model']->name . '</strong> successfully edited');
                        break;
                }

                $this->redirect(clsUrls::getAdminCitiesUrl());
            }
        }
        $model_cities = $data['model'];
        $data['title'] .= !$model_cities->isNewRecord ? ' / ' . $model_cities->name : '';
        $this->render('edit_page', array(
            'data' => $data
        ));
    }

    public function actionCities() {
        $this->page_title = 'Cities | Admin';
        $model = clsLoad::getDPTblCities();
        $tbl = 'TblCities';

        if (isset($_GET[$tbl])) {
            $opt = array(
                'model' => $model,
                'data' => $_GET[$tbl]
            );
            $model = clsFunctions::connectArrayValueWithModelAttributes($opt);
        }
        $model->order = clsFunctions::getOrderCol(get_class($model), $_GET);
        $opt = array(
            'view' => array(
                'title' => 'All cities', // Title of table
                'header_link' => clsUrls::getAdminCitiesEditUrl(['slug' => 'new', 'slug_id' => true]),
                'header_link_title' => 'Add city',
                // 'title_tag' => 'h1', // Title tag name
                'ajax_enable' => false, // Enable more rows ajax actions
                'search_enable' => false,
                // 'quick_search_enable' => false,
                'action_id' => $this->action->id
            ), // What action is active
            'grid' => array(
                'grid_id' => 'grid-products', // table ID
                'grid_data' => $model, // Data provider from model
                'columns' => 'gridCitiesColumns'
            ),
            'ajax' => array(),
            'search' => array(
                'quick_search_partial' => '_quick_search_cities'
            )
        );
        $this->render('view_page', array(
            'opt' => $opt
        ));
    }

}
