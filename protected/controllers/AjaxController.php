<?php

class AjaxController extends Controller
{

    function init()
    {
        parent::init();
    }

    //CITY COMMENTS
    public function actionCityComments()
    {
        $data = (object)[
            'id_cities' => isset($_POST['id_cities']) && $_POST['id_cities'] ? $_POST['id_cities'] : false,
            'id_city_comments' => isset($_POST['id_city_comments']) && $_POST['id_city_comments'] ? $_POST['id_city_comments'] : false,
            'number_comments' => isset($_POST['number_comments']) && $_POST['number_comments'] ? $_POST['number_comments'] : false,
            'form_data' => isset($_POST['form_data']) && $_POST['form_data'] ? $_POST['form_data'] : false,
            'action' => isset($_POST['action']) && $_POST['action'] ? $_POST['action'] : false,
        ];
        $response = [
            'status' => 'ERROR',
            'html' => ''
        ];
        switch ($data->action) {
            case "show":
                if ($data->id_cities) {
                    $opt_sql = [
                        'id' => $data->id_cities
                    ];
                    $model_cities = clsLoad::findTblCities($opt_sql);
                    if ($model_cities) {
                        $opt_sql = [
                            'id_cities' => $model_cities->id,
                            'page_size'=>$data->number_comments?$data->number_comments:''
                        ];
                        $all_city_comments = clsLoad::findAllTblCityComments($opt_sql);
                        $opt_partials = [
                            'all_city_comments' => $all_city_comments
                        ];
                        $response['html'] = Yii::app()->controller->renderPartial("//template/partials/city_comments", ["data" => $opt_partials], true, false);
                        $response['status'] = "OK";
                    }
                }
                break;
            case "edit":
                if ($data->id_cities) {
                    $opt_sql = [
                        'id' => $data->id_cities
                    ];
                    $model_cities = clsLoad::findTblCities($opt_sql);
                    if ($model_cities) {
                        $model_city_comments = new TblCityComments();
                        if ($data->id_city_comments) {
                            $opt_sql = [
                                'id_cities' => $model_cities->id,
                                'id' => $data->id_city_comments
                            ];
                            $model_city_comments = clsLoad::findTblCityComments($opt_sql);
                        }
                        if ($model_city_comments) {
                            if ($model_city_comments->isNewRecord) {
                                $model_city_comments->id_cities = $model_cities->id;
                                $model_city_comments->id_users = clsRole::get_user_id();
                                $model_city_comments->create_id = $model_city_comments->id_users;
                                $model_city_comments->create_date = clsDataFormat::sqlDateTime();
                                $model_city_comments->active = 1;
                            }else{
                                $model_city_comments->update_date = clsDataFormat::sqlDateTime();
                            }
                            $model_city_comments->comment = isset($data->form_data['comment']) && $data->form_data['comment'] ? $data->form_data['comment'] : null;
                            if ($model_city_comments->save()) {
                                $opt_sql = [
                                    'id_cities' => $model_cities->id
                                ];
                                $all_city_comments = clsLoad::findAllTblCityComments($opt_sql);
                                $opt_partials = [
                                    'all_city_comments' => $all_city_comments
                                ];
                                $response['html'] = Yii::app()->controller->renderPartial("//template/partials/city_comments", ["data" => $opt_partials], true, false);
                                $response['status'] = "OK";
                                $response['alert'] = $data->id_city_comments ? "Comment successfully edited" : "Comment successfully added";

                            } else {
                                $arr_errors = $model_city_comments->errors;
                                $response['errors'] = [];
                                foreach ($arr_errors as $attr => $errors) {
                                    $response['errors'][$attr] = $errors[0];
                                }
                                $response['status'] = "ERROR";
                                $response['alert'] = "Enter valid data";
                            }
                        }
                    }
                }
                break;
            case "show_edit":
                if ($data->id_cities) {
                    $opt_sql = [
                        'id' => $data->id_cities
                    ];
                    $model_cities = clsLoad::findTblCities($opt_sql);
                    if ($model_cities) {
                        if ($data->id_city_comments) {
                            $opt_sql = [
                                'id_cities' => $model_cities->id,
                                'id' => $data->id_city_comments
                            ];
                            $model_city_comments = clsLoad::findTblCityComments($opt_sql);
                            if ($model_city_comments) {
                                $opt_sql = [
                                    'id_cities' => $model_cities->id
                                ];
                                $all_city_comments = clsLoad::findAllTblCityComments($opt_sql);
                                $opt_partials = [
                                    'all_city_comments' => $all_city_comments,
                                    'model_city_comments' => $model_city_comments
                                ];
                                $response['html'] = Yii::app()->controller->renderPartial("//template/partials/city_comments", ["data" => $opt_partials], true, false);
                                $response['status'] = "OK";
                            }
                        }
                    }
                }
                break;
            case
            "delete":
                if ($data->id_cities) {
                    $opt_sql = [
                        'id' => $data->id_cities
                    ];
                    $model_cities = clsLoad::findTblCities($opt_sql);
                    if ($model_cities) {
                        if ($data->id_city_comments) {
                            $opt_sql = [
                                'id_cities' => $model_cities->id,
                                'id' => $data->id_city_comments
                            ];
                            $model_city_comments = clsLoad::findTblCityComments($opt_sql);
                            if ($model_city_comments) {
                                $model_city_comments->active = 0;
                                if ($model_city_comments->save()) {
                                    $opt_sql = [
                                        'id_cities' => $model_cities->id
                                    ];
                                    $all_city_comments = clsLoad::findAllTblCityComments($opt_sql);
                                    $opt_partials = [
                                        'all_city_comments' => $all_city_comments,
                                    ];
                                    $response['html'] = Yii::app()->controller->renderPartial("//template/partials/city_comments", ["data" => $opt_partials], true, false);
                                    $response['status'] = "OK";
                                    $response['alert'] = "Comment successfully deleted";
                                }
                            }
                        }
                    }
                }
                break;
        }
        die(json_encode($response));
    }
}
