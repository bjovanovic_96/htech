<?php

/**
 * This is the model class for table "tbl_airports".
 *
 * The followings are the available columns in table 'tbl_airports':
 * @property integer $id
 * @property string $name
 * @property string $city
 * @property string $country
 * @property string $iata
 * @property string $icao
 * @property string $latitude
 * @property string $longitude
 * @property string $altitude
 * @property integer $notused_4
 * @property string $notused_1
 * @property string $timezone
 * @property string $notused_2
 * @property string $notused_3
 */
class TblAirports extends CActiveRecord
{
    public $order;
    public $group;
    public $page_size;
    public $date_from;
    public $date_to;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_airports';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            // array('name, city, country, iata, icao, latitude, longitude, altitude, timezone', 'required', 'message' => Yii::t('default', 'model_required')),
            array('notused_4', 'numerical', 'integerOnly' => true, 'message' => Yii::t('default', 'model_integer_only')),
            array('iata, icao, notused_1', 'length', 'max' => 63, 'tooLong' => Yii::t('default', 'model_too_long')),
            array('notused_2, notused_3', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, city, country, iata, icao, latitude, longitude, altitude, notused_4, notused_1, timezone, notused_2, notused_3', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rel_cities' => array(self::BELONGS_TO, 'TblCities', 'id_cities'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'city' => 'City',
            'country' => 'Country',
            'iata' => 'IATA',
            'icao' => 'ICAO',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'altitude' => 'Altitude',
            'notused_4' => 'notused_4',
            'notused_1' => 'notused_1',
            'timezone' => 'Timezone',
            'notused_2' => 'notused_2',
            'notused_3' => 'notused_3',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'id');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'name', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'id_cities');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'city', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'id_countries');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'country', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'iata', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'icao', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'latitude', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'longitude', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'altitude', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'notused_4');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'notused_1', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'timezone', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'notused_2', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'notused_3', array('like' => true));

        if ($this->order) {
            $criteria->order = $this->order;
        } else {
            $attr = get_class($this) . '_sort';
            if (isset($_GET[$attr])) {
                $criteria->order = $_GET[$attr];
            } else {
                $criteria->order = 't.create_date DESC';
            }
        }

        if ($this->group) {
            $criteria->group = $this->group;
            $criteria->together = true;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->page_size ? $this->page_size : clsParams::getMaxPageSize(),
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TblAirports the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
