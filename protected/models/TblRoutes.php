<?php

/**
 * This is the model class for table "tbl_routes".
 *
 * The followings are the available columns in table 'tbl_routes':
 * @property string $airline
 * @property integer $airline_id
 * @property string $source_airport
 * @property string $source_airport_id
 * @property string $destination_airport
 * @property string $destination_airport_id
 * @property string $codeshare
 * @property integer $notused_1
 * @property string $stops_equipment
 * @property string $price
 */
class TblRoutes extends CActiveRecord
{
    public $order;
    public $group;
    public $page_size;
    public $date_from;
    public $date_to;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_routes';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('notused_1', 'required', 'message' => Yii::t('default', 'model_required')),
            array('airline_id, notused_1', 'numerical', 'integerOnly' => true, 'message' => Yii::t('default', 'model_integer_only')),
            array('airline, source_airport, source_airport_id, destination_airport, destination_airport_id, stops_equipment', 'length', 'max' => 4, 'tooLong' => Yii::t('default', 'model_too_long')),
            array('codeshare', 'length', 'max' => 3, 'tooLong' => Yii::t('default', 'model_too_long')),
            array('price', 'length', 'max' => 10, 'tooLong' => Yii::t('default', 'model_too_long')),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('airline, airline_id, source_airport, source_airport_id, destination_airport, destination_airport_id, codeshare, notused_1, stops_equipment, price', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rel_source_airports' => array(self::BELONGS_TO, 'TblAirports', 'source_airport_id'),
            'rel_destination_airports' => array(self::BELONGS_TO, 'TblAirports', 'destination_airport_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'airline' => 'Airline',
            'airline_id' => 'Airline ID',
            'source_airport' => 'Source airport',
            'source_airport_id' => 'Source airport ID',
            'destination_airport' => 'Destination airport',
            'destination_airport_id' => 'Destination airport ID',
            'codeshare' => 'Codeshare',
            'notused_1' => 'notused_1',
            'stops_equipment' => 'Stops equipment',
            'price' => 'Price',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'airline', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'airline_id');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'source_airport', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'source_airport_id', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'destination_airport', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'destination_airport_id', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'codeshare', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'notused_1');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'stops_equipment', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'price', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'create_id');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'create_date', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'active');

        if ($this->order) {
            $criteria->order = $this->order;
        } else {
            $attr = get_class($this) . '_sort';
            if (isset($_GET[$attr])) {
                $criteria->order = $_GET[$attr];
            } else {
                $criteria->order = 't.create_date DESC';
            }
        }

        if ($this->group) {
            $criteria->group = $this->group;
            $criteria->together = true;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->page_size ? $this->page_size : clsParams::getMaxPageSize(),
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TblRoutes the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
