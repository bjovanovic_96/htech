<?php

/**
 * This is the model class for table "tbl_cities".
 *
 * The followings are the available columns in table 'tbl_cities':
 * @property integer $id
 * @property string $name
 * @property integer $id_countries
 * @property integer $create_id
 * @property string $craete_date
 * @property integer $active
 */
class TblCities extends CActiveRecord
{
    public $order;
    public $group;
    public $page_size;
    public $date_from;
    public $date_to;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_cities';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name,description, id_countries', 'required', 'message' =>'{attribute} '. ' is required'),
            array('id_countries, create_id, active', 'numerical', 'integerOnly' => true, 'message' => Yii::t('default', 'model_integer_only')),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, id_countries, create_id, craete_date, active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rel_countries' => array(self::BELONGS_TO, 'TblCountries', 'id_countries'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'id_countries' => 'Country',
            'create_id' => 'Create ID',
            'craete_date' => 'Create date',
            'active' => 'Active',
        );
    }

    public function print_countries(){
        $return=false;
        if($this->id_countries){
            $model_countries=$this->rel_countries;
            $return=$model_countries?$model_countries->name:false;
        }
        return $return;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'id');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'name', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'description', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'id_countries');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'create_id');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'create_date', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'active');

        if ($this->order) {
            $criteria->order = $this->order;
        } else {
            $attr = get_class($this) . '_sort';
            if (isset($_GET[$attr])) {
                $criteria->order = $_GET[$attr];
            } else {
                $criteria->order = 't.create_date DESC';
            }
        }

        if ($this->group) {
            $criteria->group = $this->group;
            $criteria->together = true;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->page_size ? $this->page_size : clsParams::getMaxPageSize(),
            ),
        ));
    }

    public function attribute_form($form_id) {
        $return = array();

        switch ($form_id) {
            case 'create-cities-form':
                $return = array(
                    /**
                     * block 0
                     */
                    (object) array(
                        'element' => (object) array(
                            'attr' => 'name',
                            'type' => 'textFieldGroup',
                            'class' => 'col-md-4 col-xs-12',
                            'opt' => array(
                                'htmlOptions' => array(
                                    'class' => 'form-control'
                                )
                            )
                        ),
                        'display' => (object) array(
                            'block' => 0,
                            'order' => 0,
                            'visible' => true
                        )
                    ),
                    (object) array(
                        'element' => (object) array(
                            'attr' => 'id_countries',
                            'type' => 'DDLGroup',
                            'class' => 'col-md-4 col-xs-12',
                            'opt' => array(
                                'htmlOptions' => array(
                                    'class' => 'form-control js-select2-ddl'
                                ),
                                'value' => clsLoad::getDDLTblCountries(),
                            )
                        ),
                        'display' => (object) array(
                            'block' => 0,
                            'order' => 1,
                            'visible' => true
                        )
                    ),
                    (object)array(
                        'element' => (object)array(
                            'attr' => 'description',
                            'type' => 'TextEditorGroup',
                            'class' => 'col-md-12 col-xs-12',
                            'opt' => array(
                                'htmlOptions' => array(
                                    'class' => 'form-control'
                                )
                            )
                        ),
                        'display' => (object)array(
                            'block' => 0,
                            'order' => 2,
                            'visible' => true
                        )
                    ),
                );

                break;
        }
        return $return;
    }

    public function attribute_block() {
        return array(
            0 => (object) array(
                'create-cities-form' => (object) array(
                    'class' => 'col-md-12 col-xs-12',
                    'title' => Yii::t('main', 'Podaci o gradu')
                )
            ),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TblCities the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
