<?php

/**
 * This is the model class for table "tbl_countries".
 *
 * The followings are the available columns in table 'tbl_countries':
 * @property integer $id
 * @property string $name
 * @property integer $create_id
 * @property string $create_date
 * @property integer $active
 */
class TblCountries extends CActiveRecord
{
    public $order;
    public $group;
    public $page_size;
    public $date_from;
    public $date_to;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_countries';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, create_id, create_date, active', 'required', 'message' => Yii::t('default', 'model_required')),
            array('create_id, active', 'numerical', 'integerOnly' => true, 'message' => Yii::t('default', 'model_integer_only')),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, create_id, create_date, active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    public function attributeLabels()
    {
        return array(
            'id' => "ID",
            'name' => 'Name',
            'create_id' => 'Create user',
            'create_date' => 'Create date',
            'active' => 'Active',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'id');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'name');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'create_id');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'create_date', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'active');

        if ($this->order) {
            $criteria->order = $this->order;
        } else {
            $attr = get_class($this) . '_sort';
            if (isset($_GET[$attr])) {
                $criteria->order = $_GET[$attr];
            } else {
                $criteria->order = 't.create_date DESC';
            }
        }

        if ($this->group) {
            $criteria->group = $this->group;
            $criteria->together = true;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->page_size ? $this->page_size : clsParams::getMaxPageSize(),
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TblCountries the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
