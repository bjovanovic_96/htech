<?php

/**
 * This is the model class for table "tbl_user_access".
 *
 * The followings are the available columns in table 'tbl_user_access':
 * @property integer $id
 * @property integer $id_users
 * @property string $username
 * @property string $password
 * @property string $salt
 * @property integer $create_id
 * @property string $create_date
 * @property integer $active
 */
class TblUserAccess extends CActiveRecord
{
    public $order;
    public $group;
    public $page_size;
    public $date_from;
    public $date_to;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_user_access';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_users,id_roles, username, password, salt, create_id, create_date, active', 'required', 'message' => Yii::t('default', 'model_required')),
            array('id_users,id_roles, create_id, active', 'numerical', 'integerOnly' => true, 'message' => Yii::t('default', 'model_integer_only')),
            array('salt', 'length', 'max' => 63, 'tooLong' => Yii::t('default', 'model_too_long')),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, id_users, username, password, salt, create_id, create_date, active', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'rel_users' => array(self::BELONGS_TO, 'TblUsers', 'id_users'),
            'rel_roles' => array(self::BELONGS_TO, 'TblRoles', 'id_roles'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_users' => 'User ID',
            'id_roles' => 'Role ID',
            'username' => 'Username',
            'password' => 'Password',
            'salt' => 'Salt',
            'create_id' => 'Create user',
            'create_date' => 'Create date',
            'active' => 'Active',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'id');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'id_users');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'id_roles');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'username', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'password', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'salt', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'create_id');
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'create_date', array('like' => true));
        $criteria = clsFunctions::SetCriteriaCompare($this, $criteria, 'active');

        if ($this->order) {
            $criteria->order = $this->order;
        } else {
            $attr = get_class($this) . '_sort';
            if (isset($_GET[$attr])) {
                $criteria->order = $_GET[$attr];
            } else {
                $criteria->order = 't.create_date DESC';
            }
        }

        if ($this->group) {
            $criteria->group = $this->group;
            $criteria->together = true;
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->page_size ? $this->page_size : clsParams::getMaxPageSize(),
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TblUserAccess the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
