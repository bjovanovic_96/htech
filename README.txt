U realizaciji taska koriscena osnova iz PHP Yii framework-a
Na osnovu Yii framework-a dodate custom klase koje se nalaze u folderu components
Klase u folderu components:
 - clsDataFormat: koristi se za formatiranje podataka (datumi, brojevi, vreme) u formate za prikaz i unos u bazu
 - clsForm: koristi se da definisanje polja (tekstualna, brojevi, editori, picker-i datuma i vremena, kroper slika,...) za unos i izmenu u admin delu
 - clsFunctions: funkcije sa globalnu obradu podataka (konvertovanje nizova, osnovne funkcije konekcije i pretage baze, obrada podataka dobijenih iz baze, ...)
 - clsLoad: klasa za rad sa bazom podataka (podesavanje upita, konekcija sa clsFunctions za obradu dobijenih podataka, pojedinacna konekcija sa svakom tabelom u bazi)
 - clsMail: klasa za rad sa mejlovima (podesavanje naslova, sadrzaja mejla, slanje mejla)
 - clsMailTemplates: klasa namenjena za podesavanje template-a mejlova (trenutno je prazna iz razloga sto u projektu nije bilo potrebno definisanje ove klase)
 - clsParams: klasa za staticke podatke (staticke nizove, emial adrese, status, osnovne podatke o projektu)
 - clsPath: klasa za definisanje putanja i adresa ka folderima (slike, video,...)
 - clsRole: klasa za proveru dozvola pristupa korisnika u sistemu
 - clsSave: klasa za cuvanje i validaciju podataka u bazi
 - clsUrls: klasa za definisanje url adresa ka stranicama i akcijama
 - clsView: klasa za definisanje globalnog prikaza podataka u admin delu (primer: template za tabelarni prikaz gradova u admin delu)
 - clsViewData: klasa koja radi paralelno sa clsView. U ovoj klasi se definisu prikazi pojedinacnih tabela u admin delu
 - dev: klasa za ispis i pracenje koda

Nacin pokretanja projekata: za pokretanje projekata potreban je virtuelni server(xampp). Kopirati projekata u folder htdocs. Posle prebacivanja projekata u folder htdocs potrebno podesiti bazu.
Baza podataka se nalazi u folderu sql na root projekta. Projekat je podesen da radi bez zakljucavanja baze i naziv baze treba da bude "htech". 
U slucaj definisanja drugog naziva baze i promene pristupnih podataka potrebno je promeniti parametre u u fajlu protected/config/main.php (linija koda: 78-84)

Rad projekta zasnovan je na tri kontrolera koji se nalaze na adresi protected/controllers:
 - AdminController: kontroler za administratorski deo sistem (prikaz i izmena gradova)
 - AjaxController: kontroler za komunikaciju sistema preko REST upita (relacija: JS (jQuery) -> AjaxController)
 - SiteControler: kontroler za prikaz korisnickih podataka (stranica gradovi i rute)

U folderu na putanji protected/views nalaze se stranice za prikaz podataka koji se obradjuju u kontrolerima (folder site komunikacija sa SiteController-om, folder admin komunikacija sa AdminController)
U folderu na putanji protected/views/layouts nalaze se layout-i stranica (povezivanje skripti, css fajlova, podesavanje meta podataka, ...)

U folderu na putanji protected/models nalaze se klase za konekciju sa bazom (klase su definisanje tako da u nazivu direktno sadrze naziv tabele u bazi sa kojom rade konekciju)

Sturktura baze:

Svi nazivi tabela pocinju sa prefiksom "tbl_". Sve tabele imaju svoje primarne kljuceve definisane sa kolonom "id". Sve table imaju kolone create_id, create_date, active.
 - kolona create_id: koristi se za cuvanje podatka koji je korisnika kreirao dati red u bazi
 - kolona create_date: koristi se za cuvanje datuma i vremena kada je red kreiran
 - kolona active: moze imati vrednosti 0 i 1 i definise da li je neki podataka i aktivan ili ne u sistemu (glavna primena je da se izbegne trajno brisanje podataka iz baze)

Baza se sastoji od tabela:
 - tbl_airports: tabela sa podacima o aerodromima (u tabeli postoje kolone koje pocinju sa nazivom "notused_" i one oznacavaju podatke koji su dobijeni u airports.txt fajlu, a nisu definisani u samom tasku i koja je njihova primena)
 - tbl_cities: tabela za gradove (kolona id_countries predstavlja relaciju sa tabelom tbl_countries)
 - tbl_city_comments: tabela za cuvanje komentara za gradove (kolone id_cities - relacija sa tabelom tbl_cities, id_users - relacija sa tabelom tbl_users)
 - tbl_countries: tabela za drzave
 - tbl_roles: tabela za uloge korisnika u sistemu
 - tbl_routes: tabela za cuvanje letova (u tabeli postoje kolone koje pocinju sa nazivom "notused_" i one oznacavaju podatke koji su dobijeni u routes.txt fajlu, a nisu definisani u samom tasku i koja je njihova primena)
 - tbl_users: tabela za cuvanje osnovnih podataka o korsinicima
 - tbl_user_access: tabela za definisanje pristupnih podataka i dozvola pristupa korisnika (kolona "salt" ima vrednost koja se koristi kao prefix pri definisanju sifre korisnika)

U sistemu postoje kreirana dva korisnika: posetilac i administrator.
U slucaju da niste ulogovani prilikom pristupanja projektu otvara se stranica za logovanje.

Pristupni podaci:
 administratorski nalog- username: admin password: admin
 korisnicki nalog - username: user password: user

U projektu su realizovane svi zahtevi osim pretrage putanja u slucaju da se gradovi ne nalaze jedan do drugog. Prilikom resavanja ovo problema pokusao sam da koristim "Dijkstra" algoritam, ali zbog broja gradova nisam uspeo da kreiram matricu gradova da bih odradio pretragu svakog sa svakim.
Primer za testiranje stranice routes u korisnickom delu:htech/routes?search%5Bfrom_city%5D=3776&search%5Bto_city%5D=4421