<?php
/**
 * TbApi class file.
* @author Christoffer Niska <christoffer.niska@gmail.com>
* @copyright Copyright &copy; Christoffer Niska 2013-
* @license http://www.opensource.org/licenses/bsd-license.php New BSD License
* @package bootstrap.components
* @version 1.2.0
*/

/**
 * Bootstrap API component.
*/
class TextEditorInit extends CApplicationComponent
{
    /**
     * @var bool whether we should copy the asset file or directory even if it is already published before.
     */
    public $forceCopyAssets = false;
    
    private $_assetsUrl;

    public function registerCoreScripts($url = null, $position = CClientScript::POS_END)
    {
        if ($url === null) {
            //$fileName = YII_DEBUG ? 'ckeditor.js' : 'ckeditor.js';
            $fileName = 'ckeditor.js';
            $url = $this->getAssetsUrl() . '/ckeditor/' . $fileName;
        }
        /** @var CClientScript $cs */
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile($url, $position);
    }
    /**
     * Registers all assets.
     */
    public function register()
    {
        $this->registerCoreScripts();
    }

    /**
     * Returns the url to the published assets folder.
     * @return string the url.
     */
    protected function getAssetsUrl()
    {
        if (isset($this->_assetsUrl)) {
            return $this->_assetsUrl;
        } else {
            $assetsPath = Yii::getPathOfAlias('texteditor');
            $assetsUrl = Yii::app()->assetManager->publish($assetsPath, false, -1, $this->forceCopyAssets);
            return $this->_assetsUrl = $assetsUrl;
        }
    }

}
