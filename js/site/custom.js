//SHOW CITY COMMENTS
function show_comments($opt = null) {
    $opt = {
        thumb: $opt && 'thumb' in $opt ? $opt.thumb : false,
        validate_active: $opt && 'validate_active' in $opt ? $opt.validate_active : true,
    };
    if ($opt.thumb) {
        var $btn_comment = $opt.thumb.find('.js-show_comments');
        var $thumb = $opt.thumb;
        var $holder = $thumb.closest('.js-cities');
        var $comment_holder = $thumb.find('.js-comment_holder');
        if ($opt.validate_active && $btn_comment.hasClass('active')) {
            $btn_comment.removeClass('active');
            $comment_holder.empty();
        } else {
            var $input_number_comments=$comment_holder.find('[name="number_comments"]');
            var $url = $holder.attr('data-url');
            var $data = {
                id_cities: $thumb.attr('data-id_cities'),
                number_comments: $input_number_comments.length>0?$input_number_comments.val():'',
                action: "show"
            };
            $.ajax({
                url: $url,
                data: $data,
                type: 'post',
                success: function ($response) {
                    var $response = jQuery.parseJSON($response);
                    switch ($response.status) {
                        case "OK":
                            $comment_holder.empty();
                            $($response.html).appendTo($comment_holder);
                            $btn_comment.addClass('active');
                            break;
                    }
                }
            });
        }
    }
}
//BTN SHOW COMMENT
$(document).on('change', '.js-cities .js-comment_holder [name="number_comments"]', function (event) {
    event.preventDefault();
    var $obj = $(this);
    var $thumb = $obj.closest('.js-thumb');
    var $opt_fnc = {
        thumb: $thumb,
        validate_active:false
    };
    show_comments($opt_fnc);
});
//CHANGE NUMBER COMMENTS
$(document).on('click', '.js-cities .js-show_comments', function (event) {
    event.preventDefault();
    var $obj = $(this);
    var $thumb = $obj.closest('.js-thumb');
    var $opt_fnc = {
        thumb: $thumb
    };
    show_comments($opt_fnc);
});
//EDIT / SAVE COMMENT
$(document).on('click', '.js-cities .js-thumb .js-form .js-edit', function (event) {
    event.preventDefault();
    var $obj = $(this);
    var $thumb = $obj.closest('.js-thumb');
    var $holder = $thumb.closest('.js-cities');
    var $url = $holder.attr('data-url');
    var $form = $obj.closest('.js-form');
    //CLEAR ERRORS
    $form.find('.input_error').removeClass('input_error');

    var $form_data = {};
    var $all_inputs = $form.find('[name]');
    if ($all_inputs.length > 0) {
        $.each($all_inputs, function ($index, $input) {
            $input = $($input);
            var $name = $input.attr('name');
            var $value = $input.val();
            $form_data[$name] = $value;
        });
    }
    var $data = {
        id_cities: $thumb.attr('data-id_cities'),
        id_city_comments: $form.attr('data-id_city_comments'),
        form_data: $form_data,
        action: "edit"
    };
    $.ajax({
        url: $url,
        data: $data,
        type: 'post',
        success: function ($response) {
            var $response = jQuery.parseJSON($response);
            switch ($response.status) {
                case "OK":
                    var $comment_holder = $thumb.find('.js-comment_holder');
                    $comment_holder.empty();
                    $($response.html).appendTo($comment_holder);
                    successMsgAlert($response.alert);
                    break;
                case "ERROR":
                    $.each($response.errors, function ($name, $error) {
                        var $input = $form.find('[name="' + $name + '"]');
                        if ($input.length > 0) {
                            $input.addClass('input_error');
                        }
                    });
                    errorMsgAlert($response.alert);
                    break;
            }
        }
    });
});
//SHOW EDIT COMMENT
$(document).on('click', '.js-cities .js-thumb .js-comment_row .js-edit', function (event) {
    event.preventDefault();
    var $obj = $(this);
    var $thumb = $obj.closest('.js-thumb');
    var $holder = $thumb.closest('.js-cities');
    var $comment_row = $obj.closest('.js-comment_row');
    var $url = $holder.attr('data-url');
    var $data = {
        id_cities: $thumb.attr('data-id_cities'),
        id_city_comments: $comment_row.attr('data-id_city_comments'),
        action: "show_edit"
    };
    $.ajax({
        url: $url,
        data: $data,
        type: 'post',
        success: function ($response) {
            var $response = jQuery.parseJSON($response);
            switch ($response.status) {
                case "OK":
                    var $comment_holder = $thumb.find('.js-comment_holder');
                    $comment_holder.empty();
                    $($response.html).appendTo($comment_holder);
                    break;
            }
        }
    });
});
//DELETE COMMENT
$(document).on('click', '.js-cities .js-thumb .js-comment_row .js-delete', function (event) {
    event.preventDefault();
    var $obj = $(this);
    var $confirm_text = $obj.attr('data-confirm_text');
    if (confirm($confirm_text)) {
        var $thumb = $obj.closest('.js-thumb');
        var $holder = $thumb.closest('.js-cities');
        var $comment_row = $obj.closest('.js-comment_row');
        var $url = $holder.attr('data-url');
        var $data = {
            id_cities: $thumb.attr('data-id_cities'),
            id_city_comments: $comment_row.attr('data-id_city_comments'),
            action: "delete"
        };
        $.ajax({
            url: $url,
            data: $data,
            type: 'post',
            success: function ($response) {
                var $response = jQuery.parseJSON($response);
                switch ($response.status) {
                    case "OK":
                        var $comment_holder = $thumb.find('.js-comment_holder');
                        $comment_holder.empty();
                        $($response.html).appendTo($comment_holder);
                        successMsgAlert($response.alert);
                        break;
                }
            }
        });
    }
});
