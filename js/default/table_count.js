$(document).ready(function(){    
    $('.sortable-table').each(function(){
        var $table_cls = $(this).attr('data-cls');
        calculate_row_total_values('.'+$table_cls);
        calculate_column_total_values('.'+$table_cls);   
    });    
});

$(document).on('click', '.filter-stat-btn', function(){
   var $table = $(this).attr('data-table');
   var $cell = $(this).attr('data-cell');
   $($table).find('.'+$cell).toggle();
   // sminka
   var $obj_icon = $(this).find('i');
   if($obj_icon.hasClass('fa-eye')){
       $obj_icon.removeClass('fa-eye').addClass('fa-eye-slash');
       $obj_icon.closest('div').addClass('btn-warning');
   } else {
       $obj_icon.removeClass('fa-eye-slash').addClass('fa-eye');
       $obj_icon.closest('div').removeClass('btn-warning');
   }
   // racunanje
   calculate_row_total_values($table);
   calculate_column_total_values($table);
});

function calculate_column_total_values($table_name){
    var $col_arr = $($table_name).attr('data-cells');
    $col_arr = $.parseJSON($col_arr);

    $.each($col_arr, function($i, $row) {
        var $sum_cell = $row.val_cell;
        var $sum_obj = $($table_name).find('tbody').find($sum_cell);
        var $sum = Number(0);
        $($sum_obj).each(function() {
            var $num = $(this).text();
            $num = num_encode($num);
            if(Math.floor($num) == $num && $.isNumeric($num)){
                $sum += Number($num);
            }
        });
        
        //$sum = num_decode($sum);
        $($table_name).find($sum_cell+"_total").text($sum);
    });
    return false;
}

function calculate_row_total_values($table_name){
    var $table_row = $($table_name).find('.otp_stat_row');
    
    $table_row.each(function(){
        var $row = $(this).find('td');
        var $sum = Number(0);
        $row.each(function(){
            if($(this).hasClass('sum') && $(this).css('display') !== 'none'){
                var $num = $(this).text();
                $num = num_encode($num);
                $sum += Number($num);
            }
        });
        //$sum = num_decode($sum);
        $(this).find('.cls_total').text($sum);
    });
}

function num_encode($num, $decimal) {
    $decimal = ($.isNumeric($decimal)) ? $decimal : 2;
    $num = $num.toString();
    $num = $num.replace('.', '');
    $num = $num.replace('.', '');
    $num = $num.replace('.', '');
    $num = $num.replace('.', '');
    $num = $num.replace(',', '.');
    $num = Number($num).toFixed($decimal);
    return $num;
}

function num_decode($num, $decimal) {
    $decimal = ($.isNumeric($decimal)) ? $decimal : 2;
    $num = addCommas($num, $decimal);
    return $num;
}

function addCommas(nStr, $decimal) {
    nStr = nStr.toString();
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? ',' + pad(x[1], $decimal) : ','+pad('00', $decimal);
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

function pad(str, max) {
  str = str.toString();
  return str.length < max ? pad( str + "0", max) : str;
}