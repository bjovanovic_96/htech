"use strict";

//Alert success message
function successMsgAlert($message) {
    console.log($message);
    var $holder = $('.js-msg-alert-holder');

    hideMsgAlert($holder);
    $holder.removeClass('hide');
    $holder.attr('style', 'display:block;');
    $holder.addClass('alert-success');
    $holder.find('.js-msg-holder').append($message);
    $holder.delay(2000).fadeOut(1000);

    return false;
}

//Alert error message
function errorMsgAlert($obj) {
    var $holder = $('.js-msg-alert-holder');

    hideMsgAlert($holder);

    $holder.removeClass('hide');
    $holder.attr('style', 'display:block;');
    $holder.addClass('alert-danger');
    $holder.find('.js-msg-holder').append($obj);
    $holder.delay(2000).fadeOut(1000);

    return false;
}

//hide message
function hideMsgAlert($holder) {
    $holder.addClass('hide');
    $holder.removeClass('alert-success');
    $holder.removeClass('alert-danger');
    $holder.find('.js-msg-holder').html('');
}

