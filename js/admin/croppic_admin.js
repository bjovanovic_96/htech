/*
 * CROPPIC
 * dependancy: jQuery
 * author: Ognjen "Zmaj Džedaj" Božičković and Mat Steinlin
 */

(function (window, document) {

    Croppic = function (id, options) {

        var that = this;
        that.id = id;
        that.obj = $('#' + id);
        that.outputDiv = that.obj;

        // DEFAULT OPTIONS
        that.options = {
            modal_id: '#MyModal',
            holder_id: '',
            member_image_id: '#member-image',
            width: '',
            height: '',
            file_name: '',
            folder_path_name: '',
            folder_url_name: '',
            uploadUrl: '',
            uploadData: {},
            cropUrl: '',
            cropData: {},
            outputUrlId: '',
            imgSize: 128576000,
            //styles
            imgEyecandy: true,
            imgEyecandyOpacity: 0.2,
            zoomFactor: 80,
            rotateFactor: 90,
            doubleZoomControls: false,
            rotateControls: true,
            modal: false,
            customUploadButtonId: '',
            loaderHtml: '',
            scaleToFill: false,
            processInline: false,
            loadPicture: '',
            onReset: null,
            enableMousescroll: true,
            //callbacks
            onBeforeImgUpload: null,
            onAfterImgUpload: null,
            onImgDrag: null,
            onImgZoom: null,
            onImgRotate: null,
            onBeforeImgCrop: null,
            onAfterImgCrop: null,
            onBeforeRemoveCroppedImg: null,
            onAfterRemoveCroppedImg: null,
            onError: null,
        };

        // OVERWRITE DEFAULT OPTIONS
        for (i in options)
            that.options[i] = options[i];

        // INIT THE WHOLE DAMN THING!!!
        that.init();


        // added 13.01.2016
        $(that.options.holder_id + ' .image-preview-clear').on('click', function () {
            that.reset();
            $(that.options.holder_id + ' .image-preview-clear').css('display', 'none');
            $(that.options.holder_id + ' .image-preview-filename').val('');
            $(that.options.member_image_id).attr('value', '');
            $(that.options.modal_id + ' .croppedImg').remove();
            $(that.options.modal_id + ' .cropControls').remove();
            $(that.options.modal_id + ' .float-ok-status-text').remove();
            $(that.options.modal_id + ' .float-ok-status-holder').remove();
        });
        // end 13.01.2016
    };

    Croppic.prototype = {
        id: '',
        imgInitW: 0,
        imgInitH: 0,
        imgW: 0,
        imgH: 0,
        objW: 0,
        objH: 0,
        actualRotation: 0,
        windowW: 0,
        windowH: $(window).height(),
        $img_name: '',
        obj: {},
        outputDiv: {},
        outputUrlObj: {},
        img: {},
        defaultImg: {},
        croppedImg: {},
        imgEyecandy: {},
        form: {},
        iframeform: {},
        iframeobj: {},
        cropControlsUpload: {},
        cropControlsCrop: {},
        cropControlZoomMuchIn: {},
        cropControlZoomMuchOut: {},
        cropControlZoomIn: {},
        cropControlZoomOut: {},
        cropControlCrop: {},
        cropControlReset: {},
        cropControlRemoveCroppedImage: {},
        modal: {},
        loader: {},
        init: function () {
            var that = this;

            //console.log('init', that.obj);

            //that.objW = (that.width) ? that.width : that.obj.width();
            //that.objH = (that.height) ? that.height : that.obj.height();

            //console.log('that.objW', that.objW);
            //console.log('that.objH', that.objH);

            that.objW = that.obj.width();
            that.objH = that.obj.height();
            // reset rotation
            that.actualRotation = 0;

            if ($.isEmptyObject(that.defaultImg)) {
                that.defaultImg = that.obj.find('img');
            }

            that.createImgUploadControls();

            $(that.options.holder_id + ' .croppic_imgUploadForm').remove();

            //console.log('AAAAa');

            //console.log('that.options', that.options);

            if ($.isEmptyObject(that.options.loadPicture)) {
                that.bindImgUploadControl();
            } else {
                that.loadExistingImage();
            }
        },
        createImgUploadControls: function () {
            //console.log('createImgUploadControls');
            var that = this;

            var cropControlUpload = '';
            if (that.options.customUploadButtonId === '') {
                cropControlUpload = '<i class="cropControlUpload"></i>';
            }

            var cropControlRemoveCroppedImage = '<i class="cropControlRemoveCroppedImage" title="Izbriši sliku"></i>';

            if ($.isEmptyObject(that.croppedImg)) {
                cropControlRemoveCroppedImage = '';
            }
            if (!$.isEmptyObject(that.options.loadPicture)) {
                cropControlUpload = '';
            }

            // added 13.01.2016
            //console.log('cropControlRemoveCroppedImage', cropControlRemoveCroppedImage);
            //var html = '<div class="cropControls cropControlsUpload"> ' + cropControlUpload + cropControlRemoveCroppedImage + ' </div>';
            var html = '<div class="cropControls cropControlsUpload" style="visibility: hidden;"> ' + cropControlUpload + cropControlRemoveCroppedImage + ' </div>';
            //console.log('html', html);
            // end 13.01.2016

            that.outputDiv.append(html);

            that.cropControlsUpload = that.outputDiv.find('.cropControlsUpload');

            if (that.options.customUploadButtonId === '') {
                that.imgUploadControl = that.outputDiv.find('.cropControlUpload');
                //console.log('that.options.customUploadButtonId');
            } else {
                //console.log('that.imgUploadControl', that.imgUploadControl);

                that.imgUploadControl = $('#' + that.options.customUploadButtonId);
                that.imgUploadControl.show();
            }

            if (!$.isEmptyObject(that.croppedImg)) {

                that.cropControlRemoveCroppedImage = that.outputDiv.find('.cropControlRemoveCroppedImage');
            }

            //console.log('$(.cropControlZoomOut).trigger(click)');
            //$('.cropControlZoomOut').trigger('click');
        },
        bindImgUploadControl: function () {
            //console.log('bindImgUploadControl');

            var that = this;

            // CREATE UPLOAD IMG FORM
            var formHtml = '<form class="' + that.id + '_imgUploadForm" style="visibility: hidden; position: absolute;">  <input type="file" accept="image/png, image/jpeg"  name="img" id="' + that.id + '_imgUploadField" />  </form>';
            that.outputDiv.append(formHtml);

            //console.log('that.outputDiv.append(formHtml);', formHtml );
            //console.log('that.outputDiv.append(formHtml);', that.outputDiv );

            that.form = that.outputDiv.find('.' + that.id + '_imgUploadForm');

//console.log('that.outputDiv.append(formHtml);  that.form', that.form );

            // CREATE FALLBACK IE9 IFRAME
            var fileUploadId = that.CreateFallbackIframe();

            //console.log('1');

            that.imgUploadControl.off('click');
            that.imgUploadControl.on('click', function () {
                //console.log('!!!!!!!!!!!!!!!!!!!');
                if (fileUploadId === "") {
                    //console.log('fileUploadId === ""');

                    that.reset();



                    that.form.find('input[type="file"]').trigger('click');

                    // $(that.options.holder_id).find('input[type="file"]').trigger('click');

                    //$('#' + that.id + '_imgUploadField').trigger('click');

                    //$(that.options.modal_id).modal('show');

                } else {
                    //console.log('fileUploadId => that.iframeform', that.iframeform);
                    //Trigger iframe file input click, otherwise access restriction error
                    that.iframeform.find('input[type="file"]').trigger('click');
                }
            });

            if (!$.isEmptyObject(that.croppedImg)) {
                that.cropControlRemoveCroppedImage.on('click', function () {
                    if (typeof (that.options.onBeforeRemoveCroppedImg) === typeof (Function)) {
                        that.options.onBeforeRemoveCroppedImg.call(that);
                    }

                    that.croppedImg.remove();
                    that.croppedImg = {};
                    $(this).hide();

                    if (typeof (that.options.onAfterRemoveCroppedImg) === typeof (Function)) {
                        that.options.onAfterRemoveCroppedImg.call(that);
                    }

                    if (!$.isEmptyObject(that.defaultImg)) {
                        that.obj.append(that.defaultImg);
                    }

                    if (that.options.outputUrlId !== '') {
                        $('#' + that.options.outputUrlId).val('');
                    }
                });
            }

            //console.log('2');

            that.form.find('input[type="file"]').change(function () {

                //console.log('that.form.find(input[type="file"]).change');

                if (that.options.onBeforeImgUpload)
                    that.options.onBeforeImgUpload.call(that);

                if (!that.checkSize(that, that.options.imgSize)) {
                    return false;
                }

                // added 13.01.2016
                //that.imgUploadControl.hide();
                /*if(that.form.find("input[type=file]").files.length == 0){
                 alert('Nije odobran fajl');
                 that.hideLoader();
                 return false;
                 }*/

                that.showLoader();

                that.$img_name = that.form.closest('.js-crop-holder').attr('data-core_image_name');

                $(that.options.modal_id + ' .modal-footer .btn-modal-crop-image').removeClass('display-none');
                $(that.options.modal_id + ' .modal-footer .btn-modal-reset-image').removeClass('display-none');

                $(that.options.modal_id + ' .modal-body .float-ok-status-text').remove();

                // end 13.01.2016

                if (that.options.processInline) {
                    // Checking Browser Support for FileReader API
                    if (typeof FileReader == "undefined") {
                        if (that.options.onError)
                            that.options.onError.call(that, "processInline is not supported by your Browser");
                        that.reset();
                    } else {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            var image = new Image();

                            image.src = e.target.result;
                            image.onload = function () {
                                that.imgInitW = that.imgW = image.width;
                                that.imgInitH = that.imgH = image.height;

                                if (that.options.modal) {
                                    that.createModal();
                                }
                                if (!$.isEmptyObject(that.croppedImg)) {
                                    that.croppedImg.remove();
                                }

                                that.imgUrl = image.src;


                                // load background image
                                that.obj.append('<img src="' + image.src + '">');

                                $(that.options.modal_id + ' .cropImgWrapper').remove();
                                $(that.options.modal_id + ' .cropControls').remove();

                                that.initCropper();
                                that.hideLoader();

                                if (that.options.onAfterImgUpload)
                                    that.options.onAfterImgUpload.call(that);

                            }
                        };
                        reader.readAsDataURL(that.form.find('input[type="file"]')[0].files[0]);
                    }
                } else {

                    try {
                        // other modern browsers
                        formData = new FormData(that.form[0]);
                    } catch (e) {
                        // IE10 MUST have all form items appended as individual form key / value pairs
                        formData = new FormData();
                        formData.append('img', that.form.find("input[type=file]")[0].files[0]);

                    }

                    for (var key in that.options.uploadData) {
                        if (that.options.uploadData.hasOwnProperty(key)) {
                            formData.append(key, that.options.uploadData[key]);
                        }
                    }

                    $.ajax({
                        url: that.options.uploadUrl,
                        data: formData,
                        context: document.body,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST'
                    }).always(function (data) {
                        that.afterUpload(data);
                    });
                }

                // added 18.01.2016
                $(that.options.holder_id).focus();
                // end
            });

        },
        loadExistingImage: function () {
            //console.log('loadExistingImage');
            var that = this;

            if ($.isEmptyObject(that.croppedImg)) {
                if (that.options.onBeforeImgUpload)
                    that.options.onBeforeImgUpload.call(that);

                that.showLoader();
                if (that.options.modal) {
                    that.createModal();
                }
                if (!$.isEmptyObject(that.croppedImg)) {
                    that.croppedImg.remove();
                }

                that.imgUrl = that.options.loadPicture;

                //console.log('3 => ', that.imgUrl);

                var img = $('<img src="' + that.options.loadPicture + '">');
                that.obj.append(img);
                img.load(function () {
                    that.imgInitW = that.imgW = this.width;
                    that.imgInitH = that.imgH = this.height;
                    that.initCropper();
                    that.hideLoader();
                    if (that.options.onAfterImgUpload)
                        that.options.onAfterImgUpload.call(that);
                });

            } else {
                that.cropControlRemoveCroppedImage.on('click', function () {
                    that.croppedImg.remove();
                    $(this).hide();

                    if (!$.isEmptyObject(that.defaultImg)) {
                        that.obj.append(that.defaultImg);
                    }
                    if (that.options.outputUrlId !== '') {
                        $('#' + that.options.outputUrlId).val('');
                    }
                    that.croppedImg = '';
                    that.reset();
                });
            }

        },
        afterUpload: function (data) {
            //console.log('afterUpload');
            var that = this;

            response = typeof data == 'object' ? data : jQuery.parseJSON(data);

            if (response.status == 'success') {

                that.imgInitW = that.imgW = response.width;
                that.imgInitH = that.imgH = response.height;

                if (that.options.modal) {
                    that.createModal();
                }
                if (!$.isEmptyObject(that.croppedImg)) {
                    that.croppedImg.remove();
                }

                that.imgUrl = response.url;

                //console.log('4 => ', that.imgUrl);


                var img = $('<img src="' + response.url + '">')

                that.obj.append(img);

                img.load(function () {
                    that.initCropper();
                    that.hideLoader();
                    if (that.options.onAfterImgUpload)
                        that.options.onAfterImgUpload.call(that);
                });

                if (that.options.onAfterImgUpload)
                    that.options.onAfterImgUpload.call(that);

            }

            if (response.status == 'error') {
                alert(response.message);
                if (that.options.onError)
                    that.options.onError.call(that, response.message);
                that.hideLoader();
                setTimeout(function () {
                    that.reset();
                }, 2000)
            }
        },
        createModal: function () {
            //console.log('createModal');
            var that = this;

            var marginTop = that.windowH / 2 - that.objH / 2;
            var modalHTML = '<div id="croppicModal">' + '<div id="croppicModalObj" style="width:' + that.objW + 'px; height:' + that.objH + 'px; margin:0 auto; margin-top:' + marginTop + 'px; position: relative;"> </div>' + '</div>';

            $('body').append(modalHTML);

            that.modal = $('#croppicModal');

            that.obj = $('#croppicModalObj');

        },
        destroyModal: function () {
            //console.log('destroyModal');
            var that = this;

            that.obj = that.outputDiv;
            that.modal.remove();
            that.modal = {};
        },
        initCropper: function () {
            //console.log('initCropper');
            var that = this;

            /*SET UP SOME VARS*/
            that.img = that.obj.find('img');
            that.img.wrap('<div class="cropImgWrapper" style="width:' + that.objW + 'px; height:' + that.objH + 'px; "></div>');

            /*INIT DRAGGING*/
            that.createCropControls();

            if (that.options.imgEyecandy) {
                that.createEyecandy();
            }
            that.initDrag();
            that.initialScaleImg();
        },
        createEyecandy: function () {
            //console.log('createEyecandy');
            var that = this;

            that.imgEyecandy = that.img.clone();
            that.imgEyecandy.css({'z-index': '0', 'max-width': 'none', 'display': 'none', 'opacity': that.options.imgEyecandyOpacity}).appendTo(that.obj);
        },
        destroyEyecandy: function () {
            //console.log('destroyEyecandy');
            var that = this;
            that.imgEyecandy.remove();
        },
        initialScaleImg: function () {
            //console.log('initialScaleImg');
            var that = this;
            that.zoom(-that.imgInitW);
            that.zoom(40);

            // Adding mousewheel zoom capabilities
            if (that.options.enableMousescroll) {
                that.img.on('mousewheel', function (event) {
                    event.preventDefault();
                    that.zoom(that.options.zoomFactor * event.deltaY);
                });
            }
            // initial center image

            that.img.css({'left': -(that.imgW - that.objW) / 2, 'top': -(that.imgH - that.objH) / 2, 'position': 'relative'});
            if (that.options.imgEyecandy) {
                that.imgEyecandy.css({'left': -(that.imgW - that.objW) / 2, 'top': -(that.imgH - that.objH) / 2, 'position': 'relative'});
            }
            that.img.css('max-width', 'none');
        },
        createCropControls: function () {
            //console.log('createCropControls');
            var that = this;

            // CREATE CONTROLS
            var cropControlZoomMuchIn = '';
            var cropControlZoomIn = '<i class="fa fa-plus cropControlZoomIn" title="Uvećaj sliku"></i>';
            var cropControlZoomOut = '<i class="fa fa-minus cropControlZoomOut" title="Smanji sliku"></i>';
            var cropControlZoomMuchOut = '';
            var cropControlRotateLeft = '';
            var cropControlRotateRight = '';
            var cropControlCrop = '<i class="fa fa-crop color-green cropControlCrop" title="Iseci i sačuvaj sliku"></i>';
            var cropControlReset = '<i class="fa fa-times color-red cropControlReset" title="Izbriši sliku"></i>';

            var html;

            if (that.options.doubleZoomControls) {
                cropControlZoomMuchIn = '<i class="fa fa-plus-square cropControlZoomMuchIn" title="Uvećaj sliku"></i>';
                cropControlZoomMuchOut = '<i class="fa fa-minus-square cropControlZoomMuchOut" title="Smanji sliku"></i>';
            }
            if (that.options.rotateControls) {
                cropControlRotateLeft = '<i class="fa fa-undo cropControlRotateLeft" title="Rotiraj sliku za 90 stepenu u levo"></i>';
                cropControlRotateRight = '<i class="fa fa-repeat cropControlRotateRight" title="Rotiraj sliku za 90 stepenu u desno"></i>';
            }

            html = '<div class="cropControls cropControlsCrop"><div class="crop-btn-controls">' + cropControlZoomMuchIn + cropControlZoomIn + cropControlZoomOut + cropControlZoomMuchOut + cropControlRotateLeft + cropControlRotateRight + cropControlCrop + cropControlReset + '</div></div>';

            that.obj.append(html);

            that.cropControlsCrop = that.obj.find('.cropControlsCrop');

            // CACHE AND BIND CONTROLS
            if (that.options.doubleZoomControls) {
                that.cropControlZoomMuchIn = that.cropControlsCrop.find('.cropControlZoomMuchIn');
                that.cropControlZoomMuchIn.on('click', function () {
                    that.zoom(that.options.zoomFactor * 10);
                });

                that.cropControlZoomMuchOut = that.cropControlsCrop.find('.cropControlZoomMuchOut');
                that.cropControlZoomMuchOut.on('click', function () {
                    that.zoom(-that.options.zoomFactor * 10);
                });
            }

            that.cropControlZoomIn = that.cropControlsCrop.find('.cropControlZoomIn');
            that.cropControlZoomIn.on('click', function () {
                that.zoom(that.options.zoomFactor);
            });

            that.cropControlZoomOut = that.cropControlsCrop.find('.cropControlZoomOut');
            that.cropControlZoomOut.on('click', function () {
                that.zoom(-that.options.zoomFactor);
            });

            that.cropControlZoomIn = that.cropControlsCrop.find('.cropControlRotateLeft');
            that.cropControlZoomIn.on('click', function () {
                that.rotate(-that.options.rotateFactor);
            });

            that.cropControlZoomOut = that.cropControlsCrop.find('.cropControlRotateRight');
            that.cropControlZoomOut.on('click', function () {
                that.rotate(that.options.rotateFactor);
            });

            that.cropControlCrop = that.cropControlsCrop.find('.cropControlCrop');
            that.cropControlCrop.on('click', function () {
                that.crop();
            });

            that.cropControlReset = that.cropControlsCrop.find('.cropControlReset');
            that.cropControlReset.on('click', function () {
                that.reset();
                $(that.options.modal_id + ' .image-preview-clear').trigger('click');
                $(that.options.modal_id + ' .image-preview-clear').css('display', 'none');
            });

        },
        initDrag: function () {
            //console.log('initDrag');
            var that = this;

            that.img.on("mousedown touchstart", function (e) {

                e.preventDefault(); // disable selection

                var pageX;
                var pageY;
                var userAgent = window.navigator.userAgent;
                if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/android/i) || (e.pageY && e.pageX) == undefined) {
                    pageX = e.originalEvent.touches[0].pageX;
                    pageY = e.originalEvent.touches[0].pageY;
                } else {
                    pageX = e.pageX;
                    pageY = e.pageY;
                }

                var z_idx = that.img.css('z-index'),
                        drg_h = that.img.outerHeight(),
                        drg_w = that.img.outerWidth(),
                        pos_y = that.img.offset().top + drg_h - pageY,
                        pos_x = that.img.offset().left + drg_w - pageX;

                that.img.css('z-index', 1000).on("mousemove touchmove", function (e) {

                    var imgTop;
                    var imgLeft;

                    if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/android/i) || (e.pageY && e.pageX) == undefined) {
                        imgTop = e.originalEvent.touches[0].pageY + pos_y - drg_h;
                        imgLeft = e.originalEvent.touches[0].pageX + pos_x - drg_w;
                    } else {
                        imgTop = e.pageY + pos_y - drg_h;
                        imgLeft = e.pageX + pos_x - drg_w;
                    }

                    that.img.offset({
                        top: imgTop,
                        left: imgLeft
                    }).on("mouseup", function () {
                        $(this).removeClass('draggable').css('z-index', z_idx);
                    });

                    if (that.options.imgEyecandy) {
                        that.imgEyecandy.offset({top: imgTop, left: imgLeft});
                    }

                    if (that.options.scaleToFill) {
                        if (that.objH < that.imgH) {
                            if (parseInt(that.img.css('top')) > 0) {
                                that.img.css('top', 0);
                                if (that.options.imgEyecandy) {
                                    that.imgEyecandy.css('top', 0);
                                }
                            }
                            var maxTop = -(that.imgH - that.objH);
                            if (parseInt(that.img.css('top')) < maxTop) {
                                that.img.css('top', maxTop);
                                if (that.options.imgEyecandy) {
                                    that.imgEyecandy.css('top', maxTop);
                                }
                            }
                        } else {
                            if (parseInt(that.img.css('top')) < 0) {
                                that.img.css('top', 0);
                                if (that.options.imgEyecandy) {
                                    that.imgEyecandy.css('top', 0);
                                }
                            }
                            var maxTop = that.objH - that.imgH;
                            if (parseInt(that.img.css('top')) > maxTop) {
                                that.img.css('top', maxTop);
                                if (that.options.imgEyecandy) {
                                    that.imgEyecandy.css('top', maxTop);
                                }
                            }
                        }

                        if (that.objW < that.imgW) {
                            if (parseInt(that.img.css('left')) > 0) {
                                that.img.css('left', 0);
                                if (that.options.imgEyecandy) {
                                    that.imgEyecandy.css('left', 0);
                                }
                            }
                            var maxLeft = -(that.imgW - that.objW);
                            if (parseInt(that.img.css('left')) < maxLeft) {
                                that.img.css('left', maxLeft);
                                if (that.options.imgEyecandy) {
                                    that.imgEyecandy.css('left', maxLeft);
                                }
                            }
                        } else {
                            if (parseInt(that.img.css('left')) < 0) {
                                that.img.css('left', 0);
                                if (that.options.imgEyecandy) {
                                    that.imgEyecandy.css('left', 0);
                                }
                            }
                            var maxLeft = (that.objW - that.imgW);
                            if (parseInt(that.img.css('left')) > maxLeft) {
                                that.img.css('left', maxLeft);
                                if (that.options.imgEyecandy) {
                                    that.imgEyecandy.css('left', maxLeft);
                                }
                            }
                        }
                    }
                    if (that.options.onImgDrag)
                        that.options.onImgDrag.call(that);

                });

            }).on("mouseup", function () {
                that.img.off("mousemove");
            }).on("mouseout", function () {
                that.img.off("mousemove");
            });

        },
        rotate: function (x) {
            //console.log('rotate');
            var that = this;
            that.actualRotation += x;
            that.img.css({
                '-webkit-transform': 'rotate(' + that.actualRotation + 'deg)',
                '-moz-transform': 'rotate(' + that.actualRotation + 'deg)',
                'transform': 'rotate(' + that.actualRotation + 'deg)',
            });
            if (that.options.imgEyecandy) {
                that.imgEyecandy.css({
                    '-webkit-transform': 'rotate(' + that.actualRotation + 'deg)',
                    '-moz-transform': 'rotate(' + that.actualRotation + 'deg)',
                    'transform': 'rotate(' + that.actualRotation + 'deg)',
                });
            }
            if (typeof that.options.onImgRotate == 'function')
                that.options.onImgRotate.call(that);
        },
        zoom: function (x) {
            //console.log('zoom');
            var that = this;
            var ratio = that.imgW / that.imgH;
            var newWidth = that.imgW + x;
            var newHeight = newWidth / ratio;
            var doPositioning = true;

            if (!that.options.scaleToFill) {
                if (!newWidth || !newHeight) {
                    if (newWidth - that.objW < newHeight - that.objH) {
                        newWidth = that.objW;
                        newHeight = newWidth / ratio;
                    } else {
                        newHeight = that.objH;
                        newWidth = ratio * newHeight;
                    }
                }
            } else {
                if (newWidth < that.objW || newHeight < that.objH) {

                    if (newWidth - that.objW < newHeight - that.objH) {
                        newWidth = that.objW;
                        newHeight = newWidth / ratio;
                    } else {
                        newHeight = that.objH;
                        newWidth = ratio * newHeight;
                    }

                    doPositioning = false;

                }

                if ((newWidth > that.imgInitW || newHeight > that.imgInitH)) {

                    if (newWidth - that.imgInitW < newHeight - that.imgInitH) {
                        newWidth = that.imgInitW;
                        newHeight = newWidth / ratio;
                    } else {
                        newHeight = that.imgInitH;
                        newWidth = ratio * newHeight;
                    }

                    doPositioning = false;

                }
            }

            // Set image new demension
            that.imgW = newWidth;
            that.img.width(newWidth);

            that.imgH = newHeight;
            that.img.height(newHeight);



            // Set box position
            if (that.options.scaleToFill) {
                var newTop = parseInt(that.img.css('top')) - x / 2;
                var newLeft = parseInt(that.img.css('left')) - x / 2;

                if (newTop > 0) {
                    newTop = 0;
                }
                if (newLeft > 0) {
                    newLeft = 0;
                }

                var maxTop = -(newHeight - that.objH);
                if (newTop < maxTop) {
                    newTop = maxTop;
                }
                var maxLeft = -(newWidth - that.objW);
                if (newLeft < maxLeft) {
                    newLeft = maxLeft;
                }

                if (doPositioning) {
                    that.img.css({'top': newTop, 'left': newLeft});
                }
            }


            if (that.options.imgEyecandy) {
                that.imgEyecandy.width(newWidth);
                that.imgEyecandy.height(newHeight);
                if (doPositioning) {
                    that.imgEyecandy.css({'top': newTop, 'left': newLeft});
                }
            }

            if (that.options.onImgZoom)
                that.options.onImgZoom.call(that);

        },
        crop: function () {
            //console.log('crop');
            var that = this;

            if (that.options.onBeforeImgCrop)
                that.options.onBeforeImgCrop.call(that);

            that.cropControlsCrop.remove();
            that.showLoader();

            var cropData = {
                imgUrl: that.imgUrl,
                imgInitW: that.imgInitW,
                imgInitH: that.imgInitH,
                imgW: that.imgW,
                imgH: that.imgH,
                //imgY1: Math.abs(parseInt(that.img.css('top'))),
                imgY1: parseInt(that.img.css('top')),
                //imgX1: Math.abs(parseInt(that.img.css('left'))),
                imgX1: parseInt(that.img.css('left')),
                cropH: that.objH,
                cropW: that.objW,
                rotation: that.actualRotation,
                /* added for crop file name and path */
                file_name: that.$img_name,
                folder_path_name: that.options.folder_path_name,
                folder_url_name: that.options.folder_url_name
            };


            var formData;

            if (typeof FormData == 'undefined') {

                var XHR = new XMLHttpRequest();
                var urlEncodedData = "";
                var urlEncodedDataPairs = [];

                for (var key in cropData) {
                    urlEncodedDataPairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(cropData[key]));
                }
                for (var key in that.options.cropData) {
                    urlEncodedDataPairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(that.options.cropData[key]));
                }
                urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');
                XHR.addEventListener('error', function (event) {
                    if (that.options.onError)
                        that.options.onError.call(that, "XHR Request failed");
                });
                XHR.onreadystatechange = function () {
                    if (XHR.readyState == 4 && XHR.status == 200)
                    {
                        that.afterCrop(XHR.responseText);
                    }
                }
                XHR.open('POST', that.options.cropUrl);

                XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                XHR.setRequestHeader('Content-Length', urlEncodedData.length);

                XHR.send(urlEncodedData);

            } else {
                formData = new FormData();

                console.log('cropData', cropData);
                console.log('that.options', that.options);

                for (var key in cropData) {
                    if (cropData.hasOwnProperty(key)) {
                        formData.append(key, cropData[key]);
                    }
                }

                for (var key in that.options.cropData) {
                    if (that.options.cropData.hasOwnProperty(key)) {
                        formData.append(key, that.options.cropData[key]);
                    }
                }

                // added 18.01.2016
                $(that.options.modal_id + ' .modal-footer .btn-modal-crop-image').addClass('display-none');
                $(that.options.modal_id + ' .modal-footer .btn-modal-reset-image').addClass('display-none');
                // end

                //console.log('formData', formData);

                $.ajax({
                    url: that.options.cropUrl,
                    data: formData,
                    context: document.body,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST'
                }).error(function () {
                    that.obj.append('<a class="float-ok-status-holder"><i class="fa fa-times-circle-o color-red float-ok-status"></i><br/><span class="float-error-status color-red">Grešta tokom obrade fotografije, učitajte fotografiju ponovo.</span></a>');
                }).success(function (data) {
                    that.afterCrop(data);
                });
            }

            //
        },
        afterCrop: function (data) {
            //console.log('afterCrop');
            var that = this;
            try {
                response = jQuery.parseJSON(data);
            } catch (err) {
                response = typeof data == 'object' ? data : jQuery.parseJSON(data);
            }

            //console.log(response);

            if (response.status == 'success') {
                if (that.options.imgEyecandy)
                    that.imgEyecandy.hide();

                //that.destroy();

                that.obj.append('<span class="color-green float-ok-status-text"><i class="fa fa-check-circle"></i> Uspešno obrađena fotografija</span>');
                that.obj.append('<a data-toggle="tooltip" data-placement="top"  title="Uspešno pripremljenja fotografija" class="float-ok-status-holder"><i class="fa fa-check-circle color-green float-ok-status"></i></a>');

                $(that.options.modal_id + ' .modal-footer .btn-modal-crop-image').addClass('display-none');
                $(that.options.modal_id + ' .modal-footer .btn-modal-reset-image').addClass('display-none');

                $(that.options.holder_id + ' .image-preview-filename').val(that.$img_name);
                $(that.options.holder_id + ' .image-preview-clear').show();

                //that.obj.append('<img class="croppedImg" src="' + response.url + '">');

                $(that.options.member_image_id).attr('value', response.file);

                $(that.options.holder_id).find('.multiple-concept-img img').attr('src', response.url);
                $(that.options.holder_id).find('.multiple-concept-img-holder').attr('src', response.url);

                $(that.options.holder_id + '-upload-image').attr('src', response.url_real);


                if (that.options.outputUrlId !== '') {
                    $('#' + that.options.outputUrlId).val(response.url);
                }
                //that.croppedImg = that.obj.find('.croppedImg');
                //$('.cropImgWrapper').remove();
                that.init();
                that.hideLoader();
                that.hideOkStatus();
            }
            if (response.status == 'error') {
                if (that.options.onError)
                    that.options.onError.call(that, response.message);
                that.hideLoader();
                //that.obj.append('<span class="color-red float-ok-status-text"><i class="fa fa-times-circle-o"></i> ' + response.message + '</span>');
                that.obj.append('<a data-toggle="tooltip" data-placement="top"  title="' + response.message + '" class="float-ok-status-holder"><i class="fa fa-times-circle-o color-red float-ok-status"></i><br/><span class="float-error-status color-red">' + response.message + '</span></a>');

                $(that.options.modal_id + ' .modal-footer .btn-modal-crop-image').removeClass('display-none');
                $(that.options.modal_id + ' .modal-footer .btn-modal-reset-image').removeClass('display-none');

                setTimeout(function () {
                    that.reset();
                }, 4000);

            }

            // added 18.01.2016
            $(that.options.modal_id + ' .submit_form').removeAttr('disabled');
            // end

            if (that.options.onAfterImgCrop)
                that.options.onAfterImgCrop.call(that, response);
        },
        showLoader: function () {
            //console.log('showLoader');
            var that = this;

            that.obj.append(that.options.loaderHtml);
            that.loader = that.obj.find('.loader');

        },
        hideLoader: function () {
            //console.log('hideLoader');
            var that = this;
            //console.log('that.loader', that.loader);
            that.loader.remove();

            //console.log('that.options.modal_id', that.options.modal_id);

            $(that.options.modal_id).modal('show');


        },
        reset: function () {
            //console.log('reset');
            var that = this;
            that.destroy();

            that.init();

            if (!$.isEmptyObject(that.croppedImg)) {
                that.obj.append(that.croppedImg);
                if (that.options.outputUrlId !== '') {
                    $('#' + that.options.outputUrlId).val(that.croppedImg.attr('url'));
                }
            }
            if (typeof that.options.onReset == 'function')
                that.options.onReset.call(that);
        },
        destroy: function () {
            //console.log('destroy');
            var that = this;
            if (that.options.modal && !$.isEmptyObject(that.modal)) {
                that.destroyModal();
            }
            if (that.options.imgEyecandy && !$.isEmptyObject(that.imgEyecandy)) {
                that.destroyEyecandy();
            }
            if (!$.isEmptyObject(that.cropControlsUpload)) {

                //console.log('that.cropControlsUpload.remove();');
                that.cropControlsUpload.remove();
            }
            if (!$.isEmptyObject(that.cropControlsCrop)) {

                //console.log('that.cropControlsCrop.remove();');
                that.cropControlsCrop.remove();
            }
            if (!$.isEmptyObject(that.loader)) {

                //console.log('that.loader.remove();');
                that.loader.remove();
            }
            if (!$.isEmptyObject(that.form)) {

                //console.log('that.form.remove();');
                that.form.remove();
            }

            /*if (that.$img_name === '') {
             $('.image-preview-filename').val('');
             }*/


            $(that.options.modal_id + ' .float-ok-status-text').remove();
            $(that.options.modal_id + ' .float-ok-status-holder').remove();


            //console.log('that.obj.html()', that.obj.html());


            that.obj.html('');
        },
        hideOkStatus: function () {
            var that = this;
            //console.log('hideOkStatus');
            var interval = null;
            interval = setInterval(function () {
                $(that.options.modal_id + ' .float-ok-status-holder').remove();
                $(that.options.modal_id).modal('hide');
                clearInterval(interval);
            }, 1500);
        },
        isAjaxUploadSupported: function () {
            //console.log('isAjaxUploadSupported');
            var input = document.createElement("input");
            input.type = "file";

            return ("multiple" in input &&
                    typeof File != "undefined" &&
                    typeof FormData != "undefined" &&
                    typeof (new XMLHttpRequest()).upload != "undefined");
        },
        CreateFallbackIframe: function () {
            //console.log('CreateFallbackIframe');
            var that = this;

            if (!that.isAjaxUploadSupported()) {

                if (jQuery.isEmptyObject(that.iframeobj)) {
                    var iframe = document.createElement("iframe");
                    iframe.setAttribute("id", that.id + "_upload_iframe");
                    iframe.setAttribute("name", that.id + "_upload_iframe");
                    iframe.setAttribute("width", "0");
                    iframe.setAttribute("height", "0");
                    iframe.setAttribute("border", "0");
                    iframe.setAttribute("src", "javascript:false;");
                    iframe.style.display = "none";
                    document.body.appendChild(iframe);
                } else {
                    iframe = that.iframeobj[0];
                }

                var myContent = '<!DOCTYPE html>'
                        + '<html><head><title>Uploading File</title></head>'
                        + '<body>'
                        + '<form '
                        + 'class="' + that.id + '_upload_iframe_form" '
                        + 'name="' + that.id + '_upload_iframe_form" '
                        + 'action="' + that.options.uploadUrl + '" method="post" '
                        + 'enctype="multipart/form-data" encoding="multipart/form-data" style="display:none;">'
                        + $("#" + that.id + '_imgUploadField')[0].outerHTML
                        + '</form></body></html>';

                iframe.contentWindow.document.open('text/htmlreplace');
                iframe.contentWindow.document.write(myContent);
                iframe.contentWindow.document.close();

                that.iframeobj = $("#" + that.id + "_upload_iframe");
                that.iframeform = that.iframeobj.contents().find("html").find("." + that.id + "_upload_iframe_form");

                that.iframeform.on("change", "input", function () {
                    that.SubmitFallbackIframe(that);
                });
                that.iframeform.find("input")[0].attachEvent("onchange", function () {
                    that.SubmitFallbackIframe(that);
                });

                var eventHandlermyFile = function () {
                    if (iframe.detachEvent)
                        iframe.detachEvent("onload", eventHandlermyFile);
                    else
                        iframe.removeEventListener("load", eventHandlermyFile, false);

                    var response = that.getIframeContentJSON(iframe);

                    if (jQuery.isEmptyObject(that.modal)) {
                        that.afterUpload(response);
                    }
                }

                if (iframe.addEventListener)
                    iframe.addEventListener("load", eventHandlermyFile, true);
                if (iframe.attachEvent)
                    iframe.attachEvent("onload", eventHandlermyFile);

                return "#" + that.id + '_imgUploadField';

            } else {
                return "";
            }

        },
        SubmitFallbackIframe: function (that) {
            //console.log('SubmitFallbackIframe');
            that.showLoader();
            if (that.options.processInline && !that.options.uploadUrl) {
                if (that.options.onError) {
                    that.options.onError.call(that, "processInline is not supported by your browser ");
                    that.hideLoader();
                }
            } else {
                if (that.options.onBeforeImgUpload)
                    that.options.onBeforeImgUpload.call(that);
                that.iframeform[0].submit();
            }
        },
        getIframeContentJSON: function (iframe) {
            //console.log('getIframeContentJSON');
            try {
                var doc = iframe.contentDocument ? iframe.contentDocument : iframe.contentWindow.document,
                        response;

                var innerHTML = doc.body.innerHTML;
                if (innerHTML.slice(0, 5).toLowerCase() == "<pre>" && innerHTML.slice(-6).toLowerCase() == "</pre>") {
                    innerHTML = doc.body.firstChild.firstChild.nodeValue;
                }
                response = jQuery.parseJSON(innerHTML);
            } catch (err) {
                response = {success: false};
            }

            return response;
        },
        checkSize: function (that, max_img_size) {
            var input = that.form.find("input[type=file]")[0];
            // check for browser support (may need to be modified)
            if (input.files && input.files.length === 1)
            {
                if (input.files[0].size > max_img_size)
                {
                    var helperMsg = "Slika ne može biti veća od " + (max_img_size / 1024 / 1024) + "MB!";
                    $(that.options.member_image_id).closest('.form-group').find(".error_holder").html(helperMsg);
                    $(that.options.member_image_id).closest('.form-group').find(".error_holder").addClass("errorMessage");
                    $(that.options.member_image_id).closest('.form-group').find(".form-control").addClass("input-error");
                    $(that.options.member_image_id).closest('.form-group').find(".error_holder").removeClass("hide");
                    return false;
                }
            }

            return true;
        }
    }

})(window, document);
