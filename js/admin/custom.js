/** 
 *  Custom admin js
 * */

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
"use strict";

$(document).ready(function () {
    Tooltip();

    $('.pdf-embed').css('height', $(window).height() - 160 + 'px');
});
/** ******  left menu  *********************** **/
$(function () {
    $('#sidebar-menu li ul').slideUp();
    $('#sidebar-menu li').removeClass('active');

    $('#sidebar-menu li').click(function () {
        if ($(this).is('.active')) {
            $(this).removeClass('active');
            $('ul', this).slideUp();
            $(this).removeClass('nv');
            $(this).addClass('vn');
        } else {
            $('#sidebar-menu li ul').slideUp();
            $(this).removeClass('vn');
            $(this).addClass('nv');
            $('ul', this).slideDown();
            $('#sidebar-menu li').removeClass('active');
            $(this).addClass('active');
        }
    });

    $('#menu_toggle').click(function () {
        if ($('body').hasClass('nav-md')) {
            $('body').removeClass('nav-md');
            $('body').addClass('nav-sm');
            $('.left_col').removeClass('scroll-view');
            $('.left_col').removeAttr('style');
            $('.sidebar-footer').hide();

            if ($('#sidebar-menu li').hasClass('active')) {
                $('#sidebar-menu li.active').addClass('active-sm');
                $('#sidebar-menu li.active').removeClass('active');
            }
        } else {
            $('body').removeClass('nav-sm');
            $('body').addClass('nav-md');
            $('.sidebar-footer').show();

            if ($('#sidebar-menu li').hasClass('active-sm')) {
                $('#sidebar-menu li.active-sm').addClass('active');
                $('#sidebar-menu li.active-sm').removeClass('active-sm');
            }
        }
    });
});

/* Sidebar Menu active class */
$(function () {
    var url = window.location;
    $('#sidebar-menu a[href="' + url + '"]').parent('li').addClass('current-page');
    $('#sidebar-menu a').filter(function () {
        return this.href == url;
    }).parent('li').addClass('current-page').parent('ul').slideDown().parent().addClass('active');
});

/** ******  /left menu  *********************** **/



/** ******  tooltip  *********************** **/
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
/** ******  /tooltip  *********************** **/
/** ******  progressbar  *********************** **/
if ($(".progress .progress-bar")[0]) {
    $('.progress .progress-bar').progressbar(); // bootstrap 3
}
/** ******  /progressbar  *********************** **/
/** ******  switchery  *********************** **/
if ($(".js-switch")[0]) {
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    elems.forEach(function (html) {
        var switchery = new Switchery(html, {
            color: '#26B99A'
        });
    });
}
/** ******  /switcher  *********************** **/
/** ******  collapse panel  *********************** **/
// Close ibox function
$('.close-link').click(function () {
    var content = $(this).closest('div.x_panel');
    content.remove();
});

// Collapse ibox function
$('.collapse-link').click(function () {
    var x_panel = $(this).closest('div.x_panel');
    var button = $(this).find('i');
    var content = x_panel.find('div.x_content');
    content.slideToggle(200);
    (x_panel.hasClass('fixed_height_390') ? x_panel.toggleClass('').toggleClass('fixed_height_390') : '');
    (x_panel.hasClass('fixed_height_320') ? x_panel.toggleClass('').toggleClass('fixed_height_320') : '');
    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
    setTimeout(function () {
        x_panel.resize();
    }, 50);
});
/** ******  /collapse panel  *********************** **/
/** ******  iswitch  *********************** **/
if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}
/** ******  /iswitch  *********************** **/
/** ******  star rating  *********************** **/
// Starrr plugin (https://github.com/dobtco/starrr)
var __slice = [].slice;

(function ($, window) {
    var Starrr;

    Starrr = (function () {
        Starrr.prototype.defaults = {
            rating: void 0,
            numStars: 5,
            change: function (e, value) {}
        };

        function Starrr($el, options) {
            var i, _, _ref,
                    _this = this;

            this.options = $.extend({}, this.defaults, options);
            this.$el = $el;
            _ref = this.defaults;
            for (i in _ref) {
                _ = _ref[i];
                if (this.$el.data(i) != null) {
                    this.options[i] = this.$el.data(i);
                }
            }
            this.createStars();
            this.syncRating();
            this.$el.on('mouseover.starrr', 'span', function (e) {
                return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
            });
            this.$el.on('mouseout.starrr', function () {
                return _this.syncRating();
            });
            this.$el.on('click.starrr', 'span', function (e) {
                return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
            });
            this.$el.on('starrr:change', this.options.change);
        }

        Starrr.prototype.createStars = function () {
            var _i, _ref, _results;

            _results = [];
            for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
                _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
            }
            return _results;
        };

        Starrr.prototype.setRating = function (rating) {
            if (this.options.rating === rating) {
                rating = void 0;
            }
            this.options.rating = rating;
            this.syncRating();
            return this.$el.trigger('starrr:change', rating);
        };

        Starrr.prototype.syncRating = function (rating) {
            var i, _i, _j, _ref;

            rating || (rating = this.options.rating);
            if (rating) {
                for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                    this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
                }
            }
            if (rating && rating < 5) {
                for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
                    this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
                }
            }
            if (!rating) {
                return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
            }
        };

        return Starrr;

    })();
    return $.fn.extend({
        starrr: function () {
            var args, option;

            option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
            return this.each(function () {
                var data;

                data = $(this).data('star-rating');
                if (!data) {
                    $(this).data('star-rating', (data = new Starrr($(this), option)));
                }
                if (typeof option === 'string') {
                    return data[option].apply(data, args);
                }
            });
        }
    });
})(window.jQuery, window);

$(function () {
    return $(".starrr").starrr();
});

$(document).ready(function () {

    $('#stars').on('starrr:change', function (e, value) {
        $('#count').html(value);
    });


    $('#stars-existing').on('starrr:change', function (e, value) {
        $('#count-existing').html(value);
    });

});
/** ******  /star rating  *********************** **/
/** ******  table  *********************** **/
$('table input').on('ifChecked', function () {
    check_state = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('table input').on('ifUnchecked', function () {
    check_state = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});

var check_state = '';
$('.bulk_action input').on('ifChecked', function () {
    check_state = '';
    $(this).parent().parent().parent().addClass('selected');
    countChecked();
});
$('.bulk_action input').on('ifUnchecked', function () {
    check_state = '';
    $(this).parent().parent().parent().removeClass('selected');
    countChecked();
});
$('.bulk_action input#check-all').on('ifChecked', function () {
    check_state = 'check_all';
    countChecked();
});
$('.bulk_action input#check-all').on('ifUnchecked', function () {
    check_state = 'uncheck_all';
    countChecked();
});

function countChecked() {
    if (check_state == 'check_all') {
        $(".bulk_action input[name='table_records']").iCheck('check');
    }
    if (check_state == 'uncheck_all') {
        $(".bulk_action input[name='table_records']").iCheck('uncheck');
    }
    var n = $(".bulk_action input[name='table_records']:checked").length;
    if (n > 0) {
        $('.column-title').hide();
        $('.bulk-actions').show();
        $('.action-cnt').html(n + ' Records Selected');
    } else {
        $('.column-title').show();
        $('.bulk-actions').hide();
    }
}
/** ******  /table  *********************** **/
/** ******  Accordion  *********************** **/

$(function () {
    $(".expand").on("click", function () {
        $(this).next().slideToggle(200);
        $expand = $(this).find(">:first-child");

        if ($expand.text() == "+") {
            $expand.text("-");
        } else {
            $expand.text("+");
        }
    });
});

/** ******  Accordion  *********************** **/


/** Loader functions */
$(document).on('click', '.menu_section li', function () {
    if (!$(this).find('ul').hasClass('child_menu')) {
        showLoaderCustom();
    }
});

$(document).on('click', '.js_display_loader', function () {
    showLoaderCustom();
});

function showLoaderCustom($obj) {
    var $loaderHtml = '<div id="loader-wrapper"><div id="loader"></div></div>';

    if ($obj) {
        var $loader_holder = $($obj);
        $loader_holder.html($loaderHtml);
    } else {
        $('body').prepend($loaderHtml);
    }
    return false;
}

function hideLoaderCustom() {
    $('#loader-wrapper').remove();
    return false;
}

/** ./Loader functions */

/** Tooltip */

function Tooltip() {
    $('.po-markup > .po-link').popover({
        trigger: 'click',
        html: true, // must have if HTML is contained in popover

        // get the title and conent
        title: function () {
            return $(this).parent().find('.po-title').html();
        },
        content: function () {
            return $(this).parent().find('.po-body').html();
        },
        container: 'body',
        placement: 'bottom'
    });
}

$(document).on('click', 'body', function (e) {
    //$('[data-toggle=popover]').each(function () {
    $('.po-markup > .po-link[aria-describedby]').each(function () {
        // hide any open popovers when the anywhere else in the body is clicked
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});

/** ./Tooltip */

/** Resize charts in tabs */
$(document).on('click', '.nav-tabs li', function (e) { // on tab selection event
    var resizeChartVal = setInterval(function () {
        resizeChart();
        clearInterval(resizeChartVal);
    }, 350);
});

function resizeChart() {
    $(".tab-content .highcharts-container").each(function () { // target each element with the .contains-chart class
        var $obj = $(this).parent();
        console.log($obj.html());

        var chart = $obj.highcharts(); // target the chart itself
        chart.reflow();
    });
}
/** ./Resize charts in tabs */

/** add gallery items */
(function ($) {
    $(function () {

        var addFormGroup = function (event) {
            event.preventDefault();

            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
            var $formGroupClone = $formGroup.clone();

            $(this)
                    .toggleClass('btn-success btn-add btn-danger btn-remove')
                    .html('–');

            $formGroupClone.find('.input-group-url-val').val('');
            $formGroupClone.find('.input-group-select-val').val($multipleFormGroup.attr('data-default-ddl-value'));
            $formGroupClone.find('.concept').text($multipleFormGroup.attr('data-default-ddl-text'));

            //products attributes
            $formGroupClone.find('.form-attributes-input').find('*[name]').val('');

            $formGroupClone.find('.multiple-concept-img img').attr('src', $multipleFormGroup.attr('data-default-ddl-value'));
            $formGroupClone.find('.multiple-concept-img-holder').attr('src', $multipleFormGroup.attr('data-default-ddl-value'));
            $formGroupClone.insertAfter($formGroup);

            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', true);
            }
        };

        var removeFormGroup = function (event) {
            if (!confirm('Da li želite da obrišete podatak?')) {
                return false;
            }

            event.preventDefault();

            var $formGroup = $(this).closest('.form-group');
            var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

            var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
            if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                $lastFormGroupLast.find('.btn-add').attr('disabled', false);
            }

            if ($multipleFormGroup.hasClass('image-form')) {
                var $holder = $multipleFormGroup.closest('.contacts');

                var $path = $holder.attr('data-img-path');
                var $url = $holder.attr('data-file-remove-url');
                var $code = $holder.attr('data-code');
                var $tbl_name = $holder.attr('data-tbl-name');
                var $col_name = $holder.attr('data-col-name');
                var $name = $multipleFormGroup.find('.input-group-url-val').val();

                var $data = {
                    path: $path,
                    name: $name,
                    code: $code,
                    tbl_name: $tbl_name,
                    col_name: $col_name
                };

                $.post($url, $data, function ($reponse) {
                    console.log($reponse);
                });
            }

            $formGroup.remove();
        };

        var selectFormGroup = function (event) {
            event.preventDefault();

            var $selectGroup = $(this).closest('.input-group-select');
            var param = $(this).attr("href").replace("#", "");
            var concept = $(this).text();

            $selectGroup.find('.concept').text(concept);
            $selectGroup.find('.input-group-select-val').val(param);

        }

        var countFormGroup = function ($form) {
            return $form.find('.form-group').length;
        };

        $(document).on('click', '.form-holder .btn-add', addFormGroup);
        $(document).on('click', '.form-holder .btn-remove', removeFormGroup);
        $(document).on('click', '.form-holder .dropdown-menu a', selectFormGroup);

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var $obj = $(input).closest('.image-multiple-form');

                    $obj.find('.multiple-concept-img img').attr('src', e.target.result);
                    $obj.find('.multiple-concept-img-holder').attr('src', e.target.result);
                    $obj.find('.input-group-url-val').val(input.files[0].name);


                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change', '.file-btn-select', function () {
            readURL(this);
        });

        $(document).ready(function () {
            $('.video-multiple-form-holder').each(function () {
                var $holder = $(this);
                var $data = $(this).attr('data-db-data');

                if ($data) {
                    var $data_arr = JSON.parse($data);

                    $.each($data_arr, function ($val, $row) {

                        $holder.find('.btn-add').trigger('click');

                        var $multi_holder = $holder.find('.video-multiple-form').eq($val);

                        // video
                        $multi_holder.find('.input-group-select-val').val($row.type);
                        $multi_holder.find('.concept').text($row.type);
                        $multi_holder.find('.input-group-url-val').val($row.url);
                    });
                }
            });
        });

        $(document).ready(function () {
            $('.image-multiple-form-holder').each(function () {
                var $holder = $(this);
                var $data = $(this).attr('data-db-data');
                var $img_url = $(this).attr('data-img-url');

                if ($data) {
                    var $data_arr = JSON.parse($data);

                    $.each($data_arr, function ($val, $row) {
                        $holder.find('.btn-add').trigger('click');

                        var $multi_holder = $holder.find('.image-multiple-form').eq($val);

                        // images
                        $multi_holder.find('.multiple-concept-img img').attr('src', $img_url + $row);
                        $multi_holder.find('.multiple-concept-img-holder').attr('src', $img_url + $row);
                        $multi_holder.find('.input-group-url-val').val($row);
                        //$multi_holder.find('.btn-file').remove();
                    });


                }

            });
        });
    });
})(jQuery);

/** Set time to tick */
function updateClock() {
    var currentTime = new Date( );
    var currentHours = currentTime.getHours( );
    var currentMinutes = currentTime.getMinutes( );
    var currentSeconds = currentTime.getSeconds( );

    // Pad the minutes and seconds with leading zeros, if required
    currentMinutes = (currentMinutes < 10 ? "0" : "") + currentMinutes;
    currentSeconds = (currentSeconds < 10 ? "0" : "") + currentSeconds;

    // Choose either "AM" or "PM" as appropriate
    //var timeOfDay = (currentHours < 12) ? "AM" : "PM";

    // Convert the hours component to 12-hour format if needed
    //currentHours = (currentHours > 12) ? currentHours - 12 : currentHours;

    // Convert an hours component of "0" to "12"
    //currentHours = (currentHours == 0) ? 12 : currentHours;

    // Compose the string for display
    //var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;
    var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds;

    $("#clock").val(currentTimeString);
}

$(document).ready(function () {
    setInterval(function () {
        updateClock();
    }, 1000);
});
/** ./Set time to tick */
$(document).ready(function () {
//$('#js-categories-specification').addClass('hide');
});

/** Product Attributes DB data */

$(document).ready(function () {
    var $index_attr = 0;
    $('.attributes-multiple-form-holder').each(function () {
        var $holder = $(this);
        var $data = $(this).attr('data-db-data');
        if ($data) {
            var $data_arr = JSON.parse($data);
            console.log($data_arr);

            $.each($data_arr, function ($val, $row) {
                $holder.find('.btn-add').trigger('click');
                var $multi_holder = $holder.find('.attributes-multiple-form').eq($index_attr);
                $multi_holder.find('[name*="id_product"]').val($val);
                $.each($row, function ($attr_name, $attr_value) {

                    $multi_holder.find('[name*=' + $attr_name + ']').val($attr_value);

                });
                $index_attr++;
            });
        }
    });
});
/** ./Product Attributes DB data */
function hideProductsAttributes() {
    var $attributes_holder = $('#js-product-attributes');
    if ($attributes_holder) {
        var attributes_inputs = $attributes_holder.find('*[name]');

        $.each(attributes_inputs, function ($index, $attr) {
            $($attr).closest('.form-attributes-input').addClass('hide');
        });

        $attributes_holder.addClass('hide');
    }
}
$('.js-categories-select').on('change', function () {
    var $obj = $(this);
    var $url = $obj.attr('url');
    var code_categoires = $(".js-categories-select option:selected").val();
    if (code_categoires != "") {
        var $data = {
            code_categoires: code_categoires,
        };
        $.post($url, $data, function ($response) {
            $response = JSON.parse($response);
            var $attributes_holder = $('#js-product-attributes');
            if ($attributes_holder) {
                $.each($response, function ($index, $attr) {
                    $attributes_holder.find("*[name*=" + $attr + "]").closest('.form-attributes-input').removeClass('hide');
                });
                $attributes_holder.removeClass('hide');
            }
        });
    } else {
        hideProductsAttributes();
    }
});

$(document).ready(function () {
    var code_categoires = $(".js-categories-select option:selected").val();
    if (code_categoires == "") {
        hideProductsAttributes();
    }

});

$(document).on('change', '.js-change-status-ddl', function () {
    var $obj = $(this);
    var $url = $obj.attr('data-url');
    var $status = $obj.attr('data-status');
    var $id = $obj.attr('data-id');

    var $val = $obj.val();

    $obj.closest('div').addClass('hide');

    var $data = {
        'id': $id,
        'status': $status,
        'val': $val,
    };

    $.post($url, $data, function ($response) {
        $response = JSON.parse($response);

        if ($response.fnc) {
            window[$response.fnc]($obj, $response.fnc_data);
        } else if ($response.url) {
            window.location = $response.url;
        } else {
            window.location.reload();
        }
    });
});

$(document).on('click', '.js-change-status', function () {
    var $obj = $(this);
    var $url = $obj.attr('data-url');
    var $status = $obj.attr('data-status');
    var $id = $obj.attr('data-id');

    var $data = {
        'id': $id,
        'status': $status,
    };

    $.post($url, $data, function ($response) {
        $response = JSON.parse($response);

        if ($response.fnc) {
            window[$response.fnc]($obj, $response.fnc_data);
        } else if ($response.url) {
            window.location = $response.url;
        } else {
            window.location.reload();
        }
    });
});

function changeProductBtn($obj, $data) {
    $obj.removeClass($data.btn_remove_class);
    $obj.addClass($data.btn_add_class);
    $obj.attr('data-status', $data.status);
    $obj.html($data.text);
    return false;
}

function changeGroupIngrediantsBtn($obj, $data) {
    window.location.reload();
    return false;
}

function checkNewOrders() {
    var $holder = $('.js-order-table');
    var $url = $holder.attr('data-url');
    var $active_row = $holder.find('.js-order-row.active').attr('data-id');
    var $new_row = $holder.find('.js-order-row.alert.alert-info:first').attr('data-id');
    var $id = $active_row ? $active_row : $new_row;

    var $data = {
        'id': $id
    };

    $.post($url, $data, function ($response) {
        $response = JSON.parse($response);

        if ($response.status == 'OK') {
            $('.js-order-table').attr('data-new-orders', 'yes');
            window.location.href = $response.url;
        } else {
            $('.js-order-table').attr('data-new-orders', 'no');

            if ($('.js-order-row.alert-info.active').length) {
                window.location.href = $response.url;
            }
        }

        playOrderSound();
    });
}

function displayOrderDetails() {
    $(document).on('click', '.js-order-row', function () {
        var $obj = $(this);
        var $url = $obj.attr('data-url');
        var $id = $obj.attr('data-id');

        $id = $id && $id != 'undefined' ? $id : '';
        clearAllActiveRows();

        $obj.addClass('active');

        var $data = {
            'id': $id
        };

        ajaxSendOrderDetails($url, $data);
    });
}

function setSelectedRow() {
    var $holder = $('.js-order-table');
    var $id = $holder.attr('data-selected-row');
    var $new_row = $holder.find('.js-order-row.alert.alert-info:first').attr('data-id');
    $id = $new_row && $new_row != 'undefined' ? $new_row : $id;
    $id = $id && $id != 'undefined' ? $id : $holder.find('.js-order-row:first').attr('data-id');

    if ($id && $id != 'undefined') {
        var $row = $holder.find('.js-order-row[data-id="' + $id + '"]');
        $row.addClass('active');
        var $url = $row.attr('data-url');

        var $data = {
            'id': $id
        };
        ajaxSendOrderDetails($url, $data);
    }

}

function ajaxSendOrderDetails($url, $data) {
    $.post($url, $data, function ($response) {
        $response = JSON.parse($response);
        var $holder = $('.js-order-details');
        $holder.html($response.data);

        countDownOrderPreparation($holder);
    });
}

function clearAllActiveRows() {
    $('.js-order-row').removeClass('active');
}

function countDownOrderPreparation($holder) {
    var $display_holder = $holder.find('.js-delivery-time-calc');
    var $min = $display_holder.attr('data-left-time');

    if ($min) {
        $min = Number($min) * Number(60);
        startTimer($min, $display_holder.find('b'));
    }
}

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text(minutes + ":" + seconds);

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

function printOrder() {
    $(document).on('click', '.js-order-print', function () {
        window.print();
    });
}

function setPlayer() {
    var $audio_path = $(document).find('.js-audio-url').attr('data-audio-url');

    soundManager.setup({
        url: $audio_path + 'soundmanager/swf/',
        flashVersion: 9, // optional: shiny features (default = 8)
        useFlashBlock: false, // optionally, enable when you're ready to dive in
        preferFlash: false,
        onready: function () {
            // Ready to use; soundManager.createSound() etc. can now be called.
            soundManager.createSound({
                id: 'alertOrder',
                url: $audio_path + '/telephone-ring-04.ogg',
                autoLoad: true,
                volume: 100,
                onfinish: function () {
                    soundManager.play('alertOrder');
                }
            });
        }
    });

    soundManager.onready(function () {
        // SM2 is ready to go!

        playOrderSound();
    });

    soundManager.onerror = function () {
        alert("Podešavanje zvuka nije uspelo!");
    }
}

function playOrderSound() {
    var $new_orders = $('.js-order-table').attr('data-new-orders');

    if ($new_orders == 'yes') {
        soundManager.play('alertOrder');
    } else {
        soundManager.stop('alertOrder');
    }
}

$(document).ready(function () {
    if ($('body').attr('data-action') == "index") {
        displayOrderDetails();
        setSelectedRow();
        printOrder();
        setPlayer();

        setInterval(function () {
            checkNewOrders();
        }, 25000);
    }

    if ($('body').attr('data-action') == "ordermaplocation") {
        printOrder();
    }

});


$(document).on('click', '.js-extra-row-display', function () {

    var $row = $(this).closest('tr').find('.js-extra-row');
    var $status = $row.css('display');

    $(this).closest('body').find('.js-extra-row').css('display', 'none');

    if ($status == 'none') {
        $row.css('display', 'block');
    }
});

function getCustomerOrders($customer_id, $url) {
    var $data = {
        'id': $customer_id
    };

    $.post($url, $data, function ($response) {
        $('.js-customer-data').html($response);
    });
}